<?php
/**
 * @package SmsAnalysis
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/sms-analysis
 */
namespace SmsAnalysis;

require_once '../vendor/autoload.php';

use SmsAnalysis\Lib\Utility;

session_start();

try {
    Utility::doAction($_GET, $_POST, $_FILES);
    $pageData = Utility::getPageData($_GET);
    //var_dump($pageData['chart']);
} catch (\Exception $e) {
    $error= $e->getMessage();
    Utility::setMessage($error, "error");
    Utility::setSession("die", "true");
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php Utility::getElement('htmlHead', array("title" => "Sms Analysis")); ?>
</head>
<body>
    <?php Utility::getElement('navbar'); ?>
    <?php Utility::getElement('importModal'); ?>
    <?php Utility::getElement('removeDataModal'); ?>
    <?php Utility::getElement('installModal'); ?>
    <?php Utility::getElement('removeDatabaseModal'); ?>
    <?php Utility::getElement('showMessage'); ?>
    <?php //Utility::getElement('chartModal', $pageData['contacts']); ?>
    <div id="main" class="row">
        <div id="titles">
            <h3 class="contacts">Contacts</h3>
            <h3 class="messages"><?php echo empty($pageData['number'])?"":$pageData['contacts'][$pageData['number']]; ?> Messages</h3>
            <h3 class="stats">Stats</h3>
        </div>
        <div id="data">
            <ul id="contacts" class="list-unstyled">
                <?php foreach ($pageData['contacts'] as $key => $name): ?>
                    <li>
                        <a href="index.php?number=<?php echo $key; ?>"><?php echo $name; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div id="messages">
                <ul class="list-unstyled">
                    <?php foreach ($pageData['messages'] as $message): ?>
                        <li class="<?php echo $message['type']; ?> triangle-right">
                            <p class="<?php echo  $message['lang']; ?>"/>
                                <?php echo $message['body']; ?>
                            </p>
                            <small class="farsi pull-right"><?php echo $message['jalali_date']; ?></small>
                            <small class="pull-left">
                                <?php echo $message['type']=='Received'?"From: ":"To: "; ?> 
                                <a href="index.php?number=<?php echo $message['number']; ?>">
                                    <?php echo $pageData['contacts'][$message['number']]; ?>
                                </a>
                            </small>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <ul class="pagination">
                    <?php
                        if (!empty($pageData['pageRange'])) {
                            $currURL = Utility::getCurrentURL($pageData['number']);
                            if ($pageData['currPage'] > 1) {
                              echo "<li><a href='" . $currURL . ($pageData['currPage'] - 1) . "'>Prev</a></li>";
                            } else {
                              echo "<li class='disabled'><span>Prev</span></li>";
                            }
                            foreach ($pageData['pageRange'] as $i) {
                                if ($pageData['currPage'] == $i) {
                                    echo "<li class='active disabled'><a href='" . $currURL . $i . "'>" . $i . "</a></li>";
                            } else {
                                echo "<li><a href='" . $currURL . $i . "'>" . $i . "</a></li>";
                                }
                            }
                            if ($pageData['currPage'] < $pageData['pageCount']) {
                               echo "<li><a href='" . $currURL . ($pageData['currPage'] + 1) . "'>Next</a></li>";
                            } else {
                                echo "<li class='disabled'><span>Next</span></li>";
                            }
                        }
                   ?>
                </ul>
            </div>
            <div id="stats">
                <!--table cellspacing="5px" cellpadding="5px">
                    <tr>
                        <th></th>
                        <th>Msg Count</th>
                        <th>Cost</th>
                    <tr>
                    <tr>
                        <td><strong>Recieved</strong></td>
                        <td><?php echo $pageData['stats'][0]['recievedCount']; ?></td>
                        <td><?php echo $pageData['stats'][0]['recievedCost']; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Sent</strong></td>
                        <td><?php echo $pageData['stats'][0]['sentCount']; ?></td>
                        <td><?php echo $pageData['stats'][0]['sentCost']; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Total</strong></td>
                        <td><?php echo $pageData['stats'][0]['recievedCount'] + $pageData['stats'][0]['sentCount']; ?></td>
                        <td><?php echo $pageData['stats'][0]['recievedCost'] + $pageData['stats'][0]['sentCost']; ?></td>
                    </tr>
                </table-->
                <div>
                    <form class="form-horizontal">
                        <div class="datePicker form-group">
                            <label for="datePickerFrom" class="col-lg-2 control-label">From</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="datePickerFrom" />
                                <input type="hidden" id="hiddenFrom"/>
                            </div>
                        </div>
                        <div class="datePicker form-group">
                            <label for="datePickerTo" class="col-lg-2 control-label">To</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="datePickerTo" />
                                <input type="hidden" id="hiddenTo"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="period" class="col-lg-2 control-label">Period</label>
                            <div class="col-lg-10">
                                <select id="period" class="form-control">
                                    <option value='86400000' data-meterTitle="Day" data-meterUnit='dy'>Daily</option>
                                    <option value='604800000' data-meterTitle="Week" data-meterUnit='wk'>Weekly</option>
                                    <option value='2592000000' data-meterTitle="Month" data-meterUnit='mn'>Monthly</option>
                                    <option value='31104000000' data-meterTitle="Year" data-meterUnit='yr'>Yearly</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-4">
                                <input type="hidden" id="number" value=<?php echo empty($pageData['number'])?json_encode("all"):json_encode($pageData['number']); ?> />
                                <button id="updateCharts" class="btn btn-default">Update</button>
                            </div>
                            <div class="col-lg-4">
                                <button id="convertImage" class="btn btn-default">See as Image</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="normalCharts">
                    <div id="sentAndRecievedCountChart" class="chart" style="width: 280px; height: 250px;"></div>
                    <div id="msgCountChart" class="chart" style="width: 280px; height: 300px;"></div>
                    <div id="msgCostChart" class="chart" style="width: 280px; height: 300px;"></div>
                    <div id="msgPerPeroidChart" class="chart" style="width: 280px; height: 200px;"></div>
                </div>
                <div id="imageCharts">
                    
                </div>
            </div>
        </div>
        <a href="#"><span id="contactsTop" class="glyphicon glyphicon-chevron-up"></span></a>
        <a href="#"><span id="contactsDown" class="glyphicon glyphicon-chevron-down"></span></a>
        <a href="#"><span id="messagesTop" class="glyphicon glyphicon-chevron-up"></span></a>
        <a href="#"><span id="messagesDown" class="glyphicon glyphicon-chevron-down"></span></a>
        <a href="#"><span id="statsTop" class="glyphicon glyphicon-chevron-up"></span></a>
        <a href="#"><span id="statsDown" class="glyphicon glyphicon-chevron-down"></span></a>
    </div>
</body>
</html>
