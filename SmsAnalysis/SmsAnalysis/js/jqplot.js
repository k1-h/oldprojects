$(document).ready(function(){
    var ajaxDataRenderer = function(url, plot, options) {
        var ret = null;
        $.ajax({
            // have to use synchronous here, else the function 
            // will return before the data is fetched
            async: false,
            url: url,
            dataType:"json",
            success: function(data) {
                ret = data;
            }
        });
        //console.log(ret);
        return ret;
    };
    
    function pieChart(data)
    {
        $("div#sentAndRecievedCountChart").empty();
        $("div#sentAndRecievedCountChart").jqplot(data, {
            title: '',
            gridPadding: {top:0, bottom:38, left:0, right:0},
            seriesDefaults:{
                renderer:$.jqplot.PieRenderer, 
                trendline:{ show:false }, 
                rendererOptions: { padding: 8, showDataLabels: true }
            },
            grid: {
                drawBorder: false, 
                drawGridlines: false,
                background: 'rgba(0,0,0,0)',
                shadow:false
            },
            legend:{
                show:true, 
                location: 'ne'
            }
        });
    };
    
    function linerChart(data, id, title, format) 
    {
        $('div#' + id).empty(); //console.log(url);
        $('div#' + id).jqplot(data, {
            title: 'Message ' + title,
            axesDefaults: {
            tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                tickOptions: {
                  angle: -30,
                  fontSize:'9pt', 
                    fontFamily:'Tahoma', 
                }
            },
            axes: {
                xaxis: {
                    renderer:$.jqplot.CategoryAxisRenderer, 
                },
                yaxis:{
                    min:0,
                }
            },
            series: [ 
                {
                    rendererOptions: {
                        smooth: true
                    },
                    markerOptions: {
                        size: 7
                    },
                }
            ],
            highlighter: {
                show: true,
                sizeAdjust: 8,
                tooltipAxes: 'y',
                useAxesFormatters: false,
                tooltipFormatString: '<strong>%d</strong> ' + format,
            },
            cursor:{
                show: true,
                showTooltip: false,
                zoom:true,
                looseZoom: true
            }
        });
    };
    
    function meterGauge(data) 
    {
        $("div#msgPerPeroidChart").empty();
        $("div#msgPerPeroidChart").jqplot(data, {
           title: 'Message per ' + $('#period option:selected').data('metertitle'),
           seriesDefaults: {
               renderer: $.jqplot.MeterGaugeRenderer,
               rendererOptions: {
                   label: 'msg/' + $('#period option:selected').data('meterunit'),
               }
           },
           grid: {
                drawBorder: false, 
                drawGridlines: false,
                background: 'rgba(0,0,0,0)',
                shadow:false
           },
       });
    };
    
    function getChartsData(data, baseURL) 
    {console.log(baseURL);
        data.sentAndRecievedCountChart = ajaxDataRenderer(baseURL + 'sentAndRecievedCountChart');
        data.msgCountChart = ajaxDataRenderer(baseURL + 'msgCountChart');
        data.msgCostChart = ajaxDataRenderer(baseURL + 'msgCostChart');
        data.msgPerPeroidChart = ajaxDataRenderer(baseURL + 'msgPerPeroidChart');
    }
    
    function setInputValues(baseURL) 
    {
        inputValues = ajaxDataRenderer(baseURL + 'inputValues');
        /*$('#datePickerFrom').datepicker("setDate", inputValues[0][1]);
        $('#hiddenFrom').val(inputValues[0][1]);
        $('#datePickerTo').datepicker("setDate", inputValues[0][2]);
        $('#datePickerTo').datepicker(
            'option',
            'minDate',
            new JalaliDate(
                $('#hiddenFrom').val().split(',')[0], 
                $('#hiddenFrom').val().split(',')[1], 
                $('#hiddenFrom').val().split(',')[2]
            )
        );*/
        //$('#datePickerFrom').datepicker("setDate", new JalaliDate(1392,5,5));
        //$('#hiddenTo').val(inputValues[0][3]);
        //$('#period').val(inputValues[0][4]);
    }
    
    function setChartsData(data) 
    {
        pieChart(data.sentAndRecievedCountChart);
        linerChart(data.msgCountChart, 'msgCountChart', 'Count', 'Messages');
        linerChart(data.msgCostChart, 'msgCostChart', 'Cost', 'Toman');
        meterGauge(data.msgPerPeroidChart);
    }
   
    // The url for our json data
    var baseURL = 'index.php?number=' + $('#number').val() + '&chart=';
    var data = {};
    getChartsData(data, baseURL);
    setInputValues(baseURL);
    setChartsData(data);

    $('#convertImage').click(function(event) {
        event.preventDefault();
        $('div#normalCharts').children().each(function() {
            $(this).data('lastWidth', $(this).css('width'));
            $(this).data('lastHeight', $(this).css('height'));
            $(this).css('width', 500);
            $(this).css('height', 500);
            $(this).css('visibility', 'hidden');
        });
        
        setChartsData(data);
        $('div#normalCharts').children().each(function() {
            var imgsrc = $(this).jqplotToImageStr({backgroundColor: 'rgba(0, 0, 0 ,0)'});
            link = $('<a></a>', {
                href: imgsrc,
                target: '_blank'
            }).appendTo('div#imageCharts');
            $('<img>', {
                src: imgsrc
            }).appendTo(link);
        }).hide();
    });
    
    $('#updateCharts').click(function(event) {
        event.preventDefault();
        var dateFrom = $('#hiddenFrom').val();
        var dateTo = $('#hiddenTo').val();
        var period = $('#period').val();
        
        baseURL = 'index.php?number=' + $('#number').val() + '&dateFrom=' + dateFrom + '&dateTo=' + dateTo + '&period=' + period + '&chart=';
        getChartsData(data, baseURL);
        setChartsData(data);
    });
});