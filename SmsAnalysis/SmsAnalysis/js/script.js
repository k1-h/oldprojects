$(document).ready(function(){
	/************* Select Words When Clicking on Them *************/
	$("div#contactlist > div").click(function(){
		if(event.target.nodeName!='INPUT') {
			toggle=!$(this).children("input").attr('checked');
			$(this).children("input").attr('checked',toggle);
		}
	});

	/*************** Filter Lists With Written Keys ***************/
	$('#contactSearch').keyup(function(){
		var contactlist=$(this).closest('form').find(".contact");
		contactlist.hide().each(function() {
			var word=$(this).closest('form').find('#contactSearch').val().toLowerCase();
			if($(this).text().toLowerCase().indexOf(word)!=-1)
				$(this).show();
		});
	});
	 
	/*********** Selecet and Deselect All in Remove Tab ***********/
	$('#checkAll').click(function() {
		var checked=!$(this).data('check');
		var list=$(this).closest('form').find("."+$(this).data('list'));
		
		buttonText=(checked)?"Deselect All":"Select All";
		list.each(function(){
			$(this).attr('checked',checked);
		});
		$(this).data('check',checked);
		$(this).text(buttonText);
	});
    
    $('#datePickerFrom').datepicker({
        showButtonPanel: true,
        dateFormat: 'DD، d MM yy',
        autoSize: true,
        changeMonth: true,
        changeYear: true,
        onSelect: function(dateText, inst) {
            $('#datePickerTo').datepicker('option', 'minDate', new JalaliDate(inst['selectedYear'], inst['selectedMonth'], inst['selectedDay']));
            $('#hiddenFrom').val(new JalaliDate(inst['selectedYear'], inst['selectedMonth'], inst['selectedDay']));
        }
    });
    $('#datePickerTo').datepicker({
        showButtonPanel: true,
        dateFormat: 'DD، d MM yy',
        autoSize: true,
        changeMonth: true,
        changeYear: true,
        onSelect: function(dateText, inst) {
            $('#hiddenTo').val(new JalaliDate(inst['selectedYear'], inst['selectedMonth'], inst['selectedDay']));
        }
    });
    
    // http://manos.malihu.gr/jquery-custom-content-scroller/
    $(window).load(function(){
        $("ul#contacts, div#messages, div#stats").mCustomScrollbar({
            theme:"dark-thick",
            updateOnContentResize: true,
    
            autoHideScrollbar: true,
            scrollButtons:{
                enable:true
            },
            advanced:{
                updateOnBrowserResize: true,
                updateOnContentResize: true
            }
        });
    });
    
    $('.custom-upload input[type=file]').change(function(){
        $(this).next().find('input').val($(this).val());
    });
    
    $('#contactsTop').click(function(){
        $("ul#contacts").mCustomScrollbar("scrollTo","top");
    });
    $('#contactsDown').click(function(){
        $("ul#contacts").mCustomScrollbar("scrollTo","last");
    });
    $('#messagesTop').click(function(){
        $("div#messages").mCustomScrollbar("scrollTo","top");
    });
    $('#messagesDown').click(function(){
        $("div#messages").mCustomScrollbar("scrollTo","last");
    });
    $('#statsTop').click(function(){
        $("div#stats").mCustomScrollbar("scrollTo","top");
    });
    $('#statsDown').click(function(){
        $("div#stats").mCustomScrollbar("scrollTo","last");
    });
    
});