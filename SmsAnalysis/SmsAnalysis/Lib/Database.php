<?php
/**
 * This is only file has real access to database
 * and every database related code must be in this file.
 * 
 * @package SmsAnalysis
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/sms-analysis
 * 
 */
namespace SmsAnalysis\Lib;

/**
 * Database class  
 */
class Database
{
    /**
     * Instance of connection
     * @var object
     */
    private $_connection;
    
    /**
     * Prepared PDO Statement
     * @var object
     */
    private $_prepared;
    
    /**
     * Create new instance of DB
     * 
     * Creates a new instance of database and set it to class $_connection.
     * If $install is on don't select database because it is not created yes.
     * If something is wrong then throw an exception with proper message.
     * 
     * @param int $install Detemines whether code is in install mode or not
     * 
     * @throws \Exception If could not connect to database or select database
     */
    public function __construct($install = 0)    
    {
        $hostname = 'localhost';
        $database = 'smsanalysis';
        $username = 'root';
        $password = '';

        try {
            if (!$install) {
                $this->_connection = new \PDO(
                    'mysql:host=' . $hostname . ';dbname=' . $database . ';charset=utf8',
                    $username,
                    $password,
                    array(\PDO::ATTR_PERSISTENT => true)
                );
            } else { 
                $this->_connection = new \PDO(
                    'mysql:host=' . $hostname . ';charset=utf8',
                    $username,
                    $password,
                    array(\PDO::ATTR_PERSISTENT => true) 
                );
            }
            // Throw exception on error
            $this->_connection->setAttribute(
                \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION
            );
        } catch (\PDOException $e) {
            $code = $e->getCode(); 
            if ($code==1045) { 
                throw new \Exception(
                    " Method: " . __METHOD__ . " " .
                    "Connect error, check database config. " . $e->getMessage()
                );
            } else if ($code==1049) {
                throw new \Exception(
                    " Method: " . __METHOD__ . " " . 
                    "Unknown database. <br />
                    Did you installed database? 
                    If not install it form menu" . 
                    $e->getMessage()
                );
            } else {                
                throw new \Exception(" Method: " . __METHOD__ . " " . $e->getMessage());
            }
        }
    }

    /**
     * Installs database from a .sql file from disk.
     * 
     * Read each line of file and execute it.
     * 
     * @param string $filename Path of .sql file on disk.
     * 
     * @throws \Exception If counld not open sql file or execute queris
     */
    public function installDB($filename)
    {
        if (file_exists($filename)) {
            $f = fopen($filename, 'r');
            try {
                while (!feof($f)) {
                    $query = fgets($f);
                    $this->_connection->exec($query);
                }
                fclose($f);
                $this->_connection = null;
            } catch (\PDOException $e) {
                throw new \Exception(
                    " Method: " . __METHOD__ . " " . 
                    "Bad Formated or Not SQL File. " . 
                    $e->getMessage()
                );
            }
        } else {
            throw new \Exception(
                " Method: " . __METHOD__ . " " . 
                "Could not Open Input File."
            );
        }
    }

    /**
     * Drop database and old connection
     * 
     * @throws \Exception If counld not drop database
     */
    public function removeDB()
    {
        try {
            $this->_connection->exec("DROP DATABASE IF EXISTS `smsanalysis`;");
            $this->_connection = null; 
        } catch (\PDOException $e) {
            throw new \Exception(
                " Method: " . __METHOD__ . " " .
                "Database Remove Problem. " . 
                $e->getMessage()
            );
        }
    }

    /**
     * Delete all database data or just specified table.
     * 
     * Uses MySql transactions for safety
     * 
     * @param string $table Name of table should be removed
     * 
     * @throws \Exception If counld not remove database data
     */
    public function removeData($table = null)
    {
        try {
            $this->beginTransaction();
            if ($table) {
                $this->_connection->exec('DELETE FROM ' . $table . ' WHERE 1=1;');
            } else {
                $this->_connection->exec('DELETE FROM messages WHERE 1=1;');
                $this->_connection->exec('DELETE FROM contacts WHERE 1=1;');
                $this->_connection->exec('DELETE FROM stats WHERE 1=1;');
            }
            $this->commit();
        } catch (\PDOException $e) {
            $this->rollBack();
            throw new \Exception(
                " Method: " . __METHOD__ . " " .
                "Could not remove data. " . 
                $e->getMessage()
            );
        }
    }
    
    /**
     * Begins MySql transaction
     * 
     * Sometimes other classes may need to begin transaction.
     * This will help them.
     */
    public function beginTransaction()
    {
        $this->_connection->beginTransaction();
    }
    
    /**
     * Commits MySql transaction
     * 
     * Sometimes other classes may need to Commit transaction.
     * This will help them.
     */
    public function commit()
    {
        $this->_connection->commit();
    }

    /**
     * Roll Backs MySql transaction
     * 
     * Sometimes other classes may need to Roll Back transaction.
     * This will help them.
     */
    public function rollBack()
    {
        $this->_connection->rollBack();
    }

    /**
     * Prepares database to insert data
     * 
     * Converts $cols array to string and make correct number placeholders 
     * 
     * @param string $table Name of table data will inserted
     * @param array  $cols  Name of column that data will inserted
     */
    public function prepareInsert($table, $cols)
    {
        $strCols =  implode(',', $cols);
        $values = implode(',', array_fill(0, count($cols), '?'));
        $this->_prepared = $this->_connection->prepare(
            'INSERT IGNORE ' . 
            $table . 
            ' (' . 
            $strCols . 
            ') VALUES (' . 
            $values . 
            ');'
        );
    }

    /**
     * Execute prepared statement with given values
     * 
     * @param array $values Array of values to be inserted 
     */
    public function executePrepared($values)
    {
        $this->_prepared->execute($values);
    }

    /**
     * Execute prepared query for stats table and fetch all result
     * 
     * @param array $dateRange Range of date
     * 
     * @return array Array of count and sum of date and message count in given date range
     */
    public function queryPrepared($values)
    {
        $this->_prepared->execute($values);
        return $this->_prepared->fetchAll(\PDO::FETCH_COLUMN);
    }

    /**
     * Check if tables are empty or not
     * 
     * @throws \Exception If there is no data in tables
     */
    public function checkEmpty() 
    {
        $messages = $this->query(
            array(
                "table" => "messages",
                "column" => array("date"),
                "limit" => 1
            )
        );
        $contacts = $this->query(
            array(
                "table" => "contacts",
                "column" => array("number"),
                "limit" => 1
            )
        );
        $stats = $this->query(
            array(
                "table" => "stats",
                "column" => array("number"),
                "limit" => 1
            )
        );
        if (empty($messages) or empty($contacts) or empty($stats)) {
            throw new \Exception(
                " Method: " . __METHOD__ . " " .
                "Null data, did you uploaded data file?
                 if no upload data from menu."
            );
        }
    }

    /**
     * Main method to query data from database.
     * 
     * @param mixed $queryData Query data like table name, column etc.
     * @param int   $fetchMode Mode of fetching date, default is FETCH_BOTH
     * 
     * @throws \Exception If null data returned
     * 
     * @return array|void Array of result data
     */
    public function query($queryData, $fetchMode = \PDO::FETCH_BOTH, $prepare = false)
    {
        $query = "SELECT ";
        if (!empty($queryData["distinct"])) {
            $query .= "DISTINCT ";
        }
        foreach ($queryData["column"] as $key => $column) {
            if (!empty($queryData["function"])) {
                $query .= $queryData["function"] . "(" . $column . ") AS " . $column . ", ";
            } else {
                $query .= $column . ", ";
            }
        }
        $query = rtrim($query, ", ");
        $query .= " FROM " . $queryData["table"];
        if (!empty($queryData["conditions"])) {
            $query .= " WHERE " . $queryData["conditions"];
        }
        if (!empty($queryData["order"])) {
            $query .= " ORDER BY " . $queryData["order"]["column"] . " "  . $queryData["order"]["order"];
        }
        if (!empty($queryData["limit"])) {
            if (!empty($queryData["page"])) {
                $page = $queryData["page"] * $queryData["limit"] - $queryData["limit"];
                $query .= " LIMIT " . $page . ", " . $queryData["limit"];
            } else {
                $query .= " LIMIT " . $queryData["limit"];
            }
        }
        $query .= ";";
        try {
            if ($prepare) {
                $this->_prepared = $this->_connection->prepare($query);
            } else { //echo $query;
                $result = $this->_connection->query($query);
                return $result->fetchAll($fetchMode);
            }
        } catch (PDOException $e) {
            throw new \Exception(
                " Method: " . __METHOD__ . " " .
                "Null data returned. " .
                $e->getMessage() .
                $query
            );
        }
    }
}
