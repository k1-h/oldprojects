<?php
/**
 * Get data from database
 * 
 * @package SmsAnalysis
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/sms-analysis
 */
namespace SmsAnalysis\Lib;

use SmsAnalysis\Lib\Database;

/**
 * GetData class
 */
class GetData
{
    /**
     * Instance of Database class
     * @var object
     */
    private $_db;
    
    /**
     * Get new instance of database and
     * Check if data is empty, if yes show error
     * 
     * @uses Database Class to get data
     */
    public function __construct()
    {
        $this->_db = new Database();
        $this->_db->checkEmpty(); 
    }
    
    /**
     * Get name and number of all contacts
     * 
     * First get all contancts then make a dictionary array and return new array
     * 
     * @return array Array contact name and numbers
     */
    public function getContacts()
    { 
        $dictionaryContacts = array();
        $contacts = $this->_db->query(
            array(
                "table"  => "contacts",
                "column" => array("name", "number"),
                "order"  => array("column" => "name", "order" => "ASC")
            )
        );
        foreach ($contacts as $contact) {
            $dictionaryContacts[$contact['number']] = $contact['name'];
        }

        return $dictionaryContacts;
    }
    
    /**
     * Get all messages or specified contact messages.
     * 
     * If user specified a contact, get contact's messages
     * Else get all messages.
     * 
     * @param int    $page   Number of starting record
     * @param string $number Number of contact
     * @param int    $limit  Limit result messages
     * 
     * @return array Array of result messages
     */
    public function getMessages($page, $number = null, $limit = 100)
    {
        if ($number) { 
            $messages = $this->_db->query(
                array(   
                    "table"      => "messages",
                    "column"     => array("*"),
                    "conditions" => "number = '" . $number . "'",
                    "order"      => array("column" => "date", "order" => "ASC"),
                    "page"       => $page,
                    "limit"      => $limit
                )
            );
        } else {
            $messages = $this->_db->query(
                array(   
                    "table"  => "messages",
                    "column" => array("*"),
                    "order"  => array("column" => "date", "order" => "ASC"),
                    "page"   => $page,
                    "limit"  => $limit
                )
            );
        }

        return $messages;
    }
    
    /**
     * Get total stats or specified contact's stat
     * 
     * If not specified a contact name get all data by summing 
     * stats of all contacts
     * 
     * @param string $number Number of specified contact to get stats
     * 
     * @return array Array of stats
     */
    public function getStats($number = null)
    {
        if ($number) {
            $stats = $this->_db->query(
                array(
                    "table"      => "stats",
                    "column"     => array ("recievedCount", "recievedCost", "sentCount", "sentCost"),
                    "conditions" => "number = '" . $number . "'"
                )
            );
        } else {
            $stats = $this->_db->query(
                array(
                    "table"    => "stats",
                    "column"   => array ("recievedCount", "recievedCost", "sentCount", "sentCost"),
                    "function" => "SUM"
                )
            );
        }

        return $stats;
    }
}
