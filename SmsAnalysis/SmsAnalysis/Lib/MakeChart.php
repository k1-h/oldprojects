<?php
/**
 * Insert uploaded file to data
 * 
 * @package SmsAnalysis
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/sms-analysis
 * 
 */
namespace SmsAnalysis\Lib;

use SmsAnalysis\Lib\Database;
use EZUtil\Date\Jalali;	
use SmsAnalysis\Lib\Utility;
use SmsAnalysis\Lib\DateUtilities;

class MakeChart
{
    
    /**
     * Instance of Database class
     * @var object
     */
    private $_db;
    
    /**
     * @var int
     */
    private $dateEpochFrom;
    
    /**
     * @var int
     */
    private $dateEpochTo;
    
    /**
     * @var int
     */
    private $period;
    
    /**
     * __construct
     */
    public function __construct($data) 
    {
        $this->_db = new Database();
        $this->_db->checkEmpty();
        $number = ($data['number'] == "all")?"%":$data['number'];
        
        if (!empty($data['dateFrom'])) {
            $this->dateEpochFrom = DateUtilities::jalaliToEpoch($data['dateFrom'], true);         
        } else {
            $this->dateEpochFrom = $this->calculateDateFrom($number);
        }
        
        if (!empty($data['dateTo'])) {
            $this->dateEpochTo = DateUtilities::jalaliToEpoch($data['dateTo'], true);
        } else {
            $this->dateEpochTo = $this->calculateDateTo($number);
        }

        if (!empty($data['period'])) {
            $this->period = $data['period'];
        } else {
            $this->period = DateUtilities::calculatePeriod($this->dateEpochFrom, $this->dateEpochTo);
        }
        $this->dateEpochFrom = DateUtilities::dateJalaliFloor($this->dateEpochFrom, $this->period);
        $this->dateEpochTo = DateUtilities::dateJalaliCeiling($this->dateEpochTo, $this->period);
        
        switch ($data['chart']) {
            case 'sentAndRecievedCountChart':
                $chartData = $this->sentAndRecievedChart($number, "msgCount");
                break;
            case 'msgCountChart':
                $chartData = $this->msgChart($number, "msgCount");
                break;
            case 'msgCostChart':
                $chartData = $this->msgChart($number, "msgCost");
                break;
            case 'msgPerPeroidChart':
                $chartData = $this->msgChart($number, "msgPerPeroid");
                break;
            case 'inputValues':
                $chartData = array(
                    DateUtilities::epochToJalali($this->dateEpochFrom, " l، j F Y", false, false),
                    DateUtilities::epochToJalali($this->dateEpochFrom, "Y,n,j", false, false),
                    DateUtilities::epochToJalali($this->dateEpochTo, " l، j F Y", false, false),
                    DateUtilities::epochToJalali($this->dateEpochTo, "Y,n,j", false, false),
                    $this->period
                );
        }
        
        header('Content-type: application/json');
        echo json_encode(array($chartData));
        die();
    }
    
    private function sentAndRecievedChart($number, $chart)
    {
        $recieved = $this->_db->query(
            array(
                "table"      => "messages",
                "column"     => array("date"),
                "function"   => "COUNT",
                "conditions" => "number LIKE '" . $number . "' AND type='Received' " .
                                "AND date >= " . $this->dateEpochFrom . " AND date <= " .
                                $this->dateEpochTo
            ),
            \PDO::FETCH_COLUMN
        )[0];
        $sent = $this->_db->query(
            array(
                "table"      => "messages",
                "column"     => array("date"),
                "function"   => "COUNT",
                "conditions" => "number LIKE '" . $number . "' AND type='Sent' " .
                                "AND date >= " . $this->dateEpochFrom . " AND date <= " .
                                $this->dateEpochTo
            ),
            \PDO::FETCH_COLUMN
        )[0];
        
        $chartData = array(
            array("Recieved", (int) $recieved),
            array("Sent", (int) $sent)
        );
        
        return $chartData;
    }
    
    private function msgChart($number, $chart) 
    {
        $dateRange = $this->makeDateRange($this->dateEpochFrom, $this->dateEpochTo, $this->period);
        
        if ($chart == "msgCount" or $chart == 'msgPerPeroid') {
            $column = "date";
            $function = "COUNT";
        } elseif ($chart == "msgCost") {
            $column = "msgCost";
            $function = "SUM";
        }
        $this->_db->query(
            array(
                "table"      => "messages",
                "column"     => array($column),
                "function"   => $function,
                "conditions" => 'date >= ? AND date <= ? AND number LIKE "' . $number .'"'
            ),
            \PDO::FETCH_COLUMN,
            true
        );
        $chartData = array();
        $sum = 0;
        foreach ($dateRange as $range) {
            if ($chart == "msgCount" or $chart == "msgCost") {
                $chartData[] = array(
                    $this->makePeriodStr($range[0]),
                    (int) $this->_db->queryPrepared($range)[0]
                );                
            } elseif ($chart == 'msgPerPeroid') {
                $sum += (int) $this->_db->queryPrepared($range)[0];
            }
        }
        if ($chart == 'msgPerPeroid') {
            $chartData[0] = (int) ($sum / count($dateRange));
        }
        
        return $chartData;
    }
    
    /**
     * makeQueryConditions
     */
    private function makeDateRange($dateEpochFrom, $dateEpochTo, $period) 
    {
        $dateRange = array();
        $currEpochFrom = $dateEpochFrom;
        $currEpochTo = $dateEpochFrom + $period + DateUtilities::calculatePeriodDiff($dateEpochFrom, $period);
        while($currEpochTo < $dateEpochTo) {
            $dateRange[] = array($currEpochFrom, $currEpochTo);
            $currEpochFrom = $currEpochTo + 1; 
            $currEpochTo += $period + DateUtilities::calculatePeriodDiff($currEpochFrom, $period);
        }
        if($currEpochFrom <= $dateEpochTo) {
            $dateRange[] = array($currEpochFrom, $dateEpochTo);
        }
        return $dateRange;
    }

    private function calculateDateFrom($number) 
    {
        $dateEpochFrom = $this->_db->query(
            array(
                "table"      => "messages",
                "column"     => array("date"),
                "conditions" => "number LIKE '" . $number ."'",
                "order"      => array("column" => "date", "order" => "ASC"),
                "limit"      => 1
            ),
            \PDO::FETCH_COLUMN
        )[0];

       return $dateEpochFrom;
    }
    
    private function calculateDateTo($number)
    {
        $dateEpochTo = $this->_db->query(
            array(
                "table"      => "messages",
                "column"     => array("date"),
                "conditions" => "number LIKE '" . $number ."'",
                "order"      => array("column" => "date", "order" => "DESC"),
                "limit"      => 1
            ),
            \PDO::FETCH_COLUMN
        )[0];
        
        return $dateEpochTo;
    }
    
    private function makePeriodStr($date) 
    {
        switch ($this->period) {
            case DateUtilities::$EPOCH_DAY:
                $msg = DateUtilities::epochToJalali($date, "y/n/j", false, false);
                break;
            case DateUtilities::$EPOCH_WEEK:
                $msg = DateUtilities::epochToJalali($date, "y/n/j", false, false);
                break;
            case DateUtilities::$EPOCH_MONTH:
                $msg = DateUtilities::epochToJalali($date, "y F", false, false);
                break;
            case DateUtilities::$EPOCH_YEAR:
                $msg = DateUtilities::epochToJalali($date, "y", false, false);
                break;
            default:
                $msg = null;
        }
        
        return $msg;
    }
}
