<?php
/**
 * Insert uploaded file to data
 * 
 * @package SmsAnalysis
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/sms-analysis
 * 
 */
namespace SmsAnalysis\Lib;

use XmlIterator\XmlIterator;
use SmsAnalysis\Lib\Database;

/**
 *  Get path of an xml file then
 *  Convert it to array
 *  Prepare it to insert
 *  Insert messages to DB
 *  Insert contacts to DB
 *  Calculate and insert stats to DB
 */
class InsertData
{
    /**
     * Instance of Database class
     * @var object
     */
    private $_db;

    /**
     * Insert xml file data to database and recalculate stats
     * 
     * Read xml, return xml object
     * Prepare messages to inserted to Database
     * Insert messages to Database
     * Insert contacts to Database
     * Calculate and insert stats to DB
     * 
     * @uses Database Class to insert messages
     * @uses XmlIterator\XmlIterator To read xml and return array
     * 
     * @param string $file        Path of xml file to be imported
     * @param int    $extractContacts When is true import contact name directly from xml file
     * 
     * @throws \Exception If something was wrong with file
     */
    public function __construct($file, $extractContacts)
    {
        $this->_db = new Database();

        $xml = new XmlIterator("data/" . $file, "sms");
        if (!$xml) {
            throw new \Exception(
                " Method: " . __METHOD__ . " " .
                "Input file error, could not convert xml to array."
            );
        }
        $messages = $this->prepareMessages($xml);
        if (!$messages) {
            throw new \Exception(
                " Method: " . __METHOD__ . " " .
                "Input file error, could not prepare array."
            );
        }
        $this->insertMessages($messages);
        $contacts = $extractContacts?$this->extractContacts($messages):null;
        $this->insertNumbers($contacts); 
        $this->insertStats();
    }

    /**
     * Actually detects encoding of text
     * If encoding is ascii then return english
     * Else If encoding is UTF-8 return persian
     * 
     * @param string $text Text to 
     * 
     * @return string Language of text, enlish or persian
     */
    private function detectLang($text)
    {
        $encoding = mb_detect_encoding($text);
        if ($encoding == "ASCII") {
            return "english";
        } elseif ($encoding == "UTF-8") {
            return "persian";
        }
    }
    
    private function validateDate($readable_date)
    {
        $date = new \DateTime($readable_date);
        $today = new \DateTime();
        $fiveYearAge = new \DateTime("-5 year");
        
        if ($date > $today or $date < $fiveYearAge) {
            return false;            
        }
        return true;
    }

    /**
     * Prepare messages array to be inserted to database.
     * 
     * Fields in the XML File
     *   address – The phone number of the sender/recipient.
     *   date – The Java date representation (including millisecond) of the time when the message was sent/received. 
     *          Check out www.epochconverter.com for information on how to do the conversion 
     *          from other languages to Java.
     *   type – 1 = Received, 2 = Sent, 3 = Draft, 4 = Outbox, 5 = Failed, 6 = Queued
     *   body – The content of the message.
     *   service_center – The service center for the received message, null in case of sent messages.
     *   read – Read Message = 1, Unread Message = 0.
     *   status – None = -1, Complete = 0, Pending = 32, Failed = 64.
     *   readable_date – Optional field that has the date in a human readable format.
     *   contact_name – Optional field that has the name of the contact.
     * Convert address field
     *   If number start with 0
     *    Then remove 0 and add 98 to first of it
     *   Else if number start with +
     *    Then just remove +
     *   Remove spaces and dashes from number
     * Convert date to jalali date
     * Decode type value
     * Detect language of message
     * Sanitize message body
     * If lang is farsi, Get lenth of message and divide it to 70 to count of message 
     *   Then multiple count to 10 to get cost of message
     * If lang is english do same but divide it to 160 and multiple to 16
     * Decode read value
     * Decode status value
     * Date of insertion
     * Add prepared $sms to new array
     * 
     * @param mixed $xml array of data to be prepared
     * 
     * @return mixed Prepared array to be inserted to database
     */
    private function prepareMessages($xml)
    {
        $messages = array();

        foreach ($xml as $key => $sms) {
            $sms = $sms['@attributes'];
            
            if (!$this->validateDate($sms['readable_date'])) {
                continue;
            }
            if (substr($sms['address'], 0, 1) == "0") {
                $sms['address'] = "98" . substr($sms['address'], 1);
            } elseif (substr($sms['address'], 0, 1) == "+") {
                $sms['address'] = substr($sms['address'], 1);
            }
            $sms['address'] = str_replace(' ', '', $sms['address']);
            $sms['address'] = str_replace('-', '', $sms['address']);
            
            $sms['jalali_date'] = Utility::epochToJalali($sms['date']);
            
            switch ($sms['type']) {
                case 1:
                    $sms['type'] = 'Received';
                    break;
                case 2:
                    $sms['type'] = 'Sent';
                    break;
                case 3:
                    $sms['type'] = 'Draft';
                    break;
                case 4:
                    $sms['type'] = 'Outbox';
                    break;
                case 5:
                    $sms['type'] = 'Failed';
                    break;
                case 6:
                    $sms['type'] = 'Queued';
                    break;
            }
            $sms['lang'] = $this->detectLang($sms['body']);
            $sms['body'] = htmlspecialchars($sms['body']);

            if ($sms['lang'] == 'persian' || $sms['lang'] == 'arabic' || $sms['lang'] == 'farsi') {
                $sms['msgCount'] = (int) (mb_strlen($sms['body'], 'UTF-8') / 70) + 1;
                $sms['msgCost'] = $sms['msgCount'] * 10;
            } else {
                $sms['msgCount'] = (int) (mb_strlen($sms['body'], 'UTF-8') / 160) + 1;
                $sms['msgCost'] = $sms['msgCount'] * 16;
            }
            if ($sms['read'] == 1) {
                $sms['read'] = 'Read';
            } else {
                $sms['read'] = 'Unread';
            }
            switch ($sms['status']) { 
                case -1:
                    $sms['status'] = 'None';
                    break;
                case 0:
                    $sms['status'] = 'Complete';
                    break;
                case 32:
                    $sms['status'] = 'Pending';
                    break;
                case 64:
                    $sms['status'] = 'Failed';
                    break;
            }
            $sms['created'] = date("Y-m-d H:i:s");

            $messages[] = $sms;
        }

        return $messages;
    }

    /**
     * Insert messages array to database
     * 
     * Begin transaction
     * Prepare database to insert
     * Because some collision may happen on messages with same date and number
     *  We need to change date a bit
     * Just select and insert usefull data to DB
     * Insert message
     * Commit inserts
     * If something went wrong, catch it
     *  Then roll back changes
     *  And show a proper error message, with throwing new exception
     * 
     * @param mixed $messages array of messages to be inserted
     */
    private function insertMessages($messages)
    {
        $this->_db->beginTransaction();
        $cols = array(
            "number",
            "date",
            "jalali_date",
            "type",
            "lang",
            "body",
            "msgCount",
            "msgCost",
            "service_center",
            "readed",
            "status",
            "created"
        );
        try {
            $this->_db->prepareInsert("messages", $cols);
            foreach ($messages as $key=>$sms) {
                $filterdMessage = array(
                    $sms["address"],
                    $sms["date"],
                    $sms["jalali_date"],
                    $sms["type"],
                    $sms["lang"],
                    $sms["body"],
                    $sms["msgCount"],
                    $sms["msgCost"],
                    $sms["service_center"],
                    $sms["read"],
                    $sms["status"],
                    $sms["created"]
                );
                $this->_db->executePrepared($filterdMessage); 
            }
            $this->_db->commit(); 
        } catch (\PDOException $e) { 
            $this->_db->rollBack();  
            throw new \Exception(
                "Database error, 
                could not insert data in messages table. " . 
                $e->getMessage()
            );
        }
    }

    /**
     * Extract contacts from xml message array
     * 
     * @param mixed $messages array of messages to be inserted
     * 
     * @return mixed Array of contact name and numbers
     */
    private function extractContacts($messages)
    {
        $contacts = array();
        foreach ($messages as $message) {
            if (!empty($message['contact_name']) and ($message['contact_name'] != "(Unknown)")) {
                $contacts[$message['address']] = $message['contact_name'];
            } else {
                $contacts[$message['address']] = $message['address'];
            }
        }

        return $contacts;
    }

    /**
     * Insert contacts to DB
     * 
     * Get list of numbers which are in messages table and not in contacts table
     * Fetch method shall return only a single requested column from the next row in the result set
     * Prepare database to insert
     * Commit queries
     * If something went wrong, catch it
     *  Then roll back changes
     *  And show a proper error message, with throwing new exception
     * 
     * @param mixed $contacts Array of contact name and numbers
     * 
     * @todo Fix bug when contact name is already in table
     * 
     * @throws \Exception If could not insert data
     */
    private function insertNumbers($contacts)
    {
        $numbers = $this->_db->query(
            array(
                "table"      => "messages",
                "distinct"   => true,
                "column"     => array("number"),
                "conditions" => "number NOT IN (SELECT number FROM contacts)"
            ),
            \PDO::FETCH_COLUMN
        );
        try {
            $this->_db->beginTransaction();
            $cols = array("number", "name");
            $this->_db->prepareInsert("contacts", $cols);
            foreach ($numbers as $key => $number) {
                if (!empty($contacts[$number])) {
                    $contact = array($number, $contacts[$number]);
                } else {
                    $contact = array($number,$number);
                }
                $this->_db->executePrepared($contact);
            }
            $this->_db->commit();
        } catch (\PDOException $e) {
            $this->_db->rollBack();
            throw new \Exception(
                " Method: " . __METHOD__ . " " .
                "Database error, 
                could not insert data in contacts table. " . 
                $e->getMessage()
            );
        }
    }

    /**
     * Calculate and insert stats
     * 
     * Get list of numbers
     * Get count of recieved message from specified number
     * Get cost of recieved message from specified number
     * Get count of sent message to specified number
     * Get cost of sent message to specified number
     * Fetch method shall return only a single requested column from the next row in the result set
     * Add calculated stat to stats array
     * Remove old stats
     * Prepare database to insert
     * Insert stats
     * Commit queries
     * If something went wrong, catch it
     *  Then roll back changes
     *  And show a proper error message, with throwing new exception
     * 
     * @throws \Exception If could not insert data
     */
    private function insertStats()
    {
        $stats = array();
        $numbers = $this->_db->query(
            array(
                "table"  => "contacts",
                "column" => array("number")
            )
        );
        foreach ($numbers as $number) {
            $temp = array();
            $temp['number'] = $number[0];
            $temp['recievedCount'] = $this->_db->query(
                array(
                    "table"      => "messages",
                    "column"     => array("date"),
                    "function"   => "COUNT",
                    "conditions" => "number = '" . $number[0] . "' and type='Received'"
                ),
                \PDO::FETCH_COLUMN
            )[0];
            $temp['recievedCost'] = $this->_db->query(
                array(
                    "table"      => "messages",
                    "column"     => array("msgCost"),
                    "function"   => "SUM",
                    "conditions" => "number = '" . $number[0] . "' and type='Received'"
                ),
                \PDO::FETCH_COLUMN
            )[0];
            $temp['sentCount'] = $this->_db->query(
                array(
                    "table"      => "messages",
                    "column"     => array("date"),
                    "function"   => "COUNT",
                    "conditions" => "number = '" . $number[0] . "' and type='Sent'"
                ),
                \PDO::FETCH_COLUMN
            )[0];
            $temp['sentCost'] = $this->_db->query(
                array(
                    "table"      => "messages",
                    "column"     => array("msgCost"),
                    "function"   => "SUM",
                    "conditions" => "number = '" . $number[0] . "' and type='Sent'"
                ),
                \PDO::FETCH_COLUMN
            )[0];

            $stats[] = $temp;
        }
        $this->_db->removeData('stats');
        $this->_db->beginTransaction();
        $cols = array(
            "number",
            "recievedCount",
            "recievedCost",
            "sentCount",
            "sentCost"
        );
        try {
            $this->_db->prepareInsert("stats", $cols);
            foreach ($stats as $stat) {
                $this->_db->executePrepared(array_values($stat));
            }
            $this->_db->commit();
        } catch (\PDOException $e) {
            $this->_db->rollBack();
            throw new \Exception(
                " Method: " . __METHOD__ . " " .
                "Database error, 
                could not insert data in stats table. " . 
                $e->getMessage()
            );
        }
    }
}
