DROP DATABASE IF EXISTS `smsanalysis`;
CREATE DATABASE `smsanalysis` DEFAULT CHARACTER SET utf8 COLLATE utf8_persian_ci;
USE `smsanalysis`;
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (`date` varchar(80) NOT NULL, `number` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,`jalali_date` varchar(80) CHARACTER SET utf8 COLLATE utf8_persian_ci NULL,`type` varchar(20) NOT NULL,`lang` varchar(20) NULL,`body` TEXT CHARACTER SET utf8 COLLATE utf8_persian_ci NULL,`msgCount` int(11) NULL,`msgCost` int(11) NULL,`service_center` varchar(20) NULL,`readed` varchar(20) NOT NULL,`status` varchar(20) NOT NULL, `created` datetime NOT NULL, PRIMARY KEY (`date` , `number`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (`number` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,`name` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci NULL, PRIMARY KEY (`number`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `stats`;
CREATE TABLE `stats` (`number` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,`recievedCount` int(11) NULL, `recievedCost` int(11) NULL, `sentCount` int(11) NULL, `sentCost` int(11) NULL,PRIMARY KEY (`number`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1;