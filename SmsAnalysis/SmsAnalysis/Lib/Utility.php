<?php
/**
 * @package SmsAnalysis
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/sms-analysis
 */
namespace SmsAnalysis\Lib;

use SmsAnalysis\Lib\GetData;
use SmsAnalysis\Lib\Database;
use SmsAnalysis\Lib\InsertData;
use SmsAnalysis\Lib\MakeChart;

Class Utility
{
    public static function getPageData($pageArgs) 
    {
        $pageData = array();
        
        $pageData['currPage'] = empty($pageArgs['p'])?1:$pageArgs['p'];
        $getData = new GetData();
        // Get all contacts names from database contacts table
        $pageData['contacts'] = $getData->getContacts();
        // If user specified a contact just show contact's messages and stats
        if (!empty($pageArgs['number'])) {
            $pageData['number'] = $pageArgs['number'];
            $pageData['messages'] = $getData->getMessages($pageData['currPage'], $pageData['number']);
            $pageData['stats'] = $getData->getStats($pageData['number']);
        } else {// Else get and show all mesaages
            $pageData['number'] = null;
            $pageData['messages'] = $getData->getMessages($pageData['currPage']);
            $pageData['stats'] = $getData->getStats();
        }
        $pageData['pageCount'] = (int) (($pageData['stats'][0]['recievedCount'] + $pageData['stats'][0]['sentCount']) / 100) + 1;
        $pageData['pageRange'] = Utility::makePageRange($pageData['currPage'], $pageData['pageCount']);
        //$pageData['chart'] = Utility::makeChart($pageData['number'],1);
        
        return $pageData;
    }
    
    public static function doAction($get, $post, $file) 
    {
        if (!empty($post['removeData'])) {
            $db = new Database();
            $db->removeData();
            Utility::setMessage("Data Successfully Removed", "success");
        } elseif (!empty($post['removeDB'])) {
            $db = new Database();
            $db->removeDB();
            Utility::setMessage("Database Successfully Removed.", "success");
        } elseif (!empty($file["file"]["tmp_name"])) {
            Utility::insertData($file, $post);
        } elseif (!empty($post['filename'])) {
            $db = new Database(1);
            $db->installDB($post['filename']);
            Utility::setMessage("Successfully Installed Database File.", "success");
        } elseif (!empty($get['number']) and !empty($get['chart'])) {
            new MakeChart($get);
        }
    }
    
    public static function insertData($file, $post) 
    {
        if (!file_exists("data/" . $file["file"]["name"])) {
            move_uploaded_file(
                $file["file"]["tmp_name"],
                "data/" . $file["file"]["name"]
            );
        }
        $extractContacts = empty($post['extractContacts'])?0:1;
        new InsertData($file["file"]["name"], $extractContacts);
        Utility::setMessage("Successfully Inserted Data File.", "success");
    }
    
    /**
     * redirect
     */
    public static function redirect($newLoc)
    {
        header("Location: $newLoc");
    }

    /**
     * setMessage
     */
    public static function setMessage($msg, $type)
    {
        $_SESSION['msg'] = $msg;
        $_SESSION['msgType'] = $type;
    }

    /**
     * clearMessage
     */
    public static function clearMessage()
    {
        $_SESSION['msg'] = null;
        $_SESSION['msgType'] = null;
    }

    /**
     * setSession
     */
    public static function setSession($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * getSession
     */
    public static function getSession($key)
    {
        if (!empty($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return null;
        }
    }

    /**
     * getCurrentURL
     */
    public static function getCurrentURL($number = null)
    {
        $currURL = "index.php";
        if ($number) {
            $currURL .= "?number=" . $number . "&p=";
        } else {
            $currURL .= "?p=";
        }

        return $currURL;
    }

    /**
     * makePageRange
     */
    public static function makePageRange($page, $pageCount)
    {
        $pageRange = array();
        if ($page < 3) {
            $start = 1;
        } elseif ($pageCount - $page < 3) {
            $start = $page - 4 + ($pageCount - $page);
        } else {
            $start = $page - 2;
        }
        for ($i = 0; $i < 5 and ($start + $i) < $pageCount; $i++) {
            $pageRange[] = $start + $i;
        }

        return $pageRange;
    }

    /**
     * getElement
     */
    public static function getElement($element, $param = null)
    {
        include "Elements/$element.php";
    }
}
