<?php
/**
 * @package SmsAnalysis
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/sms-analysis
 */
namespace SmsAnalysis\Lib;

use EZUtil\Date\Jalali;

Class DateUtilities
{
    public static $EPOCH_DAY = 86400000;
    public static $EPOCH_WEEK = 604800000;
    public static $EPOCH_MONTH = 2592000000;
    public static $EPOCH_YEAR = 31104000000;
    public static $JALALI_MONTH_EPOCH_DIFF = array(
        86400000, 86400000, 86400000, 86400000, 86400000, 86400000,
        0, 0, 0, 0, 0, -86400000
    );
    
    public static function calculatePeriod($dateEpochFrom, $dateEpochTo)
    {
        $dateEpochDiff = $dateEpochTo - $dateEpochFrom;
        
        if ($dateEpochDiff < 10 * DateUtilities::$EPOCH_DAY) {
            return DateUtilities::$EPOCH_DAY;
        } elseif ($dateEpochDiff < 10 * DateUtilities::$EPOCH_WEEK) {
            return DateUtilities::$EPOCH_WEEK;
        } elseif ($dateEpochDiff < 10 * DateUtilities::$EPOCH_MONTH) {
            return DateUtilities::$EPOCH_MONTH;
        } elseif ($dateEpochDiff < 10 * DateUtilities::$EPOCH_YEAR) {
            return DateUtilities::$EPOCH_YEAR;
        }
        return DateUtilities::$EPOCH_YEAR;
    }
    
    public static function calculatePeriodDiff($epoch, $period) 
    {
        if ($period == DateUtilities::$EPOCH_MONTH) {
            $jalaliMonth = DateUtilities::epochToJalali($epoch, 'n', false, false);
            $kabise = (DateUtilities::epochToJalali($epoch, 'Y', false, false) % 4) == 3;
            if ($kabise and $jalaliMonth == 12) {
                return DateUtilities::$JALALI_MONTH_EPOCH_DIFF[$jalaliMonth - 2];
            } else {
                return DateUtilities::$JALALI_MONTH_EPOCH_DIFF[$jalaliMonth - 1];
            }
        } else {
            return 0;
        }
    }
    
    /**
     * jalaliToEpoch
     */
    public static function jalaliToEpoch($jalaliStr, $fixBug = false)
    {
        $jalaliArray = explode(",", $jalaliStr);
        $jalaliArray[1] = ($fixBug)?$jalaliArray[1]++:$jalaliArray[1]; // javascript jalali date bug
        $jalali_obj = new Jalali($jalaliArray[0], $jalaliArray[1], $jalaliArray[2]);
        $gregorianDate = $jalali_obj->getGregorian()->format("Y-n-j");
        $datetime = new \DateTime($gregorianDate, new \DateTimeZone("UTC"));
        $epoch = $datetime->format("U") * 1000;
        return $epoch;
    }
    
    /**
     * Convert jalali date to gregorian and
     * Convert english digits to persian digits
     * 
     * @uses EZUtil\Date\Jalali To convert jalali date to gregorian
     * 
     * @return string Converted jalai date, like: ۵:۲۲:۴۸ جمعه، ۱۳ مرداد ۱۳۹۱
     */
    public static function epochToJalali($epoch, $format = " l، j F Y", $includeTime = true, $persianNumbers = true)
    {
        $readable_date_obj = new \DateTime("@" . floor($epoch / 1000), new \DateTimeZone("UTC"));
        $jalali = new Jalali();
        if ($includeTime) {
            $res = $readable_date_obj->format('G:i:s');
        } else {
            $res = "";
        }
        $res .= $jalali->setGregorianDate($readable_date_obj)->getJalali()->format($format);
        $en_num = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $fa_num = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        
        if ($persianNumbers) {
            return str_replace($en_num, $fa_num, $res);
        } else {
            return $res;
        }
    }
    
    public static function dateJalaliFloor($epoch, $period)
    {
        $jalaliYear = DateUtilities::epochToJalali($epoch, "Y", false, false);
        $jalaliMonth = (int) DateUtilities::epochToJalali($epoch, "n", false, false);
        $jalaliDay = DateUtilities::epochToJalali($epoch, "j", false, false);
        $jalaliWeekDay = DateUtilities::epochToJalali($epoch, "w", false, false);
        $jalaliObj = new Jalali($jalaliYear, $jalaliMonth, $jalaliDay);
        
        switch ($period) {
            case DateUtilities::$EPOCH_DAY:
                // Do nothing
                break;
            case DateUtilities::$EPOCH_WEEK:
                $jalaliObj->sub("P" . $jalaliWeekDay . "D");
                break;
            case DateUtilities::$EPOCH_MONTH:
                $jalaliObj = new Jalali($jalaliYear, $jalaliMonth, 1);
                break;
            case DateUtilities::$EPOCH_YEAR:
                 $jalaliObj = null;
                $jalaliObj = new Jalali($jalaliYear, 1, 1);
                break;
        }
        return DateUtilities::jalaliToEpoch($jalaliObj->format("Y,n,j"));
    }

    public static function dateJalaliCeiling($epoch, $period)
    {
        $epochFloor = DateUtilities::dateJalaliFloor($epoch, $period);
        
        $jalaliYear = DateUtilities::epochToJalali($epochFloor, "Y", false, false);
        $jalaliMonth = (int) DateUtilities::epochToJalali($epochFloor, "n", false, false);
        $jalaliDay = DateUtilities::epochToJalali($epochFloor, "j", false, false);
        
        $jalaliObj = new Jalali($jalaliYear, $jalaliMonth, $jalaliDay);

        switch ($period) {
            case DateUtilities::$EPOCH_DAY:
                $jalaliObj->add("P1D");
                break;
            case DateUtilities::$EPOCH_WEEK:
                $jalaliObj->add("P8D");
                break;
            case DateUtilities::$EPOCH_MONTH:
                $jalaliObj->add("P1M1D");
                break;
            case DateUtilities::$EPOCH_YEAR:
                $jalaliObj->add("P1Y1D");
                break;
        }
        return DateUtilities::jalaliToEpoch($jalaliObj->format("Y,n,j"));
    }
}
