<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Import Messages</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="index.php" enctype="multipart/form-data" id="importDataForm">
                    <div class="control-group">
                        <label for="file" class="control-label">Select messages file to upload:</label>
                        <div class="custom-upload">
                            <input type="file" name="file" id="file" required="required"/>
                            <div class="fake-file">
                                <input disabled="disabled" >
                            </div>
                        </div>
                    </div>
                    <label for="extractContacts" class="checkbox-inline">
                        <input type="checkbox" name="extractContacts" id="extractContacts" value="1" checked="checked"/>
                        Extract Contacts From Input File
                    </label>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" form="importDataForm">Upload</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->