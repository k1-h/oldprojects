<?php
use SmsAnalysis\Lib\Utility;

if (Utility::getSession('msg') != "") {
    echo '<div id="msg" class="alert alert-' . Utility::getSession('msgType') . '">';
    echo Utility::getSession('msg');
    echo '<a class="close" data-dismiss="alert" href="#">&times;</a>';
    echo '</div>';
    Utility::clearMessage();
    if (Utility::getSession('die')) {
        Utility::setSession("die", "false");
        die();
    }
}
