<div class="modal fade" id="chartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Chart</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="chart.php">
                    <div id="chartLeft">
                        <input type="text" id="contactSearch" class="form-control" autofocus placeholder="Enter Search Query"/>
                        <div id="contactlist">
                            <?php
                                foreach ($param as $number => $name) {
                                    echo '<div class="contact">';
                                        echo '<input type="checkbox" class="contactSelected" name="contacts[]" value="' . $number . '"/> ';
                                        echo '<span>' .  $name . '</span>';
                                    echo '</div>';
                                }
                            ?>
                        </div>
                    </div>
                    <div id="chartRight">
                        <div class="datePicker">
                            <label for="datePickerFrom" class="control-label">From</label>
                            <div>
                                <input type="text" class="form-control" id="datePickerFrom" />
                                <input type="hidden" name="dateFrom" id="hiddenFrom"/>
                            </div>
                        </div>
                        <div class="datePicker">
                            <label for="datePickerTo" class="control-label">To</label>
                            <div>
                                <input type="text" class="form-control" id="datePickerTo" />
                                <input type="hidden" name="dateTo" id="hiddenTo"/>
                            </div>
                        </div>
                        <div id="chartOther">
                            <button type="button" id="checkAll" class="btn btn-info" data-check="false" data-list="contactSelected" data-toggle="button">Select All</button>
                            <div>
                                <label for="msgCount" class="checkbox-inline">
                                  <input type="checkbox" id="msgCount" name="msgCount" value="1"> Count
                                </label>
                                <label for="msgCost" class="checkbox-inline">
                                  <input type="checkbox" id="msgCost" name="msgCost" value="1"> Cost
                                </label>
                            </div>
                            <div id="period">
                                <label for="Type" class="control-label">Period</label>
                                <select id="Type" name="period" class="form-control">
                                    <option value="86400000">Daily</option>
                                    <option value="604800000">Weekly</option>
                                    <option value="2629743000">Monthly</option>
                                    <option value="31556926000">Yearly</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" name="add" value="add" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->