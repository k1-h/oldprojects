<div class="modal fade" id="removeDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Remove All Data</h4>
            </div>
            <div class="modal-body">
                <span>Are you sure to <strong>remove all data</strong> stored in database?</span>
                <form method="post" action="index.php" id="removeDataForm">
                    <input type="hidden" name="removeData" value="1" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No, Cancel!</button>
                <button type="submit" class="btn btn-danger" form="removeDataForm">Yes, Remove All</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->