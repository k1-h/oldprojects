<div class="modal fade" id="removeDatabaseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Remove Database</h4>
            </div>
            <div class="modal-body">
                <span>Are you sure to database?</span>
                <span>This will <strong>remove all data</strong> stored in database</span>
                <form method="post" action="index.php" id="removeDatabaseForm">
                    <input type="hidden" name="removeDB" value="1" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No, Cancel!</button>
                <button type="submit" class="btn btn-danger" form="removeDatabaseForm">Yes, Delete</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->