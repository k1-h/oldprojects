<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Base -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"  media="screen">
<link href="css/font-awesome.min.css"  rel="stylesheet" type="text/css" >

<!-- Date Picker: http://hasheminezhad.com/datepicker -->
<script type="text/javascript" src="js/datepicker/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/datepicker/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="js/datepicker/jquery.ui.datepicker-cc-fa.js"></script>
<script type="text/javascript" src="js/datepicker/calendar.js"></script>
<link href="css/datepicker/jquery-ui-1.8.14.css" rel="stylesheet" type="text/css" />

<!-- Scrollbar -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />

<!-- jqPlot -->
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="js/jqplot/excanvas.js"></script><![endif]-->
<script language="javascript" type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.barRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.json2.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.highlighter.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.cursor.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.pointLabels.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.pieRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.meterGaugeRenderer.min.js"></script>
<link rel="stylesheet" type="text/css" href="js/jqplot/jquery.jqplot.css" />

<!-- Custom -->
<script src="js/jqplot.js"></script>
<script src="js/script.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" >

<title><?php echo $param['title']; ?></title>
