<div class="modal fade" id="installModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Install Database</h4>
            </div>
            <div class="modal-body">
                <span>Enter Database Filename to Import(Default filename is "sql.sql"):</span>
                <form method="post" action="index.php" id="installDatabaseForm">
                    <input type="text" name="filename" required="required" autofocus value="Lib/sql.sql"/><br />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" form="installDatabaseForm">Install</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->