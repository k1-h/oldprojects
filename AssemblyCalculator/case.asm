.model small
.stack 64

.data
	coname db "Test Text", '$'
	
.code
main proc far
	mov ax,@data
	mov ds,ax
	mov es,ax
	lea bx,coname
	mov cx,09
comp:
	mov ah,[bx]
	cmp ah,41h
	jb next
	cmp ah,5ah
	jb tolower
	cmp ah,7ah
	ja next
	cmp ah,60h
	ja toupper
next:
	inc bx
	loop comp
	jmp print
tolower:
	xor ah,00100000b
	mov [bx],ah
	jmp next
toupper:
	and ah,11011111b
	mov [bx],ah
	jmp next
print:
	mov ah,09h
	lea dx,coname
	int 21h
	mov ax,4c00h
	int 21h
main endp
end main