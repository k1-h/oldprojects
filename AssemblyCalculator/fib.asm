.model small
.stack 32
.data
	f1 db 1
	f2 db 1
	f3 db 0,'$'
	
.code
main proc far
	mov ax,@data
	mov ds,ax
	mov cx,10

fib:
	mov al,f1
	add al,f2
	mov f3,al
	
	mov al,f2
	mov f1,al
	
	mov al,f3
	mov f2,al
	
	loop fib
	
	mov ah,09h
	lea dx,f3
	int 21h
	mov ax, 4c00h
	int 21h
main endp
end main