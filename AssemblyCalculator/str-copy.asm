.model small
.stack 64

.data
	heading1 db "abcdefg", '$'
	heading2 db "1234567", '$'
	temp db 20 dup('$')
	abov db 'First string is grater than second.','$'
	belo db 'First string is smaller than second.','$'
	euqe db 'First string is equal with second.','$'
	d1 db 0ah, '$'

.code
main	proc far
		mov ax,@data
		mov ds,ax
		mov es,ax
		
		mov bl,07 ; heading1 length
		mov bh,07 ; heading2 length
		cmp bl,bh
		je equal
		ja above
		jb below
	equal:
		mov ah,09h
		lea dx,euqe
		int 21h
		mov ah,09h
		lea dx,d1
		int 21h
		jmp replace
	above:
		mov ah,09h
		lea dx,abov
		int 21h
		mov ah,09h
		lea dx,d1
		int 21h
		jmp replace
	below:
		mov ah,09h
		lea dx,belo
		int 21h
		mov ah,09h
		lea dx,d1
		int 21h
		jmp replace
	replace:
		lea si,heading1
		lea di,temp
		mov cl,bh
		call copy
		lea si,heading2
		lea di,heading1
		mov cl,bl
		call copy
		lea si,temp
		lea di,heading2
		mov cl,bh
		call copy
	print:
		mov ah,09h
		lea dx,heading1
		int 21h
		mov ah,09h
		lea dx,d1
		int 21h
		mov ah,09h
		lea dx,heading2
		int 21h
		mov ax,4c00h
		int 21h
main		endp
copy proc near
	start:
		mov al,[si]
		mov [di],al
		inc si
		inc di
		dec cl
		cmp cl,0
		jnz start
	ret
copy endp
end main	