.model small
.stack 64
.data
	byte_tbl db 20,20,20,0,10
	byte_total db 0,'$'
	print db '  ','$'
	
.code
main proc far
	mov ax,@data
	mov ds,ax
	mov cx,5
	mov bx,0
	
ad:
	mov al,byte_tbl+bx
	add byte_total,al
	inc bx
	loop ad
	
	mov ah,09h
	lea dx,byte_total
	int 21h
	mov ax, 4c00h
	int 21h
main endp
end main