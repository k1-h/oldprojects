.model small
.386
.stack 64
.data
	strNum1 db 8 dup(0),'$'
	strNum2 db 8 dup(0),'$'
	strResInt db 8 dup(0),'$'
	strResFloat db 8 dup(0),'$'
	strNull db 8 dup(0),'$'
	colors db 7 ,9 ,10 ,11, 15, 26, 27,30, 91, 95, 121, 128, 159, 176, 185, 206, 207, 208, 240, 0
	colorPTR dw 0
	aborted db 1
	zeroDivide db 0
	negative db 0
	overflow db 0
	op db 0
	num1 dd 0,'$'
	num2 dd 0,'$'
	resInt dd 0,'$'
	resFloat dd 0,'$'
	txtNum1 db 'Enter First number: $'
	txtNum2 db 'Enter Second number: $'
	txtOperator db 'Enter Operator: $'
	txtPrompt db 'Agian?, Clear screen?, Change color?, Help?, Quit? [acohq]: $'
	txtHelp db  'Assembly Calculator',0dh,0ah,'+: Sum',0dh,0ah,'-: Subtract',0dh,0ah,'*: Multiply',0dh,0ah,'/: Divide',0dh,0ah,'\: Integer Divide',0dh,0ah,'%: Reminder',0dh,0ah,'Press q any time to Quit',0dh,0ah,'$'
	txtRes db 'Result: $'
	txtOverflow db 'Overflow!, Incorrect result.',0dh,0ah,'$'
	txtAborted db 0ah,'Aborted!$'
	txtZeroDiv db 'Can not divide to Zero!',0dh,0ah,'$'
	newline db ,0dh,0ah, '$'

.code
main proc far
	mov ax,@data
	mov ds,ax
	mov es,ax
	
	lea si,colors
	mov colorPTR,si
	call cls
	mov ah,09h
	lea dx,txtHelp
	int 21h
main_start:
	call clearData
	call getInput
	call calculate
	cmp zeroDivide,1
	je main_prompt
	call printRes
main_prompt:
	mov ah,09h
	lea dx,txtPrompt
	int 21h
main_get:
	mov ah,10h
	int 16h
	cmp al,97
	je main_jump
	cmp al,65
	je main_jump
	cmp al,99
	je main_cls
	cmp al,67
	je main_cls
	cmp al,111
	je main_chg_color
	cmp al,79
	je main_chg_color
	cmp al,104
	je main_printHelp
	cmp al,72
	je main_printHelp
	cmp al,113
	je main_done
	cmp al,81
	je main_done
	jmp main_get 
main_cls:
	mov ah,02h
	mov dl,al
	int 21h
	call cls
	jmp main_prompt
main_printHelp:
	mov ah,02h
	mov dl,al
	int 21h
	mov ah,09h
	lea dx,newline
	int 21h
	mov ah,09h
	lea dx,txtHelp
	int 21h
	jmp main_prompt
main_chg_color:
	mov ah,02h
	mov dl,al
	int 21h
	call chg_color
	jmp main_prompt
main_jump:
	mov ah,02h
	mov dl,al
	int 21h
	mov ah,09h
	lea dx,newline
	int 21h
	jmp main_start
	
main_done:
	mov ah,02h
	mov dl,al
	int 21h
	mov aborted,0
	call exit
main endp
;******************************************************clearData
clearData proc near
	mov ecx,8
clearData_start:
	mov al,strNull+ecx
	mov strNum1+ecx,al
	mov al,strNull+ecx
	mov strNum2+ecx,al
	mov al,strNull+ecx
 	mov strResInt+ecx,al
	mov al,strNull+ecx
	mov strResFloat+ecx,al
	loop clearData_start
	mov op,0
	mov num1,0
	mov num2,0
	mov resInt,0
	mov resFloat,0
	mov zeroDivide,0
	mov negative,0
	mov overflow,0
	
	ret
clearData endp
;******************************************************getInput
getInput proc near
	call getNum1
	call getOperator
	call getNum2
	
	ret
getInput endp
;*******************************************************getNum1
getNum1 proc near
getNum1_Prepare:
	mov ah,09h
	lea dx,txtNum1
	int 21h
	mov negative,0
	mov bx,0
	mov ah,10h
	int 16h
	cmp al,45
	je getNum1_Neg
	jmp getNum1_Check
getNum1_Get:
	mov ah,10h
	int 16h
getNum1_Check:
	cmp al,113
	je getNum1_exit
	cmp al,81
	je getNum1_exit
	cmp al,0dh
	je getNum1_Continue
	cmp al,48
	jb getNum1_Get
	clc
	cmp al,57
	ja getNum1_Get
	mov ah,02h
	mov dl,al
	int 21h
	inc bx
	mov strNum1[bx],al
	jmp getNum1_Get
getNum1_Continue:
	call convNum1
	mov ah,09h
	lea dx,newline
	int 21h
	jmp getNum1_Done
getNum1_Neg:
	mov negative,1
	mov ah,02h
	mov dl,al
	int 21h
	jmp getNum1_Get
getNum1_Exit:
	call exit
getNum1_Done:
	ret
getNum1 endp
;*******************************************************convNum1
convNum1 proc near
	mov num1,0
	;dec bx
	mov ecx,1
convNum1_Next:
	mov al,strNum1+bx
	sub al,48
	movsx edx,al
	imul edx,ecx
	add num1,edx
	imul ecx,10
	dec bx
	cmp bx,0
	jne convNum1_Next
	cmp negative,1
	je convNum1_Neg
	jmp convNum1_Done
convNum1_Neg:
	neg num1
convNum1_Done:
	ret
convNum1 endp
;*******************************************************getOperator
getOperator proc near
getOperator_PrepareOp:
	mov ah,09h
	lea dx,txtOperator
	int 21h
getOperator_GetOp:
	mov ah,10h
	int 16h
getOperator_CheckOp:
	cmp al,43
	je getOperator_OpOK
	cmp al,45
	je getOperator_OpOK
	cmp al,42
	je getOperator_OpOK
	cmp al,47
	je getOperator_OpOK
	cmp al,92
	je getOperator_OpOK
	cmp al,37
	je getOperator_OpOK
	cmp al,113
	je getOperator_exit
	cmp al,81
	je getOperator_exit
	jmp getOperator_GetOp
getOperator_OpOK:
	mov ah,02h
	mov dl,al
	int 21h
	mov op,al
getOperator_WaitForEnter:
	mov ah,10h
	int 16h
	cmp al,0dh
	jne getOperator_WaitForEnter
	mov ah,09h
	lea dx,newline
	int 21h
	jmp getOperator_Done
getOperator_exit:
	call exit
	
getOperator_Done:
	ret
getOperator endp
;*******************************************************getNum2
getNum2 proc near
getNum2_Prepare:
	mov ah,09h
	lea dx,txtNum2
	int 21h
	mov negative,0
	mov bx,0
	mov ah,10h
	int 16h
	cmp al,45
	je getNum2_Neg
	jmp getNum2_Check
getNum2_Get:
	mov ah,10h
	int 16h
getNum2_Check:
	cmp al,113
	je getNum2_exit
	cmp al,81
	je getNum2_exit
	cmp al,0dh
	je getNum2_Continue
	cmp al,48
	jb getNum2_Get
	clc
	cmp al,57
	ja getNum2_Get
	mov ah,02h
	mov dl,al
	int 21h
	inc bx
	mov strNum2[bx],al
	jmp getNum2_Get
getNum2_Continue:
	call convNum2
	mov ah,09h
	lea dx,newline
	int 21h
	jmp getNum2_Done
getNum2_Neg:
	mov negative,1
	mov ah,02h
	mov dl,al
	int 21h
	jmp getNum2_Get
getNum2_exit:
	call exit
getNum2_Done:
	ret
getNum2 endp
;*******************************************************convNum2
convNum2 proc near
	mov num2,0
	;dec bx
	mov ecx,1
convNum2_Next:
	mov al,strNum2+bx
	sub al,48
	movsx edx,al
	imul edx,ecx
	add num2,edx
	imul cx,10
	dec bx
	cmp bx,0
	jne convNum2_Next
	cmp negative,1
	je convNum2_Neg
	jmp convNum2_Done
convNum2_Neg:
	neg num2
	
convNum2_Done:
	ret
convNum2 endp
;*******************************************************calculate
calculate proc near
cal_start:
	mov resInt,0
	mov al,op
	cmp al,43
	je sum
	cmp al,45
	je subt
	cmp al,42
	je mult
	cmp al,47
	je divi
	cmp al,92
	je diviInt
	cmp al,37
	je reminder
	
sum:
	mov ecx,num1
	mov resInt,ecx
	mov ecx,num2
	add resInt,ecx
	jo overflowed
	jmp done
subt:
	mov ecx,num1
	mov resInt,ecx
	mov ecx,num2
	sub resInt,ecx
	jo overflowed
	jmp done
mult:
	mov ecx,num1
	mov edx,num2
	imul edx,ecx
	mov resInt,edx
	jo overflowed
	jmp done
divi:
	cmp num2,0
	je zeroDiv
	mov edx,0
	mov eax,num1
	;mov resInt,eax
	mov ebx,num2
	idiv ebx
	mov resInt,eax
	jo overflowed
	cmp edx,0
	je done
	imul edx,100
	mov eax,edx
	mov edx,0
	idiv ebx
	mov resFloat,eax
	jmp done
diviInt:
	cmp num2,0
	je zeroDiv
	mov edx,0
	mov eax,num1
	;mov resInt,eax
	mov ebx,num2
	idiv ebx
	mov resInt,eax
	jo overflowed
	jmp done
reminder:
	cmp num2,0
	je zeroDiv
	mov edx,0
	mov eax,num1
	;mov resInt,eax
	mov ebx,num2
	idiv ebx
	mov resInt,edx
	jo overflowed
	jmp done
zeroDiv:
	mov ah,09h
	lea dx,txtZeroDiv
	int 21h
	mov zeroDivide,1
	jmp done
overflowed:
	mov overflow,1
	
done:
	ret
calculate endp
;*******************************************************binToAsc
binToAsc proc near
    mov ecx, 10         ; divisor
    xor bx, bx          ; count digits
	cmp eax,7fffffffh
	ja binToAsc_Neg
	jmp binToAsc_Div
binToAsc_Neg:
	mov negative,1
	neg eax
	jmp binToAsc_Div

binToAsc_Div:
    xor edx, edx        ; high part = 0
    div ecx             ; eax = edx:eax/ecx, edx = remainder
    push dx             ; DL is a digit in range [0..9]
    inc bx              ; count digits
    cmp eax, 0       ; EAX is 0?
    jnz binToAsc_Div          ; no, continue

    ; POP digits from stack in reverse order
    mov cx, bx          ; number of digits
binToAsc_NextDig:
    pop ax
    add al, '0'         ; convert to ASCII
    mov [si], al        ; write it to the buffer
    inc si
    loop binToAsc_NextDig
	
	ret
binToAsc endp
;*******************************************************printRes
printRes proc near
	mov eax,resInt  ; number to be converted
	lea si, strResInt   ; DS:SI points to string buffer
	mov negative,0
	call binToAsc
	mov eax,resFloat  ; number to be converted
	lea si, strResFloat   ; DS:SI points to string buffer
	call binToAsc
	cmp overflow,1
	je printRes_overflow
printRes_print:
	mov ah,09h
	lea dx,txtRes
	int 21h
	call printInt
	call printFloat
	mov ah,09h
	lea dx,newline
	int 21h
	jmp printRes_End
printRes_overflow:
	mov ah,09h
	lea dx,txtOverflow
	int 21h
	jmp printRes_print
	
printRes_End:
	ret
printRes endp
;*******************************************************printInt
printInt proc near
	mov ecx,0
	cmp negative,1
	je printInt_Neg
	jmp printInt_Next
printInt_Neg:
	mov ah,02h
	mov dl,'-'
	int 21h
printInt_Next:
	cmp strResInt+ecx,0
	je printInt_Loop
	cmp strResInt+ecx,'$'
	je printInt_Done
	mov ah,02h
	mov dl,strResInt+ecx
	int 21h
printInt_Loop:
	inc cx
	jmp printInt_Next
	
printInt_Done:
	ret
printInt endp
;*******************************************************printFloat
printFloat proc near
	mov ecx,0
	cmp strResFloat,'0'
	je printFloat_Done
	mov ah,02h
	mov dl,'.'
	int 21h
printFloat_Next:
	cmp strResFloat+ecx,0
	je printFloat_Loop
	cmp strResFloat+ecx,'$'
	je printFloat_Done
	mov ah,02h
	mov dl,strResFloat+ecx
	int 21h
printFloat_Loop:
	inc cx
	jmp printFloat_Next
	
printFloat_Done:
	ret
printFloat endp
;*******************************************************cls
cls proc near
	mov si,colorPTR
	mov ax,0600h
	mov bh,[si]
	mov cx,0000h
	mov dx,184fh
	int 10h
	mov ah,02h
	mov bh,00
	mov dx,0000h
	int 10h
	
	ret
cls endp
;*******************************************************chg_color
chg_color proc near
	inc colorPTR
	mov si,colorPTR
	mov ah,[si]
	cmp ah,0
	je chg_color_zero
	jmp chg_color_start
chg_color_zero:
	lea si,colors
	mov colorPTR,si
chg_color_start:
	mov ah,03h
	mov bh,0
	int 10h
	push dx
	mov ch,dh
	inc ch
	mov cl,0
	mov edx,[si]
	mov ax,0600h
	mov bh,[si]
	mov dx,184fh
	int 10h
	pop dx
	mov ah,02h
	mov bh,00
	mov dh,ch
	;inc dh
	mov dl,0
	;mov dx,0000h
	int 10h
	
	ret
chg_color endp
;*******************************************************exit
exit proc near
	cmp aborted,0
	je exit_Done
	mov ah,09h
	lea dx,txtAborted
	int 21h
exit_Done:
	mov ax,4c00h
	int 21h
exit endp
;*******************************************************END
end main