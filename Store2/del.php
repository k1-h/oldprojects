<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/simple-store
 */
if (!empty($_POST['submit'])) {
    $data = unserialize(file_get_contents('store'));
    unset($data[$_POST['code']]);
    file_put_contents('store', serialize($data));
}
?>
<html>
<head>
<style>
</style>
</head>
<body>
    <h1>Enter Item Code</h1>

    <form method="post" action="del.php">
        Code: <input name="code" type="text" /><br />
        <input type="submit" name="submit"/>
    </form>
    <a href="index.php">Return to Home</a>
</body>
</html>
