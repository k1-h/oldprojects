<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/simple-store
 */
if (!empty($_POST['submit'])) {
    $data = unserialize(file_get_contents('store'));
    $newdata=array();
    $newdata[]=$_POST['name'];
    $newdata[]=$_POST['type'];
    $newdata[]=$_POST['price'];
    $newdata[]=$_POST['serial'];
    $newdata[]=$_POST['date'];
    $data[$_POST['name']]=$newdata;
    file_put_contents('store', serialize($data));
}
?>
<html>
<head>
<style>
</style>
</head>
<body>
    <h1>Enter Data</h1>
    <a href="index.php">Return to Home</a>
    <pre>
        <form method="post" action="add.php">
Name:	<input name="name" type="text" /><br />
Type:	<input name="type" type="text" /><br />
Price:	<input name="price" type="text" /><br />
Serial:	<input name="serial" type="text" /><br />
Date:	<input name="date" type="date" /><br />
<input type="submit" name="submit"/>
        </form>
    </pre>
</body>
</html>
