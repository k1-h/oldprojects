<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/simple-store
 */
function sortBy($by)
{
    return function ($a, $b) use ($by) {
        return strnatcmp($a[$by], $b[$by]);
    };
}

$data = unserialize(file_get_contents('store'));
$data = array_values($data);
file_put_contents('store', serialize($data));

if (!empty($_GET['sort'])) {
    usort($data, sortBy($_GET['sort']-1));
}
?>
<html>
<head>
<style>
    table tr:first-child {
        font-weight:bold;
    }
</style>
</head>
<body>
    <table border="1" cellspacing="0" cellpadding="5">
        <tr>
            <td>Code</td>
            <td>Name</td>
            <td>Type</td>
            <td>Price</td>
            <td>Serial</td>
            <td>Date</td>
        </tr>
        <?php
            foreach ($data as $key=>$datum) {
                echo '<tr>';
                echo '<td>'.$key.'</td>';
                echo '<td>'.$datum[0].'</td>';
                echo '<td>'.$datum[1].'</td>';
                echo '<td>'.$datum[2].'</td>';
                echo '<td>'.$datum[3].'</td>';
                echo '<td>'.$datum[4].'</td>';
                echo '</tr>';
            }
        ?>
    </table>
    <a href="add.php">Add Item</a>
    <a href="del.php">Delete Item</a>
    <a href="search.php">Seach</a><br />
    <a href="index.php?sort=1">Sort By Name</a>
    <a href="index.php?sort=2">Sort By Type</a>
    <a href="index.php?sort=3">Sort By Price</a>

</body>
</html>
