<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/dictionary
 * 
 */
//ob_start();
require_once 'inc.php';
setSessionV('conn',connect(1));

if(!empty($_COOKIE['user']))
    authCookie($_COOKIE['user'],$_COOKIE['pass']);
if (!empty($_POST['login'])) {
    if(strpos($_POST['lgnUsername'],'@')>1)
        $isEmail=1;
    else
        $isEmail=0;
    authPost($_POST['lgnUsername'],$_POST['lgnPassword'],isset($_POST['lgnRemember'])?1:0,$isEmail);
} else if(!empty($_POST['register']))
    register($_POST['regUsername'],$_POST['regEmail'],$_POST['regPassword'],0,0,0);
else if(isset($_GET['logout']) && isset($_COOKIE['user']) && isset($_COOKIE['pass']))
    logout($_COOKIE['user'],$_COOKIE['pass']);

if($_SESSION['newLoc']!="" && $_SESSION['newLoc']!="index.php")
    changeLoc($_SESSION['newLoc']);
?>
<!DOCTYPE html>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <style>
        .tab-content, .nav {
            margin-left:0;
        }
        form {
            width:auto;
        }
    </style>

    <title>Login or Register</title>
</head>
<body>
    <script src="js/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    <?php if($_SESSION['msg']!=""): ?>
        <div id="msg" class="alert alert-<?php echo $_SESSION['type']; ?>">
        <?php echo $_SESSION['msg']; ?>
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>
    <?php setSessionR(null,null,null); endif; ?>
    <div class="row-fluid span4 ">
        <div id="main" class="span4 offset4 row-fluid">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#login" data-toggle="tab"><i class="icon-signin"></i> Login</a></li>
                <li><a href="#register" data-toggle="tab"><i class="icon-plus-sign"></i> Register</a></li>
            </ul>
            <div class="tab-content">
            <div id="login" class="active tab-pane">
                <form method="post" name="loginForm" action="index.php" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label" for="Login-Username">Username or Email: </label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-user"></i></span>
                                <input type="text" name="lgnUsername" id="Login-Username" required="required" autofocus placeholder="Enter Username or Email" title="Enter Username or Email" pattern="^[a-zA-Z][a-zA-Z0-9-_\.@]{1,20}$"/><br />
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="Login-Password" class="control-label">Password: </label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-key"></i></span>
                                <input type="password" name="lgnPassword" id="Login-Password" required="required" placeholder="Enter Password" title="Enter Password" pattern="\w{6,}"/><br />
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="remember" class="control-label">Remember Me</label>
                        <div class="controls">
                            <input type="checkbox" name="lgnRemember" id="remember" value="1" /><br />
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" name="login" value="Login" class="btn btn-primary" ><i class="icon-signin"></i> Login</button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="register" class="tab-pane">
                <form method="post" name="registerFrom" action="index.php" class="form-horizontal checkPass">
                    <div class="control-group">
                        <label for="Registeration-Username" class="control-label">Username: </label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-user"></i></span>
                                <input type="text" name="regUsername" id="Registeration-Username" required="required" autofocus placeholder="Enter Username with 2-20 chars" title="Enter Username with 2-20 chars" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$"/><br />
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="Email" class="control-label">Email: </label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-envelope"></i></span>
                                <input type="text" name="regEmail" id="Email" required="required" placeholder="Enter a Valid Email Address" title="Enter a Valid Email Address" pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$"/><br />
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="Registeration-Password" class="control-label">Password: </label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-key"></i></span>
                                <input type="password" name="regPassword" id="Registeration-Password" required="required" placeholder="Enter Password with min 6 Chars" title="Enter Password with min 6 Chars"  pattern="\w{6,}"/><br />
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="Registerationg-Password-Confirm" class="control-label">Confirm Password: </label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-key"></i></span>
                                <input type="password" name="confRegPassword" id="Registerationg-Password-Confirm" required="required" placeholder="Confirm your Password" title="Confirm your Password and should have min 6 Chars" pattern="\w{6,}"/><br />
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" name="register" value="Register" class="btn btn-primary"/><i class="icon-signin"></i> Register</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</body>
</html>
