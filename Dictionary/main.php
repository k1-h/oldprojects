﻿<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/dictionary
 * 
 */
ob_start();
require_once 'inc.php';
setSessionV('conn',connect(1));

if(!empty($_COOKIE['user']))
    authCookie($_COOKIE['user'],$_COOKIE['pass']);
if(!empty($_POST['add']))
    addWord($_POST['addEnglish'],$_POST['addPersian']);
if (!empty($_POST['remove'])) {
    if(empty($_POST['englishSelected']) && empty($_POST['persianSelected']))
        setSessionR("First Select Word(s).","error","main.php");
    else if(empty($_POST['englishSelected']))
        removeWords(null,$_POST['persianSelected']);
    else if(empty($_POST['persianSelected']))
        removeWords($_POST['englishSelected'],null);
    else
        removeWords($_POST['englishSelected'],$_POST['persianSelected']);
}
if(!empty($_POST['delUser']))
    deleteUser($_POST['user']);
if (!empty($_POST['editUser'])) {
    $user=userInfo($_POST['oldUser'],0);
    $addWordsPer=empty($_POST['addWordsPer'])?0:1;
    $removeWordsPer=empty($_POST['removeWordsPer'])?0:1;
    $editUsersPer=empty($_POST['editUsersPer'])?0:1;
    editUsers($_POST['user'],$_POST['email'],$_POST['password'],$addWordsPer,$removeWordsPer,$editUsersPer,$_POST['oldUser']);
}
if (!empty($_POST['editInfo'])) {
    $user=userInfo($_POST['oldUser'],0);
    editUsers($_POST['user'],$_POST['email'],$_POST['password'],$user["addWords"],$user["removeWords"],$user["editUsers"],$_POST['oldUser']);
}
if(!empty($_POST['removeDB']))
    removeDB();

if($_SESSION['newLoc']!="" && $_SESSION['newLoc']!="main.php")
    changeLoc($_SESSION['newLoc']);

?>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<head>
    <title>Dictionary</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <script src="js/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>

    <?php if($_SESSION['msg']!=""): ?>
        <div id="msg" class="alert alert-<?php echo $_SESSION['type'];?>">
        <?php echo $_SESSION['msg']; ?>
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>
    <?php setSessionR(null,null,null); endif; ?>
    <div class="row-fluid tabbable tabs-left">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#searchTab" data-toggle="tab"><i class="icon-search"></i> Search</a></li>
            <?php
                if ($_SESSION['addWords'])
                    echo '<li><a href="#addTab" data-toggle="tab"><i class="icon-plus"></i> Add</a></li>';
                if ($_SESSION['removeWords'])
                    echo '<li><a href="#removeTab" data-toggle="tab"><i class="icon-minus"></i> Remove</a></li>';
                if ($_SESSION['editUsers'])
                    echo '<li><a href="#users" data-toggle="tab"><i class="icon-user"></i> Users</a></li>';
             ?>
            <li><a href="#infoTab" data-toggle="tab"><i class="icon-info-sign"></i> Info</a></li>
            <li><a href="index.php?logout"><i class="icon-signout"></i> Logout</a></li>
        </ul>
        <div class="tab-content">
            <div id="searchTab" class="tab-pane active fade in">
                <div class="row-fluid ">
                    <form method="post" action="main.php" class="form-search span3" onsubmit='$("#submit").click(); return false;'>
                        <div class="input-append">
                            <input type="text" size="10" name="search" id="translateSearch" class="span9 search-query" autofocus required="required" placeholder="Enter Search Query"/>
                            <button type="button" id="submit" class="btn btn-info"><i class="icon-search"></i> Search</button>
                        </div><br />

                    </form>
                </div>
                <div class="row-fluid">
                    <div id="result" class="alert hide">
                        <div class="hide">Not found, closest word:</div>
                    </div>
                </div>
                <div id="englishSearchList" class="row-fluid wordlist searchList">
                    <?php
                        foreach(getEnglishWords() as $word)
                            echo "<div>$word</div>";
                    ?>
                </div>
                <div id="persianSearchList" class="row-fluid wordlist searchList">
                    <?php
                        foreach(getPersianWords() as $word)
                            echo "<div>$word</div>";
                    ?>
                </div>
            </div>
            <?php if ($_SESSION['addWords']): ?>
                <div id="addTab" class="tab-pane fade">
                    <form method="post" name="addForm" action="main.php" class="form-horizontal">
                        <div class="control-group">
                            <input type="text" name="addEnglish" id="addEnglish" required="required" placeholder="Enter English Word"/><br />
                        </div>
                        <div class="control-group">
                            <input type="text" name="addPersian" id="addPersian" required="required" placeholder="Enter Persian Equal"/><br />
                        </div>
                        <div class="control-group">
                                <button type="submit" name="add" value="add" class="btn btn-primary"><i class="icon-plus"></i> Add</button>
                        </div>
                    </form>
                </div>
            <?php endif; ?>
            <?php if ($_SESSION['removeWords']): ?>
                <div id="removeTab" class="tab-pane fade">
                    <form method="post" name="removeForm" action="main.php" class="form-horizontal" id="removeForm">
                        <div class="control-group">
                            <input type="text" name="search" id="removeSearch" class="search-query" autofocus placeholder="Enter Search Query"/><br />
                        </div>
                        <div id="englishRemoveList" class="wordlist removeList">
                            <?php
                                foreach (getEnglishWords() as $word) {
                                    echo '<div class="englishRemoveWord">';
                                        echo '<input type="checkbox" class="englishSelected" name="englishSelected[]" value="'.$word.'"/> ';
                                        echo '<span>'. $word.'</span>';
                                    echo '</div>';
                                }
                            ?>
                        </div>
                        <div id="persianRemoveList" class="wordlist removeList">
                            <?php
                                foreach (getPersianWords() as $word) {
                                    echo '<div class="persianRemoveWord">';
                                        echo '<input type="checkbox" class="persianSelected" name="persianSelected[]" value="'.$word.'"/> ';
                                        echo '<span>'. $word.'</span>';
                                    echo '</div>';
                                }
                            ?>
                        </div>
                        <div class="control-group">
                            <div class="">
                                <button type="button" id="checkAllEnglish" class="btn btn-info checkAll" data-check="false" data-list="englishSelected" data-toggle="button">Select All</button>
                                <button type="button" id="checkAllPersian" class="btn btn-info checkAll pull-right" data-check="false" data-list="persianSelected" data-toggle="button">Select All</button>
                            </div><br/>
                            <button type="submit" name="remove" value="Remove Words" class="btn btn-primary "><i class="icon-minus"></i> Remove Selected Words</button><br/><br/>
                            <?php if ($_SESSION['user']=='admin'): ?>
                                <button type="submit" name="removeDB" value="Remove Database" class="btn btn-inverse "><i class="icon-remove"></i> Remove Database</button>
                            <?php endif; ?>
                        </div>
                    </form>
                </div>
            <?php endif; ?>
            <?php if ($_SESSION['editUsers']): ?>
                <div id="users" class="tab-pane fade">
                    <table border="1" cellspacing="0" cellpadding="5" class="table-striped table-hover">
                        <thead>
                            <tr>
                                <td>User</td>
                                <td>Email</td>
                                <td>Add</td>
                                <td>Remove</td>
                                <td>Users</td>
                                <td>Edit</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach (getUsersInfo() as $user) {
                                    echo '<tr>';
                                        echo '<td>'.$user["username"].'</td>';
                                        echo '<td>'.$user["email"].'</td>';
                                        echo '<td>';
                                            echo $user["addWords"]=="1"?"Yes":"No";
                                        echo '</td>';
                                        echo '<td>';
                                            echo $user["removeWords"]=="1"?"Yes":"No";
                                        echo '</td>';
                                        echo '<td>';
                                            echo $user["editUsers"]=="1"?"Yes":"No";
                                        echo '</td>';
                                        echo '<td>';
                                            echo '<button id="showUserEdit" data-divid="editUser" class="btn btn-primary showUserEdit"><i class="icon-edit"></i> Edit</button>';
                                        echo '</td>';
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                    <div id="editUser" class="hide">
                        <form method="post" name="edit" class="checkPass" action="main.php">
                            <label for="editUserUser">Username: </label>
                            <input type="text" name="user" id="editUserUser" required="required" placeholder="Enter Username with 2-20 chars" title="Enter Username with 2-20 chars" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$"/>
                            <label for="editUserEmail" >Email: </label>
                            <input type="text" name="email" id="editUserEmail" required="required" placeholder="Enter a Valid Email Address" title="Enter a Valid Email Address" pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$"/><br />
                            <label for="editUserPassword" >New Password: </label>
                            <input type="password" name="password" id="editUserPassword" placeholder="Enter Password with min 6 Chars" title="Enter Password with min 6 Chars" pattern="\w{6,}"/><br />
                            <label for="editUserConfPassword" >Confirm Password: </label>
                            <input type="password" name="confPassword" id="editUserConfPassword" placeholder="Enter Password with min 6 Chars" title="Enter Password with min 6 Chars" pattern="\w{6,}"/><br />
                            <input type="checkbox" name="addWordsPer" id="editUserAddWordsPer" />
                            <label for="editUserAddWordsPer" >Add </label>
                            <input type="checkbox" name="removeWordsPer" id="editUserRemoveWordsPer" />
                            <label for="editUserRemoveWordsPer" >Remove </label>
                            <input type="checkbox" name="editUsersPer" id="editUserEditUsersPer" />
                            <label for="editUserEditUsersPer" >Users </label><br /><br />
                            <input type="hidden" id="editUserOldUser" name="oldUser"/>
                            <div class="controls">
                                <button name="editUser" type="submit" value="editUser" class="btn btn-primary span5"><i class="icon-save"></i> Save</button>
                            </div>
                            <div class="controls">
                                <button name="delUser" type="submit" value="delUser" class="btn btn-danger span7"><i class="icon-trash"></i> Delete User</button>
                            </div>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
            <div id="infoTab" class="tab-pane fade">
                <table border="1" cellspacing="0" cellpadding="5" class="table-striped table-hover">
                    <tr><td>Username:</td> <td><?php echo $_SESSION['user']; ?></td></tr>
                    <tr><td>Email:</td> <td><?php echo $_SESSION['email']; ?></td></tr>
                    <tr><td>Can Add Words:</td> <td><?php echo ($_SESSION['addWords']=="1")?"Yes":"No"; ?></td></tr>
                    <tr><td>Can Remove Words:</td> <td><?php echo ($_SESSION['removeWords']=="1")?"Yes":"No"; ?></td></tr>
                    <tr><td>Can Edit Users:</td> <td><?php echo ($_SESSION['editUsers']=="1")?"Yes":"No"; ?></td></tr>
                </table><br />
                <button class="btn btn-primary edit" id="showUserInfo" data-divid="editInfo"><i class="icon-edit"></i> Edit</button>
                <div id="editInfo" class="hide">
                    <form method="post" name="edit" class="checkPass" action="main.php">
                        <label for="editInfoUser">Username: </label>
                        <input type="text" name="user" id="editInfoUser" required="required" placeholder="Enter Username with 2-20 chars" title="Enter Username with 2-20 chars" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$"/>
                        <label for="editInfoEmail" >Email: </label>
                        <input type="text" name="email" id="editInfoEmail" required="required" placeholder="Enter a Valid Email Address" title="Enter a Valid Email Address" pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$"/><br />
                        <label for="editInfoPassword" >New Password: </label>
                        <input type="password" name="password" id="editInfoPassword" placeholder="Enter Password with min 6 Chars" title="Enter Password with min 6 Chars" pattern="\w{6,}"/><br />
                        <label for="editInfoConfPassword" >Confirm Password: </label>
                        <input type="password" name="confPassword" id="editInfoConfPassword" placeholder="Enter Password with min 6 Chars" title="Enter Password with min 6 Chars" pattern="\w{6,}"/><br />
                        <input type="hidden" id="editInfoOldUser" name="oldUser"/>
                        <button id="editUserInfo" name="editInfo" type="submit" value="editInfo" class="btn btn-primary span5"><i class="icon-save"></i> Save</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</body>
</html>
