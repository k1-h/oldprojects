$(document).ready(function(){
	/************ Translate Words When Clicking on Them ************/
	$(".searchList > div").click(function(){
		$("#translateSearch").val($(this).text());
		$("#submit").click();
	});
	
	/************* Select Words When Clicking on Them *************/
	$(".removeList > div").click(function(){
		if(event.target.nodeName!='INPUT') {
			toggle=!$(this).children("input").attr('checked');
			$(this).children("input").attr('checked',toggle);
		}
	});

	/*************** Filter Lists With Written Keys ***************/
	$('.search-query').keyup(function(){
		var wordlist=$(this).closest('.tab-pane').find(".wordlist").children();
		wordlist.hide().each(function() {
			var word=$(this).closest('.tab-pane').find('.search-query').val().toLowerCase();
			if($(this).text().toLowerCase().indexOf(word)!=-1)
				$(this).show();
		});
	});
	 
	/*********** Selecet and Deselect All in Remove Tab ***********/
	$('.checkAll').click(function() {
		var checked=!$(this).data('check');
		var list=$(this).closest('form').find("."+$(this).data('list'));
		
		buttonText=(checked)?"Deselect All":"Select All";
		list.each(function(){
			$(this).attr('checked',checked);
		});
		$(this).data('check',checked);
		$(this).text(buttonText);
	});

	/******************** Main Search Function ********************/
	$("#submit").click(function(){
		var data = { "search": $("#translateSearch").val()};
		$.post("translate.php",data,
		   function(data) {
				printRes(data);
		});
		$("#translateSearch").val('');
	});

	/******************* Handle JSON Response *******************/
	function printRes(data){
		prepareResult();
		if(data!="" && data[1]=='found') {
			var lastEnglish="";
			var lastPersian="";
			$('#result span').remove();
			$('#result br').remove();
			$.each(data[0], function() {
				$('<span></span>',{
						text: (this[0]!=lastEnglish)?this[0]:"",
						class: 'english'
				}).appendTo('#result');
				$('<span></span>',{
						text: (this[1]!=lastPersian)?this[1]:"",
						class: 'pull-right persian'
				}).appendTo('#result');
				$('<br />').appendTo('#result');
				lastEnglish=this[0];
				lastPersian=this[1];
			});
			$('#result').addClass('alert-info');
		}
		else if(data!="" && data[1]=='similer'){
			$('#result div').show();
			var lastEnglish="";
			var lastPersian="";
			$('#result span').remove();
			$('#result br').remove();
			$.each(data[0], function() {
				$('<span></span>',{
						text: (this[0]!=lastEnglish)?this[0]:"",
						class: 'english'
				}).appendTo('#result');
				$('<span></span>',{
						text: (this[1]!=lastPersian)?this[1]:"",
						class: 'pull-right persian'
				}).appendTo('#result');
				$('<br />').appendTo('#result');
				lastEnglish=this[0];
				lastPersian=this[1];
			});
			$('#result').addClass('alert-warnning');
		}
		else if(data!="" && data[1]=='notfound') {
			$('#result span.english').text("Not Found.");
			$('#result span.persian').text("");
			$('#result').addClass('alert-error');
		}
	}
	function prepareResult(){
		var res=$('#result')
		res.removeClass('english');
		res.removeClass('persian');
		res.removeClass('alert-info');
		res.removeClass('alert-warnning');
		res.removeClass('alert-error');
		res.children('div').hide();
		res.show();
	}
	
	/********************* Check Passwords *********************/
	$(".checkPass").submit(function(e){
		var passwords=$(this).find('input[type="password"]');
		console.log("aslkdjasd");
		if(passwords.eq(0).val()!=passwords.eq(1).val()) {
			alert("Passwords aren't same.");
			e.preventDefault();
		}
	});
	
	/********************* Show User Edit *********************/
	$(".showUserEdit").click(function() {
		var userInfo=$(this).parent().siblings();
		var form=$(this).data("divid");
		
		var addWordsPer=(userInfo.eq(2).text()=="Yes")?true:false;
		var removeWordsPer=(userInfo.eq(3).text()=="Yes")?true:false;
		var editUsersPer=(userInfo.eq(4).text()=="Yes")?true:false;
		
		$("#editUserUser").val(userInfo.eq(0).text());
		$("#editUserOldUser").val(userInfo.eq(0).text());
		$("#editUserEmail").val(userInfo.eq(1).text());
		$("#editUserAddWordsPer").attr('checked',addWordsPer);
		$("#editUserRemoveWordsPer").attr('checked',removeWordsPer);
		$("#editUserEditUsersPer").attr('checked',editUsersPer);
		
		$("#editUser").show();
	});
		
	/********************* Show Edit Info *********************/
	$("#showUserInfo").click(function() {				
		$("#editInfoUser").val($(this).siblings().find("td:contains('Username')").next().text());
		$("#editInfoOldUser").val($(this).siblings().find("td:contains('Username')").next().text());
		$("#editInfoEmail").val($(this).siblings().find("td:contains('Email')").next().text());
		
		$("#editInfo").show();
	});
});