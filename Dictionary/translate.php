<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/dictionary
 * 
 */
if (!empty($_POST)) {
    ob_start();
    require_once 'inc.php';
    setSessionV('conn',connect(1));
    json_decode($_POST['search']);
    $res=search($_POST['search']);
    header('Content-type: application/json');
    echo json_encode($res);
}
