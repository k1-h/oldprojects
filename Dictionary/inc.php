<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/dictionary
 * 
 */
session_start();
require_once 'include/authorization.php';
require_once 'include/database.php';
require_once 'include/dictionary.php';
require_once 'include/user.php';

if(empty($_SESSION['msg']))
    setSessionR(null,null,null);

function changeLoc($newLoc)
{
    header("Location: $newLoc");
}

function setSessionR($msg,$type,$newLoc)
{
    $_SESSION['msg']=$msg;
    $_SESSION['type']=$type;
    $_SESSION['newLoc']=$newLoc;
}
function setSessionV($key,$value)
{
    $_SESSION[$key]=$value;
}
