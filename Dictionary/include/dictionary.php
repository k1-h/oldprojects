<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/dictionary
 * 
 */
function search($value)
{
    $res=array();
    $value=trim($value);
    $value=mb_strtolower($value, 'UTF-8');
    $shortest = -1;
    $query=mysql_query("SELECT english,persian FROM dict", $_SESSION['conn']);
    while ($row=mysql_fetch_array($query)) {
        foreach ($row as $word) {
            $lev = levenshtein($word,$value);
            if ($lev == 0) {
                $closest = $row;
                $shortest = 0;
                $res[]=$row;
                break;
            }
            if ($lev <= $shortest || $shortest < 0) {
                $closest  = $row;
                $shortest = $lev;
            }
        }
    }
    if ($shortest == 0)
        $found='found';
    else if ($shortest <= 2) {
        $found='similer';
        $res[]=$closest;
    } else
        $found='notfound';

        return array($res,$found);
}
function addWord($english,$persian)
{
    if (!$_SESSION['addWords']) {
        setSessionR("You Don't Have Permission to Do This.","error","main.php");

        return;
    }
    /*$res=mysql_query("SELECT english FROM dict WHERE english = '".$english."';", $_SESSION['conn']);
    $row=mysql_fetch_array($res);
    if (!empty($row)) {
        setSessionR("English Word Exists.","error","main.php");

        return;
    }
    $res=mysql_query("SELECT persian FROM dict WHERE persian = '".$persian."';", $_SESSION['conn']);
    $row=mysql_fetch_array($res);
    if (!empty($row)) {
        setSessionR("Persian Equal Exists.","error","main.php");

        return;
    }*/
    $res=mysql_query("INSERT dict SET english='".$english."',persian='".$persian."';",$_SESSION['conn']);
    if($res)
        setSessionR("Word Successfully Added.","info","main.php");
    else
        setSessionR("Word Add Problem. ".mysql_error(),"error","main.php");
}

function removeWords($english,$persian)
{
    if (!$_SESSION['removeWords']) {
        setSessionR("You Don't Have Permission to Do This.","error","main.php");

        return;
    }
    $error=0;
    if(!empty($english))
        foreach ($english as $word) {
            $word=trim($word);
            $res=mysql_query("DELETE FROM dict WHERE english = '".strtolower($word)."';", $_SESSION['conn']);
            if (!$res) {
                $error++;

                return;
            }
        }
    if(!empty($persian))
        foreach ($persian as $word) {
            $word=trim($word);
            $res=mysql_query("DELETE FROM dict WHERE persian = '".$word."';", $_SESSION['conn']);
            if (!$res) {
                $error++;

                return;
            }
        }
    if($error)
        setSessionR("$error Word(s) Can't Be Deleted.","error","main.php");
    else
        setSessionR("Word(s) Successfully Removed.","info","main.php");
}

function getEnglishWords()
{
    $words=array();
    $res=mysql_query("SELECT english FROM dict ORDER BY english", $_SESSION['conn']);
    while ($row=mysql_fetch_array($res))
        $words[]= $row['english'];

    return $words;
}

function getPersianWords()
{
    $words=array();
    $res=mysql_query("SELECT persian FROM dict ORDER BY persian", $_SESSION['conn']);
    while ($row=mysql_fetch_array($res))
        $words[]= $row['persian'];

    return $words;
}
