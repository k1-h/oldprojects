<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/dictionary
 * 
 */
function register($user,$email,$pass,$addWords,$removeWords,$editUsers)
{
    $res=mysql_query("SELECT username FROM users WHERE username = '".$user."';", $_SESSION['conn']);
    $row=mysql_fetch_array($res);
    if (!empty($row)) {
        setSessionR("Username Exists, Pick Another Username.","error","index.php");

        return;
    }
    $res=mysql_query("SELECT email FROM users WHERE email = '".$email."';", $_SESSION['conn']);
    $row=mysql_fetch_array($res);
    if (!empty($row)) {
        setSessionR("Email Exists, Pick Another Email.","error","index.php");

        return;
    }
    $res=mysql_query("INSERT users SET username='".$user."',email='".$email."',password='".md5($pass)."',addWords=".$addWords.",removeWords=".$removeWords.",editUsers=".$editUsers.";",$_SESSION['conn']);
    if($res)
        setSessionR("Successfully Registered, Now Login.","success","index.php");
    else
        setSessionR("Registeration Problem. ".mysql_error(),"error","index.php");
}

function deleteUser($user)
{
    if ($user=="admin") {
        setSessionR("Admin Can't Deleted.","error","main.php");

        return;
    }
    $res=mysql_query("DELETE FROM users WHERE username = '".$user."';", $_SESSION['conn']);
    if($res)
        setSessionR("User Successfully Removed.","success","main.php");
    else
        setSessionR("User Remove Problem. ".mysql_error(),"error","main.php");
}

function editUsers($user,$email,$password,$addWords,$removeWords,$editUsers,$oldUser)
{
    if ($oldUser=="admin" && ($addWords+$removeWords+$editUsers)!=3) {
        setSessionR("Admin Permission Change Forbidden. ".mysql_error(),"error","main.php");

        return;
    }
    if(empty($password))
        $res=mysql_query("UPDATE users SET username='".$user."',email='".$email."',addWords=".$addWords.",removeWords=".$removeWords.",editUsers=".$editUsers." WHERE username='".$oldUser."';",$_SESSION['conn']);
    else
        $res=mysql_query("UPDATE users SET username='".$user."',email='".$email."',password='".md5($password)."',addWords=".$addWords.",removeWords=".$removeWords.",editUsers=".$editUsers." WHERE username='".$oldUser."';",$_SESSION['conn']);
    if ($res) {
        setSessionR("User Successfully Edited.","success","main.php");
        if($_SESSION['user']==$oldUser)
            userInfo($user);
    } else
        setSessionR("User Edit Problem. ".mysql_error(),"error","main.php");
}

function userInfo($user,$setSession=1)
{
    $res=mysql_query("SELECT * FROM users WHERE username = '".$user."';", $_SESSION['conn']);
    $row=mysql_fetch_array($res);
    if ($setSession) {
        setSessionV('user',$row['username']);
        setSessionV('email',$row['email']);
        setSessionV('addWords',$row['addWords']?1:0);
        setSessionV('removeWords',$row['removeWords']?1:0);
        setSessionV('editUsers',$row['editUsers']?1:0);
    }

    return $row;
}

function getUsersInfo()
{
    $users=array();
    $res=mysql_query("SELECT * FROM users ORDER BY username", $_SESSION['conn']);
    while ($row=mysql_fetch_array($res))
        $users[]= $row;

    return $users;
}
