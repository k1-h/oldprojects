<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/dictionary
 * 
 */
function connect($selectdb)
{
    $hostname='localhost';
    $username='root';
    $password='';
    $database='dictionary';

    $conn=mysql_connect($hostname,$username,$password);
    if (!$conn) {
        die('Connection failed. ' . mysql_error());
    }
    if ($selectdb) {
        $table=mysql_select_db($database,$conn);
        if (!$table) {
            echo 'Database select failed. ' . mysql_error();
            echo '<br />Did you installed database? If not goto <a href="install.php">database installation page</a>';
            die();
        }
    }
    mysql_set_charset('utf8',$conn);

    return $conn;
}

function installDB($filename)
{
    if (file_exists($filename)) {
        $f=fopen($filename,'r');
        while (!feof($f)) {
            $query=fgets($f);
            $res=mysql_query($query, $_SESSION['conn']);
            if ($res===FALSE) {
                setSessionR("Bad Formated or Not SQL File","error","install.php");
                break;
            }
        }
        if(feof($f))
            setSessionR("Successfully Installed Database File.","success","index.php");
        fclose($f);
    } else {
        setSessionR("Could not Open Input File.","error","install.php");
    }
}

function removeDB()
{
    if ($_SESSION['user']!='admin') {
        setSessionR("Just admin user can do this..","error","main.php");

        return;
    }
    $res=mysql_query("drop database dictionary", $_SESSION['conn']);
    if ($res) {
        setSessionR("Database Successfully Removed.","success","install.php");

        return;
    } else {
        setSessionR("Database Remove Problem. ".mysql_error(),"error","main.php");
    }
}
