<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/dictionary
 * 
 */
ob_start();
require_once 'inc.php';
setSessionV('conn',connect(0));
if (!empty($_POST['filename'])) {
    installDB($_POST['filename']);
}
if($_SESSION['newLoc']!="" && $_SESSION['newLoc']!="install.php")
    changeLoc($_SESSION['newLoc']);
?>
<!DOCTYPE html>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style>
        body {
            font-family:tahoma;
            background: url(img/dust.png) repeat 0 0;
        }
        div {margin:auto; margin-top:20px; width:20%;}
    </style>
    <title>Database Install</title>
    </head>
<body>
    <script src="js/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <?php if($_SESSION['msg']!=""): ?>
            <div id="msg" class="alert alert-<?php echo $_SESSION['type']; ?>">
            <?php echo $_SESSION['msg']; ?>
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            </div>
    <?php setSessionR(null,null,null); endif; ?>
    <div>
        <span>Enter Database Filename to Import(Default filename is "localhost.sql"):</span>
        <form method="post" action="install.php" enctype="multipart/form-data">
            <input type="text" name="filename" required="required" autofocus value="localhost.sql"/><br />
            <input type="submit" class="btn btn-primary" />
        </form>
    </div>
</body>
</html>
