###Description
A dictionary project. See screenshots for more info.

Change database password in include/database.php. Don't change database name.

This was university project.

###Specifications
+ Partial ACL implementation

+ Partial MVC implementation

+ Uses json for retrive translated words

+ Automatic database install

+ Jquery, Twitter bootstrap and Css3 for best user experince.

###Known bugs
This script uses mysql extension to connect to database, if you use newer PHP version and see Deprecated warring at top of application you need to change error reporting settings 
in php.ini.