import datetime
from hashlib import sha256
from random import random

from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.forms.models import modelform_factory

from lists.models import Member

MemberForm = modelform_factory(Member, fields=['name', 'address', 'list'])


def index(request):
    context = {'form': MemberForm()}
    return render(request, 'lists/index.html', context)


def add(request):
    if request.method == 'POST':
        user = MemberForm(request.POST).save()
        request.session['email'] = user.address
        return HttpResponseRedirect(reverse('lists:sent'))


def sent(request):
    context = {'email': request.session['email']}
    return render(request, 'lists/sent.html', context)


def activate(request, activation_id):
    member = get_object_or_404(Member, activation_id=activation_id)
    member.activate()
    context = {'message': 'You have successfully subscribed ' +
               'to TehPUG mailing list.'}
    return render(request, 'lists/activate.html', context)
