from django.contrib import admin
from lists.models import List, Member
from lists import tasks


class MemberInLine(admin.TabularInline):
    model = Member


class ListAdmin(admin.ModelAdmin):
    list_display = ('address', 'name', 'description', 'access_level',
                    'members_count', 'created_at', 'modified', 'status')
    list_filter = ('status', 'access_level')
    ordering = ('created_at',)
    radio_fields = {'access_level': admin.HORIZONTAL}
    search_fields = ('address', 'name', 'description')
    inlines = [MemberInLine]


def send_activation(modeladmin, request, queryset):
    for member in queryset:
        tasks.member_send_activation.delay(member.address)
send_activation.short_description = 'Send Activation Email'


class MemberAdmin(admin.ModelAdmin):
    list_display = ('address', 'name', 'list', 'subscribed', 'actived',
                    'created_at', 'modified', 'status')
    list_filter = ('list', 'status', 'subscribed')
    ordering = ('created_at',)
    search_fields = ('address', 'name', 'list')
    list_editable = ('subscribed', 'actived')
    actions = [send_activation]


admin.site.register(List, ListAdmin)
admin.site.register(Member, MemberAdmin)
