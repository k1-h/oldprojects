from datetime import datetime
from pprint import pformat
from hashlib import sha256
from random import random
import logging

from django.utils import timezone
from django.db import models
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

from tehpug import settings
from .managers import ListManager, MemberManager


def validate_address(email):
    validate_email(email)
    if not (email.split('@')[1] in getattr(settings, 'MAILGUN_DOMAINS')):
        raise ValidationError(u'List address hostname should' +
                              ' belong to your account domains')


class List(models.Model):
    ACCESS_LEVELS = (
        ('readonly', 'Read Only'),
        ('members', 'Members Only'),
        ('everyone', 'Everyone'))
    STATUS = (
        ('saved', 'List Saved on Database'),
        ('sending', 'Sending Request to Server'),
        ('success', 'List Added to Server'),
        ('failed', 'Error, Request Failed'))

    address = models.EmailField(unique=True, validators=[validate_address])
    name = models.CharField(max_length=40, blank=True)
    description = models.CharField(max_length=100, blank=True)
    access_level = models.CharField(max_length=10, choices=ACCESS_LEVELS,
                                    default='readonly')
    members_count = models.IntegerField(default=0, editable=False)
    created_at = models.DateTimeField(editable=False, default=datetime.today())
    modified = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS,
                              default='saved', editable=False)

    objects = ListManager()

    def save(self, update_server=True, *args, **kwargs):
        import tasks
        if update_server and self.id:
            old_address = List.objects.get(pk=self.id).address

        super(List, self).save(*args, **kwargs)

        if update_server:
            try:
                tasks.list_update.delay(old_address, self.address)
            except UnboundLocalError:
                tasks.list_add.delay(self.address)

    def delete(self, actual_delete=False, *args, **kwargs):
        if actual_delete:
            super(List, self).delete(*args, **kwargs)
        else:
            import tasks
            tasks.list_delete.delay(self.address)

    def __unicode__(self):
        return self.address


class Member(models.Model):
    STATUS = (
        ('saved', 'User Saved on Database'),
        ('sending_email', 'Sending Activation Email'),
        ('success_email', 'Activation Email Sent'),
        ('failed_email', 'Failed to Send Activation Email'),
        ('sending_server', 'Sending Request to Server'),
        ('success_server', 'User Added to Server'),
        ('failed_server', 'Failed to Add User to Server'))

    address = models.EmailField(unique=True)
    name = models.CharField(max_length=20, blank=True)
    list = models.ForeignKey('List')
    activation_id = models.CharField(max_length=200, editable=False)
    actived = models.BooleanField(default=False)
    subscribed = models.BooleanField(default=False)
    created_at = models.DateTimeField(editable=False, default=datetime.today())
    modified = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS,
                              default='saved', editable=False)

    objects = MemberManager()

    def save(self, update_server=True, *args, **kwargs):
        import tasks
        if update_server and self.id and self.actived:
            old_address = List.objects.get(pk=self.id).address
        if self.status == 'saved':
            self.activation_id = sha256(str(random())).hexdigest()

        super(Member, self).save(*args, **kwargs)

        if self.status == 'saved':
            tasks.member_send_activation.delay(self.address)
            update_server = False

        if update_server and self.actived:
            try:
                tasks.member_delete.delay(old_address, self.address)
            except UnboundLocalError:
                tasks.member_add.delay(self.address)

    def delete(self, actual_delete=False, *args, **kwargs):
        if actual_delete:
            super(Member, self).delete(*args, **kwargs)
        else:
            import tasks
            tasks.member_delete.delay(self.address)

    def activate(self):
        self.actived = True
        self.save()

    def __unicode__(self):
        return self.address
