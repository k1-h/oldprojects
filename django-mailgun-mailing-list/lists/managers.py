from django.db import models
from django.db.models.query import QuerySet


class ListQueryMixin(object):
    """ Methods that appear both in the manager and queryset. """
    def delete(self):
        # Use individual queries to the attachment is removed.
        for list in self.all():
            list.delete()


class ListQuerySet(ListQueryMixin, QuerySet):
    pass


class ListManager(ListQueryMixin, models.Manager):
    def get_query_set(self):
        return ListQuerySet(self.model, using=self._db)


class MemberQueryMixin(object):
    """ Methods that appear both in the manager and queryset. """
    def delete(self):
        # Use individual queries to the attachment is removed.
        for list in self.all():
            list.delete()


class MemberQuerySet(MemberQueryMixin, QuerySet):
    pass


class MemberManager(MemberQueryMixin, models.Manager):
    def get_query_set(self):
        return MemberQuerySet(self.model, using=self._db)
