from requests import HTTPError
import logging
from pprint import pformat

from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils import timezone

from tehpug import settings
from celery import shared_task
from .models import List, Member
from lists import mailgun


logger = logging.getLogger('lists')


@shared_task
def list_add(address):
    list = List.objects.get(address=address)
    list.status = 'sending'
    list.save(update_server=False)
    new_list = [list.address, list.name,
                list.description, list.access_level]
    try:
        response = mailgun.create_list(*new_list)
        naive_utc = timezone.datetime.strptime(response['list']['created_at'],
                                               '%a, %d %b %Y %H:%M:%S -0000')
        aware_utc = timezone.make_aware(naive_utc, timezone.utc)
        list.created_at = timezone.localtime(aware_utc)
        list.status = 'success'
    except HTTPError:
        list.status = 'failed'
    list.save(update_server=False)


@shared_task
def list_update(old_address, new_address):
    list = List.objects.get(address=new_address)
    list.status = 'sending'
    list.save(update_server=False)
    new_list = [list.address, list.name,
                list.description, list.access_level]
    try:
        response = mailgun.update_list(old_address, *new_list)
        list.status = 'success'
    except HTTPError:
        list.status = 'failed'
    list.save(update_server=False)


@shared_task
def list_delete(address):
    list = List.objects.get(address=address)
    list.status = 'sending'
    list.save(update_server=False)
    try:
        mailgun.delete_list(address)
        list.delete(actual_delete=True)
    except HTTPError:
        list.status = 'failed'
        list.save(update_server=False)


@shared_task
def member_send_activation(address):
    member = Member.objects.get(address=address)
    member.status = 'sending_email'
    member.save(update_server=False)
    email_vars = {'activation_id': member.activation_id,
                  'expiration_days': 3,
                  'site': getattr(settings, 'SITE')}
    subject = render_to_string('lists/activation_subject.txt', email_vars)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())

    message = render_to_string('lists/activation_email.txt', email_vars)

    try:
        logger.debug('Sending Email, To: ' + member.address)
        logger.debug('Sending Email, From: ' + member.list.address)
        logger.debug('Sending Email, Subject: ' + subject)
        send_mail(subject, message, member.list.address, [member.address],
                  fail_silently=False)
        member.status = 'success_email'
    except HTTPError:
        member.status = 'failed_email'
    member.save(update_server=False)


@shared_task
def member_add(list, address):
    member = Member.objects.get(address=address)
    member.status = 'sending_server'
    member.save(update_server=False)
    new_member = [member.list.address, member.address, member.name,
                  True, True]
    try:
        response = mailgun.add_member(*new_member)
        naive_utc = timezone.datetime.strptime(response['list']['created_at'],
                                               '%a, %d %b %Y %H:%M:%S -0000')
        aware_utc = timezone.make_aware(naive_utc, timezone.utc)
        member.created_at = timezone.localtime(aware_utc)
        member.subscribed = True
        member.status = 'success_server'
    except HTTPError:
        member.subscribed = False
        member.status = 'failed_server'
    member.save(update_server=False)


@shared_task
def member_update(old_address, new_address):
    member = Member.objects.get(address=new_address)
    member.status = 'sending_server'
    member.save(update_server=False)
    new_member = [member.list.address, member.address, member.name,
                  member.subscribed, True]
    try:
        response = mailgun.update_member(old_address, *new_member)
        member.status = 'success_server'
    except HTTPError:
        member.status = 'failed_server'
    member.save(update_server=False)


@shared_task
def member_delete(address):
    member = Member.objects.get(address=address)
    member.status = 'sending_server'
    member.save(update_server=False)
    try:
        mailgun.delete_member(address)
        member.delete(actual_delete=True)
    except HTTPError:
        member.status = 'failed_server'
        member.save(update_server=False)
