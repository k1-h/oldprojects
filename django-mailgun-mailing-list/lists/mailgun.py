import logging
from pprint import pformat
import json

import requests

from tehpug import settings


def call_api(method, api_url='', data=None):
    auth = ('api', getattr(settings, 'MAILGUN_ACCESS_KEY'))
    api_base_url = getattr(settings, 'MAILGUN_LISTS_BASE_URL')
    logger = logging.getLogger('lists')

    logger.debug('API Call, ' + method.upper() + ': ' + api_base_url + api_url)
    logger.debug('API Call, Data: ' + pformat(data))
    response = getattr(requests, method)(api_base_url + api_url,
                                         auth=auth, data=data)

    try:
        response_obj = response.json()
        response.raise_for_status()
        logger.debug('API Response: ' + pformat(response_obj))
        return response_obj
    except ValueError:
        msg = response.text
        logger.error(msg)
        raise requests.HTTPError(msg)
    except requests.HTTPError:
        msg = 'Error Code: {0}, Message: {1}'.format(
              response.status_code, response_obj['message'])
        logger.error(msg)
        raise requests.HTTPError(msg)


def get_lists(list_address=None):
    """
    GET /lists
    Returns a list of mailing lists under your account.

    GET /lists/<address>
    Returns a single mailing list by a given address.

    address:	Find a mailing list by it's address (optional)
    """
    api_url = ('/' + list_address) if list_address else ''

    # Don't get anything from mailgun
    return call_api('get', api_url)


def create_list(list_address, name=None,
                description=None, access_level=None):
    """
    POST /lists
    Creates a new mailing list.

    address: A valid email address for the mailing list
    name:	Mailing list name, e.g. Developers (optional)
    description:	A description (optional)
    access_level: List access level,one of:
    readonly(default), members, everyone

    {u'list': {u'access_level': u'ACCESS_LEVEL',
           u'address': u'ADDRESS',
           u'created_at': u'DATE',
           u'description': u'DESCRIPTION',
           u'members_count': INTEGER,
           u'name': u'NAME'},
    u'message': u'Mailing list has been created'}

    """
    data = {
        'address': list_address,
        'name': name,
        'description': description,
        'access_level': access_level}

    return call_api('post', data=data)


def update_list(old_list_address, new_list_address=None, name=None,
                description=None, access_level=None):
    """
    PUT /lists/<address>
    Update mailing list properties, such as address, description or name

    address:	New mailing list address, e.g. devs@mg.net (optional)
    name:	New name, e.g. My newsletter (optional)
    description:	Description string (optional)
    access_level:	List access level, one of:
    readonly (default), members, everyone

    {u'list': {u'access_level': u'ACCESS_LEVEL',
           u'address': u'ADDRESS',
           u'created_at': u'DATE',
           u'description': u'DESCRIPTION',
           u'members_count': INTEGER,
           u'name': u'NAME'},
    u'message': u'Mailing list has been updated'}
    """
    api_url = ('/' + old_list_address) if old_list_address else ''
    data = {
        'address': new_list_address,
        'name': name,
        'description': description,
        'access_level': access_level}

    return call_api('put', api_url, data)


def delete_list(list_address):
    """
    DELETE /lists/<address>
    Deletes a mailing list.
    """
    api_url = '/' + list_address

    return call_api('delete', api_url)


def get_members_list(list_address):
    """
    GET /lists/<address>/members
    Fetches the list of mailing list members.

    subscribed:	yes to list subscribed, no for unsubscribed, list all if not set
    limit;	Maximum number of records to return (100 by default)
    skip:	Records to skip (0 by default)
    """
    api_url = "/{0}/members".format(list_address) if list_address else ''

    # Don't get anything from mailgun
    return call_api('get', api_url)


def get_member(list_address, member_address):
    """
    GET /lists/<address>/members/<member_address>
    Retrieves a mailing list member.
    """
    api_url = "/{0}/members/{1}".format(list_address,
                                        member_address) if list_address else ''

    # Don't get anything from mailgun
    return call_api('get', api_url)


def add_member(list_address, member_address, name=None,
               subscribed=None, upsert=None, vars=None):
    """
    POST /lists/<address>/members
    Adds a member to the mailing list.

    address:	Valid email address specification, e.g. Alice <alice@example.com> or just alice@example.com
    name:	Optional member name
    vars:	JSON-encoded dictionary string with arbitrary parameters, e.g. {"gender":"female","age":27}
    subscribed:	yes to add as subscribed (default), no as unsubscribed
    upsert: yes to update member if present, no to raise error in case of a duplicate member (default)
    """
    api_url = "/{0}/members".format(list_address) if list_address else ''
    data = {
        'address': member_address,
        'name': name,
        'vars': json.dumps(vars),
        'subscribed': subscribed,
        'upsert': upsert}

    return call_api('post', api_url, data)


def add_members(list_address, members_address, subscribed=None):
    """
    POST /lists/<address>/members.json
    Adds multiple members, up to 1,000 per call, to a Mailing List.

    members	JSON-encoded array of email addresses, e.g. ["bob@example.com", alice@example.com"]. Custom variables can be provided, see examples.
    subscribed	no to set unsubscribed, yes as subscribed

    'members': '[{"address": "Alice <alice@example.com>", "vars": {"age": 26}},{"name": "Bob", "address": "bob@example.com", "vars": {"age": 34}}]'
    """
    api_url = "/{0}/members.json".format(list_address) if list_address else ''
    data = {
        'members': json.dumps(members_address),
        'subscribed': subscribed}

    return call_api('post', api_url, data)


def update_member(list_address, old_member_address,
                  new_member_address=None, name=None, vars=None,
                  subscribed=None):
    """
    PUT /lists/<address>/members/<member_address>
    Updates a mailing list member with given properties. Won't touch the property if it's not passed in.

    address:	Valid email address specification, e.g. Alice <alice@example.com> or just alice@example.com
    name:	Recipient name, e.g. Alice
    vars:	JSON-encoded dictionary string with arbitrary parameters, e.g. {"gender":"female","age":27}
    subscribed:	no to set unsubscribed, yes as subscribed
    """
    api_url = "/{0}/members/{1}".format(list_address, old_member_address) if list_address else ''
    data = {
        'address': new_member_address,
        'name': name,
        'vars': json.dumps(vars),
        'subscribed': subscribed}

    return call_api('put', api_url, data)


def remove_member(list_address, member_address):
    """
    DELETE /lists/<address>/members/<member_address>
    Delete a mailing list member.
    """
    api_url = "/{0}/members/{1}".format(list_address, member_address) if list_address else ''

    return call_api('delete', api_url)
