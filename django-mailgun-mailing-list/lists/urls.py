from django.conf.urls import patterns, url

from lists import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^add$', views.add, name='add'),
    url(r'^sent$', views.sent, name='sent'),
    url(r'^activate/(?P<activation_id>\S+)$',
        views.activate, name='activate'))
