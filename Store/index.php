<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/store
 */
require_once 'shop.php';
session_start();
$_SESSION['list']=array();
$s=new shop();
if(isset($_GET['do']))
switch ($_GET['do']) {
    case 'sort':
    usort($_SESSION['list'], $s->sortBy($_GET['by']));
    break;
    case 'del':
    $s->delItem($_POST['selected']);
    break;
    case 'add':
    $s->addItem($_POST['name'],$_POST['type'],$_POST['price'],$_POST['serial'],$_POST['date']);
    break;
    case 'search':
    $s->search($_POST['SearchFor'],$_POST['searchIn']);
    break;
    default:
    echo 'Bad Arg';
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Store</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style>
        label {display:inline;}
        .row-fluid {margin:10px;}
        body {font-family:tahoma;}
        .form-horizontal .controls {margin-left: 50px; margin-top:6px;}
        .form-horizontal .control-label {width: 10px;}
        .table-hover tbody tr:hover td, .table-hover tbody tr:hover th { background-color: #EBEBF5; }
    </style>
</head>
<body>
    <script src="js/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        checked = false;
        $(document).ready(function(){
            $("#checkall").click(function(){
                if (checked == false)
                    checked = true;
                else
                    checked = false;
                $(".check").each(function(){
                    $(this).attr('checked',checked);
                });
            });
            $('#SearchFor').keyup(function(){
                    $("tbody > tr").hide();
                    $("tbody > tr").each(function(){
                        if($(this).html().toLowerCase().indexOf($('#SearchFor').val().toLowerCase())!=-1)
                            $(this).show();
                    });
            });
        });
    </script>
    <div class="row-fluid">
        <div id="list" class="span6">
            <form method="post" name="list" id="list" action="index.php?do=del">
                <table border="1" cellspacing="0" cellpadding="5" class="table-striped table-hover">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checkall" name="checkall" value="checkall" /></th>
                            <th><a href="index.php?do=sort&by=name">Name</a></th>
                            <th><a href="index.php?do=sort&by=type">Type</a></th>
                            <th><a href="index.php?do=sort&by=price">Price</a></th>
                            <th><a href="index.php?do=sort&by=serial">Serial</a></th>
                            <th><a href="index.php?do=sort&by=date">Date</a></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $s->printTable(); ?>
                    </tbody>
                </table>
            </form>
        </div>
    <div class="tabbable span5">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#search" data-toggle="tab">Search</a></li>
            <li><a href="#add" data-toggle="tab">Add</a></li>
            <li><a href="#del" data-toggle="tab">Delete</a></li>
            <li><a href="index.php">Rest</a></li>
        </ul>
        <div class="tab-content">
            <div id="search" class="tab-pane active">
                <form method="post" action="index.php?do=search" class="form-horizontal">
                    <input type="text" id="SearchFor" name="SearchFor" placeholder="Enter Search Word"/>
                <!--<input type="submit" value="Search" class="btn"/><br />
                    <input type="radio" id="rdoName" name="searchIn" value="name" checked="checked"/>
                    <label for="rdoName">By Name </label>
                    <input type="radio" id="rdoType" name="searchIn" value="type"/>
                    <label for="rdoType">By Type </label>
                    <input type="radio" id="rdoPrice" name="searchIn" value="price"/>
                    <label for="rdoPrice">By Price </label>-->
                </form>
            </div>
            <div id="add" class="tab-pane">
                <form method="post" action="index.php?do=add" class="form-horizontal">
                    <div class="control-group">
                        <label for="name" class="control-label">Name:</label>
                        <div class="controls">
                            <input type="text" id="name" name="name" placeholder="Enter Item Name"  required="required"/><br />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="Type" class="control-label">Type:</label>
                        <div class="controls">
                            <select id="Type" name="type">
                                <?php $s->printTypes(); ?>
                            </select><br />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="Price" class="control-label">Price:</label>
                        <div class="controls">
                            <input type="text" id="Price" name="price" placeholder="Enter Item Price"  required="required"/><br />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="Serial" class="control-label">Serial:</label>
                        <div class="controls">
                            <input type="text" id="Serial" name="serial" placeholder="Enter Item Serial"  required="required"/><br />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="Date" class="control-label">Date:</label>
                        <div class="controls">
                            <input type="text" id="Date" name="date" placeholder="Enter Item Date" required="required" /><br />
                        </div>
                    </div>
                    <div class="controls">
                        <input type="submit" value="Add Item" class="btn"/>
                    </div>
                </form>
            </div>
            <div id="del" class="tab-pane">
                <input type="button" name="removeSelected" value="Remove Selected Items" class="btn" onclick="document.forms['list'].submit();"/>
            </div>
        </div>
    </div>
    </div>
</body>
</html>
