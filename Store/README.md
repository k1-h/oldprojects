###Description
A simple "computer store" project. See image for more info.

This was university project.

###Specifications
+ Object-oriented php

+ Partial MVC implementation

+ Csv for saving data

+ Twitter bootstrap for nice looking

+ Jquery for fast client-side search

###Notes
Apache must have write access to "data" file.

###Known bugs
When writeing new data to file script don't check if Price, Serial and Date are valid or not.

You can add it easily with html5 pattern attribute of input element.
