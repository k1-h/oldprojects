<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/store
 */
class shop
{
    public $types=array("CPU",
            "DVD R\W",
            "GPU",
            "HardDisk",
            "Modem",
            "Printer",
            "Power",
            "Ram",
            "Motherboard",
            "Sound Card",
            "Speaker");
    private $itemsCount=0;
    function __construct()
    {
        $this->readArr();
    }
    public function readArr()
    {
        $handle=fopen('data','r');
        if(!$handle) die('Input file error');
        $i=0;
        while (($data = fgetcsv($handle)) !== FALSE) {
            $_SESSION['list'][$this->itemsCount]["name"]=$data[0];
            $_SESSION['list'][$this->itemsCount]["type"]=$data[1];
            $_SESSION['list'][$this->itemsCount]["price"]=$data[2];
            $_SESSION['list'][$this->itemsCount]["serial"]=$data[3];
            $_SESSION['list'][$this->itemsCount]["date"]=$data[4];
            $this->itemsCount++;
        };
        fclose($handle);
    }
    public function sortBy($by)
    {
        return function ($a, $b) use ($by) {
            return strnatcmp($a[$by], $b[$by]);
        };
    }
    public function addItem($name,$type,$price,$serial,$date)
    {
        $_SESSION['list'][$this->itemsCount]['name']=$name;
        $_SESSION['list'][$this->itemsCount]['type']=$type;
        $_SESSION['list'][$this->itemsCount]['price']=$price;
        $_SESSION['list'][$this->itemsCount]['serial']=$serial;
        $_SESSION['list'][$this->itemsCount]['date']=$date;
        $this->itemsCount++;
        $this->writeArr();
    }
    public function delItem($items)
    {
        for ($i=0;$i<count($items);$i++) {
            array_splice($_SESSION['list'],$items[$i]-$i,1); }
        $this->writeArr();
    }
    public function writeArr()
    {
        $handle=fopen('data','w');
        if(!$handle) die('Input file error');
        foreach ($_SESSION['list'] as $row) {
            fputcsv($handle,$row,',');
        };
        fclose($handle);
    }
    public function search($value,$type)
    {
        $res=array();
        if ($type=='type') {
            foreach($this->types as $key=>$row)
            if(!(stripos($row,$value)===false))
                $value=$key;
        }
        foreach ($_SESSION['list'] as $row) {
            $key = stripos($row[$type],$value);
            if(!($key===false) || $row[$type]==$value)
                $res[]=$row;
        }
        $_SESSION['list']=array();
        $_SESSION['list']=array_merge($_SESSION['list'],$res);
    }
    public function printTable()
    {
        foreach ($_SESSION['list'] as $rowkey=>$row) {
            echo "<tr>\n\t<td><input type=\"checkbox\" class=\"check\" name=\"selected[]\" value=\"$rowkey\"/></td>\n";
            $rowkey++;
            foreach ($row as $colkey=>$col) {
                $item=$colkey=="type"?$this->types[$col]:$col;
                echo "\t<td>$item</td>\n";
            }
            echo "</tr>\n";
        }
    }
    public function printTypes()
    {
        foreach ($this->types as $key=>$type)
            echo "\t\t<option value=\"$key\">$type</option>\n";
    }
};
