import math
import numpy
import sys

import Image
import ImageDraw


def ellipse_rad(draw, x_c, y_c, r_x, r_y, color):
    if r_x:
        for x in range(x_c - r_x, x_c + r_x):
            rad = (r_y * 1.0 / r_x) * (r_x ** 2 - (x - x_c) ** 2) ** 0.5
            draw.point([x, y_c + rad], fill=color)
            draw.point([x, y_c - rad], fill=color)


def ellipse_rad_line(draw, x_c, y_c, r_x, r_y, color):
    if r_x:
        for x in range(x_c - r_x, x_c + r_x):
            rad = (r_y * 1.0 / r_x) * (r_x ** 2 - (x - x_c) ** 2) ** 0.5
            rad_next = ((r_y * 1.0 / r_x) *
                        (r_x ** 2 - (x - x_c + 1) ** 2) ** 0.5)
            draw.line([x, y_c + rad, x + 1, y_c + rad_next], fill=color)
            draw.line([x, y_c - rad, x + 1, y_c - rad_next], fill=color)


def ellipse_rad_point(draw, x_c, y_c, r_x, r_y, color):
    if r_x:
        for x in range(x_c - r_x, x_c + r_x):
            rad = (r_y * 1.0 / r_x) * (r_x ** 2 - (x - x_c) ** 2) ** 0.5
            draw.point([x, y_c + rad], fill=color)
            draw.point([x, y_c - rad], fill=color)
        for y in range(y_c - r_y, y_c + r_y):
            rad = (r_x * 1.0 / r_y) * (r_y ** 2 - (y - y_c) ** 2) ** 0.5
            draw.point([x_c + rad, y], fill=color)
            draw.point([x_c - rad, y], fill=color)


def arc(draw, x_c, y_c, r_x, r_y, color, begin_angle, end_angle):
    for teta in range(begin_angle, end_angle):
        x = r_x * math.cos(teta * math.pi / 180)
        y = r_y * math.sin(teta * math.pi / 180)
        draw.point([x + x_c, y + y_c], fill=color)


def ellipse_par4(draw, x_c, y_c, r_x, r_y, color):
    for teta in range(0, 90):
        x = r_x * math.cos(teta * math.pi / 180)
        y = r_y * math.sin(teta * math.pi / 180)
        draw.point([x + x_c, y + y_c], fill=color)
        draw.point([x + x_c, -y + y_c], fill=color)
        draw.point([-x + x_c, y + y_c], fill=color)
        draw.point([-x + x_c, -y + y_c], fill=color)


def ellipse_par8(draw, x_c, y_c, r_x, r_y, color):
    for teta in range(0, 45):
        x = r_x * math.cos(teta * math.pi / 180)
        y = r_y * math.sin(teta * math.pi / 180)
        draw.point([x + x_c, y + y_c], fill=color)
        draw.point([x + x_c, -y + y_c], fill=color)
        draw.point([-x + x_c, y + y_c], fill=color)
        draw.point([-x + x_c, -y + y_c], fill=color)
        draw.point([y + x_c, x + y_c], fill=color)
        draw.point([y + x_c, -x + y_c], fill=color)
        draw.point([-y + x_c, x + y_c], fill=color)
        draw.point([-y + x_c, -x + y_c], fill=color)


def ellipse_par1_con_line(draw, x_c, y_c, r_x, r_y, color):
    x_old = x_c + r_x
    y_old = y_c
    for teta in range(360):
        x = r_x * math.cos(teta * math.pi / 180)
        y = r_y * math.sin(teta * math.pi / 180)
        draw.line([x_old, y_old, x_c + x, y_c + y], color)
        x_old = x + x_c
        y_old = y + y_c


def ellipse_par1_con_r(draw, x_c, y_c, r_x, r_y, color):
    if r_x > r_y:
        t_inc = 360 / (2 * math.pi * r_x)
    else:
        t_inc = 360 / (2 * math.pi * r_y)
    for teta in numpy.arange(1, 360, t_inc):
        x = r_x * math.cos(teta * math.pi / 180)
        y = r_y * math.sin(teta * math.pi / 180)
        draw.point([x + x_c, y + y_c], color)


def ellipse_brez(draw, x_c, y_c, r, color):
    x = -r
    y = 0
    err = 2 - 2 * r
    while x < 0:
        draw.point([x_c - x, y_c + y], color)
        draw.point([x_c - y, y_c - x], color)
        draw.point([x_c + x, y_c - y], color)
        draw.point([x_c + y, y_c + x], color)
        r = err
        if r <= y:
            y += 1
            err += y * 2 + 1
        if r > x or err > y:
            x += 1
            err += x * 2 + 1

if __name__ == '__main__':
    pic = Image.new(mode='RGB', size=[2000, 2000], color='white')
    draw = ImageDraw.Draw(pic)
    x_c = int(sys.argv[1])
    y_c = int(sys.argv[2])
    r_x = int(sys.argv[3])
    r_y = int(sys.argv[4])
    ellipse_par1_con_r(draw, x_c, y_c, r_x, r_y, 'black')
    pic.save('ellispe.png', 'png')
