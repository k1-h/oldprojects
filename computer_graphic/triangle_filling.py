import math
import numpy
import time
import sys

import Image
import ImageDraw

from line import get_points


def tri_brez(draw, x1, y1, x2, y2, x3, y3, color):
    dx = abs(x2 - x1)
    dy = -abs(y2 - y1)
    sx = 1 if x1 < x2 else -1
    sy = 1 if y1 < y2 else -1
    err = dx + dy
    x = x1
    y = y1
    while True:
        draw.line([x, y, x3, y3], color)
        e2 = 2 * err
        if e2 >= dy:
            if x == x2:
                break
            err += dy
            x += sx
        if e2 <= dx:
            if y == y2:
                break
            err += dx
            y += sy


def tri_by_tri(draw, x1, y1, x2, y2, x3, y3, color):
    mid_1_2 = [(x1 + x2) / 2, (y1 + y2) / 2]
    mid_2_3 = [(x2 + x3) / 2, (y2 + y3) / 2]
    mid_1_3 = [(x1 + x3) / 2, (y1 + y3) / 2]

    points_1_to_2_3 = get_points(x1, y1, *mid_2_3)
    points_2_to_1_3 = get_points(x2, y2, *mid_1_3)
    points_3_to_1_2 = get_points(x3, y3, *mid_1_2)

    while points_1_to_2_3 and points_2_to_1_3 and points_3_to_1_2:
        draw.line([x1, y1, x2, y2], color)
        draw.line([x2, y2, x3, y3], color)
        draw.line([x1, y1, x3, y3], color)

        x1, y1 = points_1_to_2_3.pop()
        x2, y2 = points_2_to_1_3.pop()
        x3, y3 = points_3_to_1_2.pop()


if __name__ == '__main__':
    pic = Image.new(mode='RGB', size=[2000, 2000], color='white')
    draw = ImageDraw.Draw(pic)
    x1 = int(sys.argv[1])
    y1 = int(sys.argv[2])
    x2 = int(sys.argv[3])
    y2 = int(sys.argv[4])
    x3 = int(sys.argv[5])
    y3 = int(sys.argv[6])
    tri_by_tri(draw, x1, y1, x2, y2, x3, y3, 'black')
    pic.save('tri_filling.png', 'png')
