import random

import Image
import ImageDraw

from line import get_points
from triangle_filling import tri_by_tri


def one_angle(draw, num_point, points, color):
    for i in range(1, num_point - 1):
        tri_by_tri(draw,
                   points[0][0], points[0][1],
                   points[i][0], points[i][1],
                   points[i + 1][0], points[i + 1][1],
                   color)


def my_fill(draw, num_point, points, color):
    x_min = x_max = points[0][0]
    y_min = y_max = points[0][1]
    border_points = {}
    for i in range(1, num_point + 1):
        x_min = min(x_min, points[i][0])
        y_min = min(y_min, points[i][1])
        x_max = max(x_max, points[i][0])
        y_max = max(y_max, points[i][1])
        line_points = get_points(points[i - 1][0],
                                 points[i - 1][1],
                                 points[i][0],
                                 points[i][1])
        for i in range(len(line_points) - 1):
            if not (line_points[i][0] == line_points[i + 1][0] and
                    line_points[i][1] == (line_points[i + 1][1] + 1)):
                border_points.setdefault(line_points[i][0],
                                         []).append(line_points[i][1])
    for i in range(x_min, x_max):
        try:
            while True:
                print border_points[i]
                y1 = border_points[i].pop(0)
                y2 = border_points[i].pop()
                draw.line([i, y1, i, y2], color)
        except IndexError:
            pass


if __name__ == '__main__':
    pic = Image.new(mode='RGB', size=[2000, 2000], color='white')
    draw = ImageDraw.Draw(pic)
    points = []
    angles = int(raw_input('Enter number of angles: '))
    for i in range(angles):
        x = int(raw_input('Enter point x of angle {}: '.format(i + 1)))
        y = int(raw_input('Enter point y of angle {}: '.format(i + 1)))
        points.append([x, y])
    my_fill(draw, angles, points, 'black')
    pic.save('poly_filling.png', 'png')
