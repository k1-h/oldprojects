import math
import numpy
import time
import sys

import Image
import ImageDraw

from general_filling import general_filling_rec

def circle_rad(draw, x_c, y_c, r, color):
    for x in numpy.arange(x_c - r, x_c + r, 1):
        rad = (r ** 2 - (x - x_c) ** 2) ** 0.5
        draw.point([x, y_c + rad], fill=color)
        draw.point([x, y_c - rad], fill=color)


def circle_rad_line(draw, x_c, y_c, r, color):
    for x in numpy.arange(x_c - r, x_c + r, 1):
        rad = (r ** 2 - (x - x_c) ** 2) ** 0.5
        rad_next = (r ** 2 - (x + 1 - x_c) ** 2) ** 0.5
        draw.line([x, y_c + rad, x + 1, y_c + rad_next], fill=color)
        draw.line([x, y_c - rad, x + 1, y_c - rad_next], fill=color)


def circle_rad_point(draw, x_c, y_c, r, color):
    for x in numpy.arange(x_c - r * 3 / 4, x_c + r * 3 / 4, 1):
        rad = (r ** 2 - (x - x_c) ** 2) ** 0.5
        draw.point([x, y_c + rad], fill=color)
        draw.point([x, y_c - rad], fill=color)
    for y in numpy.arange(y_c - r * 3 / 4, y_c + r * 3 / 4, 1):
        rad = (r ** 2 - (y - y_c) ** 2) ** 0.5
        draw.point([x_c + rad, y], fill=color)
        draw.point([x_c - rad, y], fill=color)


def arc(draw, x_c, y_c, r, color, begin_angle, end_angle):
    for teta in range(begin_angle, end_angle):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.point([x + x_c, y + y_c], fill=color)


## def seesaw(x_c, y_c, r, color, end_angle):
##     teta = 0
##     counter = 0
##     images = []
##     while counter < 1:
##         pic = Image.new(mode='RGB', size=[200, 200], color='white')
##         draw = ImageDraw.Draw(pic)
##         x = r * math.cos(teta * math.pi / 180)
##         y = r * math.sin(teta * math.pi / 180)
##         draw.line([-x + x_c, -y + y_c, x + x_c, y + y_c], color)
##         if int(teta) == 0:
##             inc = -360 / (2 * math.pi * r)
##         elif int(teta) == end_angle:
##             inc = 360 / (2 * math.pi * r)
##         teta += inc
##         images.append(pic.copy())
##         time.sleep(0.1)
##         counter += 1

##     writeGif('seesaw.gif', images)


def circle_par4(draw, x_c, y_c, r, color):
    for teta in range(0, 90):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.point([x + x_c, y + y_c], fill=color)
        draw.point([x + x_c, -y + y_c], fill=color)
        draw.point([-x + x_c, y + y_c], fill=color)
        draw.point([-x + x_c, -y + y_c], fill=color)


def circle_par8(draw, x_c, y_c, r, color):
    for teta in range(0, 45):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.point([x + x_c, y + y_c], fill=color)
        draw.point([x + x_c, -y + y_c], fill=color)
        draw.point([-x + x_c, y + y_c], fill=color)
        draw.point([-x + x_c, -y + y_c], fill=color)
        draw.point([y + x_c, x + y_c], fill=color)
        draw.point([y + x_c, -x + y_c], fill=color)
        draw.point([-y + x_c, x + y_c], fill=color)
        draw.point([-y + x_c, -x + y_c], fill=color)


def circle_par1_con_line(draw, x_c, y_c, r, color):
    x_old = x_c + r
    y_old = y_c
    for teta in range(360):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.line([x_old, y_old, x_c + x, y_c + y], color)
        x_old = x + x_c
        y_old = y + y_c


def circle_par1_con_r(draw, x_c, y_c, r, color):
    t_inc = 360 / (2 * math.pi * r)
    for teta in numpy.arange(1, 360, t_inc):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.point([x + x_c, y + y_c], color)


def circle_brez(draw, x_c, y_c, r, color):
    x = -r
    y = 0
    err = 2 - 2 * r
    while x < 0:
        draw.point([x_c - x, y_c + y], color)
        draw.point([x_c - y, y_c - x], color)
        draw.point([x_c + x, y_c - y], color)
        draw.point([x_c + y, y_c + x], color)
        r = err
        if r <= y:
            y += 1
            err += y * 2 + 1
        if r > x or err > y:
            x += 1
            err += x * 2 + 1

if __name__ == '__main__':
    pic = Image.new(mode='RGB', size=[2000, 2000], color='white')
    draw = ImageDraw.Draw(pic)
    x = float(sys.argv[1])
    y = float(sys.argv[2])
    r = float(sys.argv[3])
    # circle_rad(draw, x, y, r, 'black')
    # circle_rad_point(draw, x, y, r, 'black')
    # arc(draw, x, y, r, 'blue', 0, 360)
    # circle_par4(draw, x, y, r, 'red')
    # circle_par8(draw, x, y, r, 'green')
    # circle_par1_con_line(draw, x, y, r, 'yellow')
    circle_par1_con_r(draw, x, y, r, 'blue')
    # circle_brez(draw, x, y, r, 'blue')
    general_filling_rec('blue', 'black', 2000, 2000, draw, pic, x, y)
    #pic.save('circle.png', 'png')
