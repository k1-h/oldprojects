import math
import numpy
import random
import time
import sys

import Image
import ImageDraw

from tri_filling import tri_brez


def fill_rec_hor(draw, x1, y1, x2, y2, color):
    if y1 <= y2:
        for y in range(y1, y2):
            draw.line([x1, y, x2, y], fill=color)


def fill_rec_ver(draw, x1, y1, x2, y2, color):
    if x1 <= x2:
        for x in range(x1, x2):
            draw.line([x, y1, x, y2], fill=color)


def fill_rec_by_rec(draw, x1, y1, x2, y2, color):
    dx = x2 - x1
    dy = y2 - y1
    if dx < dy:
        len = dx / 2
    else:
        len = dy / 2
    for i in range(len + 1):
        draw.rectangle([x1 + i, y1 + i, x2 - i, y2 - i], outline=color)


def fill_rec_by_rec_rec(draw, x1, y1, x2, y2, color):
    if x1 <= x2 and y1 <= y2:
        draw.rectangle([x1, y1, x2, y2], outline=color)
        fill_rec_by_rec_rec(draw, x1 + 1, y1 + 1, x2 - 1, y2 - 1, color)


def fill_rec_diag1(draw, x1, y1, x2, y2, color):
    for x in range(x2, x1, -1):
        draw.line([x1, y1, x, y2], color)
        draw.line([x2, y2, x, y1], color)


def fill_rec_diag_brez1(draw, x1, y1, x2, y2, color):
    dx = abs(x2 - x1)
    dy = -abs(y2 - y1)
    sx = 1 if x1 < x2 else -1
    sy = 1 if y1 < y2 else -1
    err = dx + dy
    x = x1
    y = y1
    while True:
        draw.line([x1, y2, x, y], color)
        draw.line([x2, y1, x, y], color)
        e2 = 2 * err
        if e2 >= dy:
            if x == x2:
                break
            err += dy
            x += sx
        if e2 <= dx:
            if y == y2:
                break
            err += dx
            y += sy


def fill_rec_diag_brez2(draw, x1, y1, x2, y2, color):
    dx = abs(x2 - x1)
    dy = -abs(y2 - y1)
    sx = 1 if x1 < x2 else -1
    sy = 1 if y1 < y2 else -1
    err = dx + dy
    x = x1
    y = y1
    while True:
        draw.line([x, y1, x, y], color)
        draw.line([x, y2, x, y], color)
        e2 = 2 * err
        if e2 >= dy:
            if x == x2:
                break
            err += dy
            x += sx
        if e2 <= dx:
            if y == y2:
                break
            err += dx
            y += sy


def rand_point(draw, x1, y1, x2, y2, color):
    x_rand = random.random() * (x2 - x1)
    y_rand = random.random() * (y2 - y1)
    tri_brez(draw, x1, y1, x2, y1, x1 + x_rand, y1 + y_rand, 'yellow')
    tri_brez(draw, x1, y2, x2, y2, x1 + x_rand, y1 + y_rand, 'blue')
    tri_brez(draw, x1, y1, x1, y2, x1 + x_rand, y1 + y_rand, 'red')
    tri_brez(draw, x2, y1, x2, y2, x1 + x_rand, y1 + y_rand, 'green')

if __name__ == '__main__':
    pic = Image.new(mode='RGB', size=[2000, 2000], color='white')
    draw = ImageDraw.Draw(pic)
    x1 = int(sys.argv[1])
    y1 = int(sys.argv[2])
    x2 = int(sys.argv[3])
    y2 = int(sys.argv[4])
    rand_point(draw, x1, y1, x2, y2, 'black')
    pic.save('rec_filling.png', 'png')
