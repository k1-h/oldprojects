import math
import numpy
import sys

import Image
import ImageDraw

from ellipse import ellipse_par1_con_r


def by_ellipse(draw, x_c, y_c, r_x, r_y, color):
    r_min = min(r_x, r_y)
    for i in range(0, r_min, 5):
        if r_x > r_y:
            ellipse_par1_con_r(draw, x_c, y_c, r_x - r_y + i, i, color)
        else:
            ellipse_par1_con_r(draw, x_c, y_c, i, r_y - r_x + i, color)


def by_ellipse_rx(draw, x_c, y_c, r_x, r_y, color):
    for i in range(r_x):
        ellipse_par1_con_r(draw, x_c, y_c, i, r_y, color)


def by_ellipse_ry(draw, x_c, y_c, r_x, r_y, color):
    for i in range(r_y):
        ellipse_par1_con_r(draw, x_c, y_c, r_x, i, color)


def radius(draw, x_c, y_c, r_x, r_y, color):
    if r_x > r_y:
        t_inc = 360 / (2 * math.pi * r_x)
    else:
        t_inc = 360 / (2 * math.pi * r_y)
    for teta in numpy.arange(0, 360, t_inc):
        x = r_x * math.cos(teta * math.pi / 180)
        y = r_y * math.sin(teta * math.pi / 180)
        draw.line([x_c, y_c, x + x_c, y + y_c], color)


def diag(draw, x_c, y_c, r_x, r_y, color):
    if r_x > r_y:
        t_inc = 360 / (2 * math.pi * r_x)
    else:
        t_inc = 360 / (2 * math.pi * r_y)
    for teta in numpy.arange(0, 180, t_inc):
        x = r_x * math.cos(teta * math.pi / 180)
        y = r_y * math.sin(teta * math.pi / 180)
        draw.line([x_c - x, y_c - y, x + x_c, y + y_c], color)


def chord_ver(draw, x_c, y_c, r_x, r_y, color):
    if r_x > r_y:
        t_inc = 360 / (2 * math.pi * r_x)
    else:
        t_inc = 360 / (2 * math.pi * r_y)
    for teta in numpy.arange(0, 180, t_inc):
        x = r_x * math.cos(teta * math.pi / 180)
        y = r_y * math.sin(teta * math.pi / 180)
        draw.line([x_c + x, y_c - y, x + x_c, y + y_c], color)


def chord_hor(draw, x_c, y_c, r_x, r_y, color):
    if r_x > r_y:
        t_inc = 360 / (2 * math.pi * r_x)
    else:
        t_inc = 360 / (2 * math.pi * r_y)
    for teta in numpy.arange(90, 270, t_inc):
        x = r_x * math.cos(teta * math.pi / 180)
        y = r_y * math.sin(teta * math.pi / 180)
        draw.line([x_c + x, y_c + y, x_c - x, y_c + y], color)


def globe(draw, x_c, y_c, r_x, r_y, color):
    r_max = max(r_x, r_y)
    for i in range(0, r_max, 50):
        ellipse_par1_con_r(draw, x_c, y_c, i, r_y, color)
        ellipse_par1_con_r(draw, x_c, y_c, r_x, i, color)


if __name__ == '__main__':
    pic = Image.new(mode='RGB', size=[2000, 2000], color='white')
    draw = ImageDraw.Draw(pic)
    x_c = int(sys.argv[1])
    y_c = int(sys.argv[2])
    r_x = int(sys.argv[3])
    r_y = int(sys.argv[4])
    by_ellipse(draw, x_c, y_c, r_x, r_y, 'black')
    pic.save('ellipse_filling.png', 'png')
