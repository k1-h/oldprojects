import Image
import ImageDraw


def draw_poly(draw, num_point, points, color):
    for i in range(num_point - 1):
        draw.line(points[i] + points[i + 1], color)


if __name__ == '__main__':
    pic = Image.new(mode='RGB', size=[640, 480], color='white')
    draw = ImageDraw.Draw(pic)
    points = []
    angles = int(raw_input('Enter number of angles: '))
    for i in range(angles):
        x = int(raw_input('Enter point x of angle {}: '.format(i + 1)))
        y = int(raw_input('Enter point y of angle {}: '.format(i + 1)))
        points.append([x, y])
    draw_poly(draw, angles, points, 'black')
    pic.save('poly.png', 'png')
