import math
import numpy
import random
import time
import sys

import Image
import ImageDraw

from circle import circle_brez


def by_circle(draw, x_c, y_c, r, color):
    for i in range(r):
        circle_brez(draw, x_c, y_c, i, color)


def radius(draw, x_c, y_c, r, color):
    t_inc = 360 / (2 * math.pi * r)
    for teta in numpy.arange(0, 360, t_inc):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.line([x_c, y_c, x + x_c, y + y_c], color)


def diag(draw, x_c, y_c, r, color):
    t_inc = 360 / (2 * math.pi * r)
    for teta in numpy.arange(0, 180, t_inc):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.line([x_c - x, y_c - y, x + x_c, y + y_c], color)


def chord_ver(draw, x_c, y_c, r, color):
    t_inc = 360 / (2 * math.pi * r)
    for teta in numpy.arange(0, 180, t_inc):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.line([x_c + x, y_c - y, x + x_c, y + y_c], color)


def chord_hor(draw, x_c, y_c, r, color):
    t_inc = 360 / (2 * math.pi * r)
    for teta in numpy.arange(90, 270, t_inc):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.line([x_c + x, y_c + y, x_c - x, y_c + y], color)


def rand_point(draw, x_c, y_c, r, color):
    rand_r = random.random() * r
    rand_teta = random.random() * 360
    x_rand = rand_r * math.cos(rand_teta * math.pi / 180)
    y_rand = rand_r * math.sin(rand_teta * math.pi / 180)
    for teta in numpy.arange(0, 360, 1):
        x = r * math.cos(teta * math.pi / 180)
        y = r * math.sin(teta * math.pi / 180)
        draw.line([x_c + x_rand, y_c + y_rand, x + x_c, y + y_c], color)

if __name__ == '__main__':
    pic = Image.new(mode='RGB', size=[2000, 2000], color='white')
    draw = ImageDraw.Draw(pic)
    x_c = int(sys.argv[1])
    y_c = int(sys.argv[2])
    r = int(sys.argv[3])
    rand_point(draw, x_c, y_c, r, 'black')
    pic.save('circle_filling.png', 'png')
