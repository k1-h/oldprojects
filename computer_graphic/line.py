from timeit import timeit
import sys

from PIL import Image, ImageDraw


def get_points(a1, b1, a2, b2):
    points = []
    dx = abs(a2 - a1)
    dy = -abs(b2 - b1)
    sx = 1 if a1 < a2 else -1
    sy = 1 if b1 < b2 else -1
    err = dx + dy
    x = a1
    y = b1
    while True:
        points.append([x, y])
        e2 = 2 * err
        if e2 >= dy:
            if x == a2:
                break
            err += dy
            x += sx
        if e2 <= dx:
            if y == b2:
                break
            err += dx
            y += sy
    points.reverse()
    return points


def adeliham_line(draw, color, x1, y1, x2, y2):
    dx = abs(x2 - x1)
    dy = abs(y2 - y1)

    if x2 > x1:
        x_inc = 1
    elif x2 < x1:
        x_inc = -1
    else:
        x_inc = 0

    if y2 > y1:
        y_inc = 1
    elif y2 < y1:
        y_inc = -1
    else:
        y_inc = 0

    length = dx + 1 if dx > dy else dy + 1

    x = x1
    y = y1
    dx1 = dy1 = 0
    for i in xrange(length):  # eq: for(i=0; i<length; i++) {}
        draw.point([x, y], fill=color)
        dx1 += dx
        if dx1 >= length:
            dx1 -= length
            x += x_inc
        dy1 += dy
        if dy1 >= length:
            dy1 -= length
            y += y_inc


def bresenham_line(draw, color, x1, y1, x2, y2):
    dx = abs(x2 - x1)
    dy = -abs(y2 - y1)
    sx = 1 if x1 < x2 else -1
    sy = 1 if y1 < y2 else -1
    error = dx + dy

    while True:
        draw.point([x1, y1], color)
        print x1, y1
        if x1 == x2 and y1 == y2:
            break
        error2 = 2 * error
        if error2 <= dx:
            error += dx
            y1 += sy
        if error2 >= dy:
            error += dy
            x1 += sx


def dda_line(draw, color, x1, y1, x2, y2):
    dx = x2 - x1
    dy = y2 - y1

    if abs(dx) > abs(dy):
        len = abs(dx) + 1
    else:
        len = abs(dy) + 1

    x_inc = float(dx) / len
    y_inc = float(dy) / len

    #print dx, dy, x_inc, y_inc, len # For Debug
    x = x1
    y = y1
    for i in xrange(len):  # eq: for(i=0; i<len; i++) {}
        draw.point([x + .5, y + .5], fill=color)
        x += x_inc
        y += y_inc


if __name__ == '__main__':
    pic = Image.new(mode='RGB', size=[640, 480], color='white')
    draw = ImageDraw.Draw(pic)
    cords = [
        int(sys.argv[1]),
        int(sys.argv[2]),
        int(sys.argv[3]),
        int(sys.argv[4])
    ]
    dda_call = 'dda_line(draw, "blue", *cords)'
    adeliham_call = 'adeliham_line(draw, "red", *cords)'
    bresenham_call = 'bresenham_line(draw, "green", *cords)'
    draw_line_call = 'draw.line(cords, "black")'
    draw.line(cords, "black")
    bresenham_line(draw, "green", *cords)
    # setup = 'from __main__ import draw, cords, adeliham_line,\
    #     bresenham_line, dda_line'
    # number = 1000
    # dda_time = timeit(stmt=dda_call, setup=setup, number=number)
    # adeliham_time = timeit(stmt=adeliham_call, setup=setup, number=number)
    # bresenham_time = timeit(stmt=bresenham_call, setup=setup, number=number)
    # draw_line_time = timeit(stmt=draw_line_call, setup=setup, number=number)
    # print 'DDA line algorithm time:', dda_time  # 4.13459205627
    # print 'Adeliham\'s line algorithm time:', adeliham_time  # 4.23598504066
    # print 'Bresenham\'s line algorithm time:', bresenham_time  # 4.18905210495
    # print 'Python\'t Draw line time:', draw_line_time  # 0.0129420757294
    pic.save('brez.png', 'png')
