import sys

import Image
import ImageColor
import ImageDraw


def general_filling_rec(fillcolor, bordercolor, maxX, maxY, draw, pic, x, y):
    sys.setrecursionlimit(100000)
    fc = ImageColor.getrgb(fillcolor)
    bc = ImageColor.getrgb(bordercolor)

    def GFill(x, y):
        if 0 < x < maxX and 0 < y < maxY:
            color = pic.getpixel((x, y))
            if color != bc and color != fc:
                draw.point([x, y], fc)
                GFill(x + 1, y)
                GFill(x, y + 1)
                GFill(x - 1, y)
                GFill(x, y - 1)
