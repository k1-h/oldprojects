<?php
	class UsersController extends AppController {
		public $paginate = array(
			'fields' => array(
				'User.id',
				'User.username',
				'User.group_id',
				'User.post_count',
				'User.created',
				'User.modified',
				'Group.name'
			),
			'limit' => 10,
			'order' => array(
				'User.created' => 'desc'
			),
			'recursive' => 0
		);

		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('add','login','logout','initDB');
			$this->paginate['order']=array(
				'User.'.Configure::read('__users_default_sort_by') => Configure::read('__users_default_sort_order')
			);
			$this->paginate['limit']=Configure::read('__users_post_limit');
		}
		
		public function index($by = null,$order = null) {
			if(!(empty($by) && empty($order))) {
				$this->paginate['order']= array('User.'.$by => $order);
			}
			
			$this->set('users',$this->paginate('User'));
		}
		
		public function view($id = null) {
			$this->User->id = $id;
			if(!$this->User->exists()) {
				throw new NotFoundException(__('Invalid user'));
			}
			
			$this->set('user',$this->User->find('first',array('conditions'=>array('User.id' => $id))));
		}
		
		public function add() {
			if($this->request->is('post')) {
				$this->User->create();
				if($this->User->save($this->request->data)) {
					$this->Session->setFlash('The user has been saved', 'default', array('alert' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash('The user could not be saved. Please try again.', 'default', array('alert' => 'error'));
				}
			}
			$groups = $this->User->Group->find('list');
			$this->set(compact('groups'));
			$this->set('title_for_layout', 'Users - Register');
		}
		
		public function edit($id = null) {
			$this->User->id=$id;
			if(!$this->User->exists()) {
				throw new NotFoundException(__('Invalid user'));
			}$this->log('-1');
			if($this->request->is('post') || $this->request->is('put')) {$this->log('0');
				if($this->User->save($this->request->data)) {$this->log('1');
					$this->Session->setFlash('The user has been saved', 'default', array('alert' => 'success'));$this->log('2');
					$this->redirect(array('action' => 'index'));$this->log('3');
				} else {
					$this->Session->setFlash('The user could not be saved. Please try again.', 'default', array('alert' => 'error'));
				}
			} else {
				$userInfo=$this->User->read(null,$id);
				$groups = $this->User->Group->find('list');
				$this->set(compact('groups'));
				$this->set('title_for_layout', 'Users - '.$userInfo['User']['username']);
				$this->request->data=$userInfo;
				unset($this->request->data['User']['password']);
			}
		}
		
		public function login() {
			if($this->request->is('post')) {
				if($this->Auth->login()) {
					if (!empty($this->data) && $this->data['User']['remember_me']) {
						$cookie = array();
						$cookie = $this->Auth->user();
						$cookie['password'] = $this->data['User']['password'];
						$this->Cookie->write('Auth.User', $cookie, true, '+2 weeks');
						unset($this->data['User']['remember_me']);
					}
					$this->redirect($this->Auth->redirect());
				} else {
					$this->Session->setFlash('Invalid username or passowrd, try again', 'default', array('alert' => 'error'));
				}
			}
			if (empty($this->data)) {
				$cookie = $this->Cookie->read('Auth.User');
				if (!is_null($cookie)) {
					if ($this->Auth->login($cookie)) {
						//  Clear auth message, just in case we use it.
						$this->Session->delete('Message.auth');
						$this->redirect($this->Auth->redirect());
					} else { // Delete invalid Cookie
						$this->Cookie->delete('Auth.User');
					}
				}
			}
		}
		
		public function logout() {
			$this->Cookie->delete('Auth.User');
			$this->redirect($this->Auth->logout());
		}
		
		public function delete($id = null){
			if(!$this->request->is('post')){
				throw new MethodNotAllowedException();
			}
			$this->User->id=$id;
			if(!$this->User->exists()){
				throw new NotFoundException(__('Invalid user'));				
			}
			if($this->User->delete()) {
				$this->Session->setFlash('User deleted', 'default', array('alert' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash('User was not deleted', 'default', array('alert' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		
		public function initDB() {
			$group = $this->User->Group;
			//Allow admins to everything
			$this->Acl->allow('group18', 'controllers');
		
			//allow managers to posts and widgets
			/*$group->id = 2;
			$this->Acl->deny($group, 'controllers');
			$this->Acl->allow($group, 'controllers/Posts');
			$this->Acl->allow($group, 'controllers/Comments');
			$this->Acl->allow($group, 'controllers/Settings');
			$this->Acl->allow($group, 'controllers/Users/edit');
			$this->Acl->allow($group, 'controllers/Users/login');
			$this->Acl->allow($group, 'controllers/Users/logout');
		
			//allow users to only add and edit on posts and widgets
			$group->id = 3;
			$this->Acl->deny($group, 'controllers');
			$this->Acl->allow($group, 'controllers/Posts/add');
			$this->Acl->allow($group, 'controllers/Posts/edit');
			$this->Acl->allow($group, 'controllers/Comments/add');
			$this->Acl->allow($group, 'controllers/Comments/edit');
			$this->Acl->allow($group, 'controllers/Users/edit');
			$this->Acl->allow($group, 'controllers/Users/login');
			$this->Acl->allow($group, 'controllers/Users/logout');*/
			
			//we add an exit to avoid an ugly "missing views" error message
			echo "all done";
			exit;
		}
	}
?>