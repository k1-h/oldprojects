<?php
	class PagesController extends AppController {
		var $uses="Post";
		public $paginate = array(
			'fields' => array(
				'Post.id',
				'Post.title',
				'Post.body',
				'Post.created',
				'Post.modified',
				'Post.user_id',
				'Post.username',
				'Post.status',
				'Post.comment_count',
				'Post.category_name',
			),
			'order' => array(
				'Post.created' => 'desc'
			),
			'limit' => 10,
			'recursive' => 1,
		);
		
		public function beforeFilter(){
			parent::beforeFilter();
			$this->Auth->allow('index', 'view');
				
			$this->paginate['order']=array(
				'Post.'.Configure::read('__pages_default_sort_by') => Configure::read('__pages_default_sort_order')
			);
			$this->paginate['limit']=Configure::read('__pages_post_limit');
		}
		
		public function index($filter = null, $filterName = null) {
			if ($this->RequestHandler->isRss() ) {
				$posts = $this->paginate('Post');
				return $this->set('posts',$posts);
			}
			$posts=$this->paginate('Post');
			foreach ($posts as $key => $post) {
				$posts[$key]['Post']['body']=substr($post['Post']['body'], 0,Configure::read('__body_char_for_pages_index')).'...';
				if($filter!=null && $filterName!=null){
					if($filter=='category') {
						if($post['Post']['category_name']!=$filterName)
							unset($posts[$key]);
					}
					if($filter=='tag' ) {
						if(empty($post['Post']['tags']) || strpos($post['Post']['tags'], $filterName.',')=== false)
							unset($posts[$key]);
					}
				}
			}
			$this->set('posts',$posts);
		}
		
		public function view($id = null) { 
			if (!$id) {
				throw new NotFoundException(__('Invalid post'));
			}
			$post=$this->Post->findById($id);
			if(!$post) {
				throw new NotFoundException(__('Invalid post'));
			}
			
			if($this->Session->check('Comment')) {
				$comment = $this->Session->read('Comment');
				$errors = $this->Session->read('CommentErrors');
				$this->request->data['Comment'] = $comment['Comment'];
				$this->Post->Comment->validationErrors = $errors;
				$this->Session->delete('Comment');
				$this->Session->delete('CommentErrors');
				
				$this->set('website',$this->request->data['Comment']['website']);
			}
			
			$this->set('post',$post);
		}
		
		public function listPosts() {
			
		}
	}
?>