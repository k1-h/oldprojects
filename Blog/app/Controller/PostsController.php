<?php
	class PostsController extends AppController {
		public $paginate = array(
			'fields' => array(
				'Post.id',
				'Post.title',
				'Post.created',
				'Post.modified',
				'Post.user_id',
				'Post.username',
				'Post.status',
				'Post.comment_count',
				'Post.category_name'
			),
			'limit' => 10,
			'order' => array(
				'Post.created' => 'desc'
			),
			'recursive' => 1,
			'contain' => 'Tag'
		);
		
		public function beforeFilter(){
			parent::beforeFilter();
			
			$this->paginate['order']=array(
				'Post.'.Configure::read('__posts_default_sort_by') => Configure::read('__posts_default_sort_order')
			);
			$this->paginate['limit']=Configure::read('__posts_post_limit');
		}
		
		public function index() {
			$this->Post->Behaviors->load('Containable');
			if ($this->request->is('requested')) {
				$tags = $this->Post->Tag->find('all',array('fields' => array('Tag.name','Tag.ferquency')));
				return $tags;
				//return $this->paginate('Post');
			}
			$this->set('posts',$this->paginate('Post'));
		}
		
		public function changeStatus($id = null ,$status = null) {
			if($this->request->is('get')) {
				throw new MethodNotAllowedException();
			}
				
			if ($this->Post->save($this->data)) {
				$this->Session->setFlash('The Post has been saved', 'default', array('alert' => 'success'));
			} else {
				$this->Session->setFlash('The Post could not be saved. Please, try again.', 'default', array('alert' => 'error'));
			}
			$this->redirect(array('controller'=> 'posts', 'action' => 'index'));
		}
		
		public function add() {
			if($this->request->is('post')) {
				$this->Post->create();
				$this->Post->data['Post']['user_id']=$this->Auth->user('id');
				if($this->saveTags($this->request->data) != false && $this->Post->save($this->request->data)) {
					$this->Session->setFlash('Your post has been saved.', 'default', array('alert' => 'success'));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash('Unable to add your post.', 'default', array('alert' => 'error'));
				}
			}
			$select = $this->Post->Category->generateTreeList(null, null, null, "---");
			$this->set('select',$select);
		}
		
		public function edit($id = null) {
			if(!$id) {
				throw new NotFoundException('Invalid post');
			}
			$post = $this->Post->findById($id);
			if(!$post) {
				throw new NotFoundExecption('Invalid post');
			}
			if($this->request->is('post') || $this->request->is('put')) {
				$this->Post->id=$id;

				if($this->saveTags($this->request->data) != false && $this->Post->save($this->request->data)) {
					$this->Session->setFlash('Your post has been updated.', 'default', array('alert' => 'success'));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash('Unable to update your post.', 'default', array('alert' => 'error'));
				}
			}
			
			if(!$this->request->data) {
				$select = $this->Post->Category->generateTreeList(null, null, null, "---");
				$this->set('select',$select);
				$this->request->data=$post;
			}
		}
		
		public function saveTags($data) {
			$tags = explode(',', $data['Post']['tags']);
			// Array for collecting all the data
			$collection = array();
			$collection['Post']=array('id' => $data['Post']['id']);
			foreach($tags as $tag){
				$tag = trim($tag);
				// Check if we already have a tag like this
				$tagId=$this->Post->Tag->find(
					'first',
					 array('conditions' => array(
				 			'Tag.name' => $tag
				 		),
				 		'fields' => array(
			 				'Tag.id',
			 				'Tag.ferquency'
		 				)
			 		)
				);
				$this->log($tagId);
				if(empty($tagId)) {
					$temp = array(
						'Tag' => array(
							'name' => $tag,
							'ferquency' => 1
						)
					);
					$this->Post->Tag->create();
					$this->Post->Tag->save($temp);
					$collection['Tag']['Tag'][] = $this->Post->Tag->id;
				} else {
					$tagId['Tag']['ferquency']++;
					$this->Post->Tag->save($tagId);
					$collection['Tag']['Tag'][] = $this->Post->Tag->id;
				}
			}
			$this->log($collection);
			return $this->Post->saveAll($collection, array('validate' => false));
		}
		
		public function getTags() {
			if ($this->request->is('requested')) {
				$tags = $this->Post->Tag->find('all',array('fields' => array('Tag.name','Tag.ferquency')));
				return $tags;
			}
			die;
		}
		
		public function delete($id) {
			if($this->request->is('get')) {
				throw new MethodNotAllowedException();
			}
			
			if($this->Post->delete($id)) {
				$this->Session->setFlash('The post with id: '.$id.' has been deleted.', 'default', array('alert' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
		}
		
	}
?>