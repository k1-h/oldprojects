<?php
	class CategoriesController extends AppController {
		
		public $paginate = array(
			'fields' => array(
				'Category.id',
				'Category.parent_id',
				'Category.lft',
				'Category.rght',
				'Category.name',
				'Category.created',
				'Category.modified'
			),
			'order' => array(
				'Category.created' => 'desc'
			),
			'recursive' => -1,
		);
		
		public function beforeFilter(){
			parent::beforeFilter();
			$this->Security->unlockedActions=array('edit','delete');
		}
		
		public function index() {
			//$categories = $this->Category->children(null,null,array('id', 'name', 'parent_id'));
			$select = $this->Category->generateTreeList(null, null, null, "---");
			
			$categories = $this->Category->find('threaded', array(
				'order' => array('Category.lft'))
			);
			//debug($cat);
			if ($this->request->is('requested')) {
				return $categories;
			}
			$this->set('categories',$categories);
			//$this->set('categories',json_encode($categories));
			$this->set('select',$select);
		}
		
		public function add() {
			if($this->request->is('post')) {
				if($this->Category->save($this->request->data)) {
					$this->Session->setFlash('Category has been saved.', 'default', array('alert' => 'success'));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash('Unable to add category.', 'default', array('alert' => 'error'));
				}
			}
		}
		
		public function edit($id = null) {
			if(!$id) {
				throw new NotFoundException('Invalid category');
			}
			$category = $this->Category->findById($id);
			if(!$category) {
				throw new NotFoundExecption('Invalid category');
			}
			if($this->request->is('post') || $this->request->is('put')) {
				debug($this->request->data);
				if($this->Category->save($this->request->data)) {
					$this->Session->setFlash('Category has been saved.', 'default', array('alert' => 'success'));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash('Unable to add category.', 'default', array('alert' => 'error'));
				}
			}
		}
		
		public function delete($id) {
			if($this->request->is('get')) {
				throw new MethodNotAllowedException();
			}
			$category = $this->Category->findById($id);
			if(!$category) {
				throw new NotFoundExecption('Invalid category');
			}	
			if($this->Category->delete($id)) {
				$this->Session->setFlash('The Category with id: '.$id.' has been deleted.', 'default', array('alert' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
		}
		
}