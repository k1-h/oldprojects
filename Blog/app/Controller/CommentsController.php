<?php
	class CommentsController extends AppController {
		public $paginate = array(
			'fields' => array(
				'Comment.id',
				'Comment.name',
				'Comment.email',
				'Comment.website',
				'Comment.body',
				'Comment.status',
				'Comment.created',
				'Post.id',
				'Post.title'
			),
			'limit' => 10,
			'order' => array(
				'Comment.created' => 'desc'
			),
			'recursive' => 1
		);
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('add');
			if(!empty($this->data) && !empty($this->data['Comment']['website'])) {
				$this->processWebsiteAddress();
			}
			
			$this->paginate['order']=array(
				'Comment.'.Configure::read('__comments_default_sort_by') => Configure::read('__comments_default_sort_order')
			);
			$this->paginate['limit']=Configure::read('__comments_post_limit');
		}
		
		public function index() {
			$comments=$this->paginate('Comment');
			foreach ($comments as $key => $comment) {
				$comments[$key]['Comment']['body']=substr($comment['Comment']['body'], 0,Configure::read('__body_char_for_comments_index')).'...';
			}
			$this->set('comments',$comments);
		}
		
		public function changeStatus($id = null ,$status = null) {
			if($this->request->is('get')) {
				throw new MethodNotAllowedException();
			}
			
			if ($this->Comment->save($this->data)) {
				$this->Session->setFlash('The Comment has been saved', 'default', array('alert' => 'success'));
			} else {
				$this->Session->setFlash('The Comment could not be saved. Please, try again.', 'default', array('alert' => 'error'));
			}
			$this->redirect(array('controller'=> 'comments', 'action' => 'index'));
		}
		
		public function add($referer = null) {
			if (!empty($this->data)) {
				$this->request->data['Comment']['status']='pending';
				$this->Comment->create();
				$this->Comment->Post->recursive = -1;
				$post = $this->Comment->Post->find('first',array(
						'conditions'=>array('Post.id'=>$this->data['Comment']['post_id'])
				));
		
				if ($this->Comment->save($this->data)) {
					$this->Session->setFlash('The Comment has been saved', 'default', array('alert' => 'success'));
					$this->redirect(array('controller'=> 'pages', 'action' => 'view', $post['Post']['id']));
					echo 'OK.';
				} else {
					$this->Session->setFlash('The Comment could not be saved. Please, try again.', 'default', array('alert' => 'error'));
					$this->Session->write('Comment', $this->data);
					$this->Session->write('CommentErrors', $this->Comment->validationErrors);
					$this->redirect(array('controller'=> 'pages', 'action' => 'view', $post['Post']['id']));
				}
			}
			// get posts
			/*$posts = $this->Comment->Post->find('list');
			$this->set(compact('posts'));*/
		}
		
		public function delete($id) {
			if($this->request->is('get')) {
				throw new MethodNotAllowedException();
			}

			if($this->Comment->delete($id)) {
				$this->Session->setFlash('The comment with id: '.$id.' has been deleted.', 'default', array('alert' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
		}
		
		public function edit($id = null) {
			if(!$id) {
				throw new NotFoundException(__('Invalid comment'));
			}
			$comment = $this->Comment->findById($id);
			if(!$comment) {
				throw new NotFoundExecption(__('Invalid comment'));
			}
			if($this->request->is('post') || $this->request->is('put')) {
				$this->Comment->id=$id;
				if($this->Comment->save($this->request->data)) {
					$this->Session->setFlash('Your comment has been updated.', 'default', array('alert' => 'success'));
					$this->redirect(array('action'=>'index'));
				} else {
					$this->Session->setFlash('Unable to update your comment.', 'default', array('alert' => 'error'));
				}
			}
				
			if(!$this->request->data) {
				$this->request->data=$comment;
			}
		}
			
		function processWebsiteAddress(){
			$website=$this->data['Comment']['website'];
			
			if($website=="http://") {
				$website="";
			} elseif (substr($website, 0,7)!="http://") {
				$website="http://".$website;
			}
			if(substr($website, strlen($website)-1,strlen($website))=='/'){
				$website=substr($website,0, strlen($website)-1);
			}
			
			$this->request->data['Comment']['website']=$website;
		}
	}
?>