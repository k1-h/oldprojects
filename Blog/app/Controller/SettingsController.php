<?php
	class SettingsController extends AppController {
		public function index() {
			if($this->request->is('post')) {
				$flag=1;
				foreach ($this->request->data['Setting'] as $key => $value) {
					if(!$this->Setting->query('UPDATE settings SET value = "'.$value.'" WHERE name = "'.$key.'";')) {
						$flag=0;
					}
				}
				if(!$flag) {
					$this->Session->setFlash(__('The settings has been saved'));
				} else {
					$this->Session->setFlash(__('The settings could not be saved. Please try again.'));
				}
			}
			
			$this->set('settings',$this->Setting->find('setting'));
		}
	}
?>