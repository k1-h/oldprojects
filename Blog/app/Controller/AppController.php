<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');
App::uses('Sanitize', 'Utility');	// For cleaning input data

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'Session',
		'Cookie',
		'Security',
		'RequestHandler',
		'DebugKit.Toolbar',
		'Acl',
		'Auth' => array(
			'loginRedirect' => array('controller' => 'pages','action' => 'index'),
			'logoutRedirect' => array('controller' => 'users','action' => 'login',),
			'authError' => 'Auth Error!',
			'authorize' => array('Actions' => array('actionPath' => 'controllers'),'Controller'),
		)
	);
	
	public $helper=array('Html','Form','Paginator','Text','RSS','Access','Tagcloud');
	
	public function beforeFilter(){
		if(!empty($this->data)) {
			$this->data = Sanitize::clean($this->data);	// Removes malicious code from user data
		}
		$this->Security->blackHoleCallback = 'securityError';	// Function name that will excuted when security problem happens
		$this->fetchSettings();	// Fetches and saves settings in Configure for global access in any action
	}
	
	public function securityError($type) {
		throw new ForbiddenException("Access forbidden for $type");	// Exception for security problem
	}
	
	function fetchSettings(){
		$this->loadModel('Setting');
		$settings_array = $this->Setting->find('all');
		foreach($settings_array as $key=>$value){
			Configure::write("__".$value['Setting']['name'], $value['Setting']['value']);
		}
	}
	
}
