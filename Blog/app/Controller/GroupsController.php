<?php
class GroupsController extends AppController {
	public function index() {
		$this->Group->recursive = 0;
		$this->set('groups', $this->paginate());
	}

	public function view($id = null) {
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid group'));
		}
		$this->set('permissions', $this->getPermissions($id));
		$this->set('group', $this->Group->read(null, $id));
	}

	public function add() {
		if ($this->request->is('post')) {
			$this->Group->create();
			if ($this->Group->save($this->request->data)) {
				$this->setPermissions($this->Group->id, $this->request->data['Group']['Acl']);
				$this->Session->setFlash('The group has been saved', 'default', array('alert' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The group could not be saved. Please, try again.', 'default', array('alert' => 'error'));
			}
		}
	}

	public function edit($id = null) {
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid group'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Group->save($this->request->data)) {
				$this->setPermissions($this->Group->id, $this->request->data['Group']['Acl']);
				$this->Session->setFlash('The group has been saved', 'default', array('alert' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The group could not be saved. Please, try again.', 'default', array('alert' => 'error'));
			}
		} else {
			$this->request->data = $this->Group->read(null, $id);
			$this->request->data['Group']['Acl']=$this->getPermissions($id);
		}
	}

	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid group'));
		}
		if ($this->Group->delete()) {
			$this->Session->setFlash('Group deleted', 'default', array('alert' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Group was not deleted', 'default', array('alert' => 'error'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function getPermissions($groupId) {
		$acl=array();
		$actions=array(
			'Categories' => array(
				'controllers/Categories/index',
				'controllers/Categories/add',
				'controllers/Categories/edit',
				'controllers/Categories/delete',
			),
			'Comments' => array(
				'controllers/Comments/index',
				'controllers/Comments/add',
				'controllers/Comments/edit',
				'controllers/Comments/delete',
				'controllers/Comments/changeStatus',
			),
			'Groups' => array(
				'controllers/Groups/index',
				'controllers/Groups/add',
				'controllers/Groups/edit',
				'controllers/Groups/delete',
				'controllers/Groups/view',
			),
			'Pages' => array(
				'controllers/Pages/index',
				'controllers/Pages/view',
			),
			'Posts' => array(
				'controllers/Posts/index',
				'controllers/Posts/add',
				'controllers/Posts/edit',
				'controllers/Posts/delete',
				'controllers/Posts/changeStatus',
			),
			'Uettings' => array(
				'controllers/Settings/index',
			),
			'Users' => array(
				'controllers/Users/index',
				'controllers/Users/add',
				'controllers/Users/edit',
				'controllers/Users/delete',
				'controllers/Users/view',
			)
		);
		foreach ($actions as $key=>$controller) {
			$acl[$key]=array();
			foreach($controller as $action) {
				if($this->Acl->check('group'.$groupId, $action)) {
					$acl[$key][]=$action;
				}
			}
		}
		return $acl;
	}
	
	public function setPermissions($groupId,$values) {
		$acls = array();
		array_walk_recursive($values, create_function('$val, $key, $obj', 'array_push($obj, $val);'), &$acls);
		$this->Acl->deny('group'.$groupId, 'controllers');
		foreach ($acls as $acl) {
			$this->Acl->allow('group'.$groupId, $acl);
		}
	}
	
	
	public function beforeFilter() {
		parent::beforeFilter();
	}
}
