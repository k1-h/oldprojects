<div class="users view">
	<h2><?php echo $user['User']['username']; ?></h2>
	<table>
		<tr>
			<td>Group</td>
			<td><?php echo $this->Html->link($user['Group']['name'],array('controller' => 'groups','action' => 'view',$user['Group']['id'])); ?></td>
		</tr>
		<tr>
			<td>Post Count</td>
			<td><?php echo $user['User']['post_count']; ?></td>
		</tr>
		<tr>
			<td>Created</td>
			<td><?php echo $this->Time->niceShort($user['User']['created']); ?></td>
		</tr>
		<tr>
			<td>Modified</td>
			<td><?php echo $this->Time->niceShort($user['User']['modified']); ?></td>
		</tr>
	</table>
	<table>
		<tr>
			<th>Title</th>
			<th>Created</th>
			<th>Modified</th>
			<th colspan="2">Actions</th>
		</tr>
		
		<?php foreach($user['Post'] as $post): ?>
			<tr>
				<td><?php echo $this->Html->link($post['title'],array('controller' => 'pages','action' => 'view',$post['id'])); ?></td>
				<td><?php echo $this->Time->niceShort($post['created']); ?></td>
				<td><?php echo $this->Time->niceShort($post['modified']); ?></td>
				<td><?php echo $this->Html->link('Edit',array('controller' => 'posts','action' => 'edit', $post['id']), array('class' => 'btn btn-primary')); ?></td>
				<td>
					<?php
						echo $this->Form->postLink(
							'Delete',
							array('controller' => 'posts','action' => 'delete', $post['id']),
							array('confirm' => 'Are you sure?','class' => 'btn btn-primary')
						);
					?>
				</td>
		<?php endforeach;?>
	</table>
</div>