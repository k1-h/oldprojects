<div class="users login">
	<fieldset>
		<legend>Please enter your username and password</legend>
		<?php 
			echo $this->Form->create('User');
			echo $this->Form->input('username');
			echo $this->Form->input('password'); 
			echo $this->Form->input('remember_me', array('label' => 'Remember Me', 'type' => 'checkbox'));
			echo $this->Form->submit('Login' , array('class' => 'btn btn-primary', 'div' => false));
			echo $this->Html->link('Cancel',array('controller' => 'pages','action' => 'index'), array('class' => 'btn btn-primary'));
			echo $this->Form->end();
		?>
	</fieldset>
</div>