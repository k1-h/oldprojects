<div class="users index">
	<h2>Users</h2>
	<div class="add">
		<?php echo $this->Html->link('Add User',array('controller' => 'users','action' => 'add'), array('class' => 'btn btn-primary')); ?>
	</div>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('Group.name','Group'); ?></th>
			<th><?php echo $this->Paginator->sort('post_count'); ?></th>
			<th><?php  echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th colspan="2">Actions</th>
		</tr>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><strong><?php echo $this->Html->link($user['User']['username'],array('action' => 'view', $user['User']['id'])); ?></strong></td>
				<td><?php echo $user['Group']['name']; ?></td>
				<td><?php echo (!empty($user['User']['post_count'])?$user['User']['post_count']:"0"); ?></td>
				<td><?php echo $this->Time->niceShort($user['User']['created']); ?></td>
				<td><?php echo $this->Time->niceShort($user['User']['modified']); ?></td>
				<td><?php echo $this->Html->link('Edit',array('action' => 'edit',$user['User']['id']), array('class' => 'btn btn-primary')); ?></td>
				<td>
					<?php 
						echo $this->Form->postLink(
							'Delete',
							array('action' => 'delete', $user['User']['id']),
							array('confirm' => 'Are you sure?','class' => 'btn btn-primary')
						); 
					?>
				</td>
			</tr>
		<?php endforeach; ?> 
	</table>
	<?php echo $this->element('pageNav'); ?>
</div>