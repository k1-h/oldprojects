<div class="users edit">
	<fieldset>
		<legend>Edit User</legend>
		<?php 
			echo $this->Form->create('User');
			echo $this->Form->input('username');
			echo $this->Form->input('password');
			echo $this->Form->input('group_id',array('options'=> $groups));
			echo $this->Form->input('id',array('type' => 'hidden'));
			echo $this->Form->submit('Submit' , array('class' => 'btn btn-primary', 'div' => false));
			echo $this->Html->link('Cancel',array('controller' => 'users','action' => 'index'), array('class' => 'btn btn-primary'));
			echo $this->Form->end();
		?>
	</fieldset>
</div>