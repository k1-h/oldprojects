<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout." - ".Configure::read('__blog_name'); ?>
	</title>
	<link rel="alternate" type="application/rss+xml" title="<?php echo $title_for_layout." - ".Configure::read('__blog_name'); ?> RSS Feed" href="<?php echo "pages/index.rss"; ?>" />
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');
		echo $this->Html->css('bootstrap.css');
		echo $this->Html->css('paginatin.css');
		echo $this->Html->css('style.css');
		
		echo $this->Html->script('jquery-latest.js');
		echo $this->Html->script('bootstrap.min.js');
		echo $this->Html->script('script.js');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">
		<?php echo $this->element('header'); ?>
		<?php echo $this->element('mainNav'); ?>
		<div id="content">
			<?php 
				echo $this->Session->flash('flash', array('element' => 'flash'));
				echo $this->Session->flash('auth', array('params' => array('alert' => 'error'),'element' => 'flash')); 
			?>
			<?php echo $this->fetch('content'); ?>
		</div>
		<?php //if ($this->name=='Pages') 
			echo $this->element('sidebar'); 
		?>
	</div>
	<div id="footer">
		<div class="footer_wrapper">
			<div class="footer_left">
				&copy;&nbsp;<?php echo Configure::read('__blog_name'); ?>.&nbsp;<?php echo 'Powered by <a href="http://wordpress.org/" title="CODE IS POETRY">WordPress</a>&nbsp;and&nbsp;<a href="http://imotta.cn/" title="Pyrmont V2 theme">Pyrmont V2</a>.'; ?>
			</div>	
		</div>
	</div>
	
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
