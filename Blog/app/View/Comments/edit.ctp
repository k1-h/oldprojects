<div class="comments edit">
	<fieldset>
		<legend>Edit Comment</legend>
		<?php
			echo $this->Form->create('Comment',array('class' => 'form'));
			echo $this->Form->input('name');
			echo $this->Form->input('email');
			echo $this->Form->input('website',array('value' => empty($website)?"http://":$website));
			echo $this->Form->input('body',array('rows' => '3'));
			echo '<div class="statusRadio">';
			$options = array('pending' => 'Pending', 'publish' => 'Publish');
			$attributes = array('legend' => false, 'value' => 'pending');
			echo $this->Form->radio('status',$options,$attributes);
			echo '</div>';
			echo $this->Form->input('id',array('type' => 'hidden'));
			echo $this->Form->submit('Save Comment' , array('class' => 'btn btn-primary', 'div' => false));
			echo $this->Html->link('Cancel',array('controller' => 'comments','action' => 'index'), array('class' => 'btn btn-primary'));
			echo $this->Form->end();
		?>
	</fieldset>
</div>