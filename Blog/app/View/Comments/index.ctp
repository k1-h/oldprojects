<div class="comments index">
	<h2>Comments</h2>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('body'); ?></th>
			<th><?php echo $this->Paginator->sort('Post.title','Post Title'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th colspan="2">Actions</th>
		</tr>
		
		<?php foreach($comments as $comment): ?>
			<tr class="<?php echo $comment['Comment']['status']=='pending'?'pending':'publish'; ?>">
				<td>
					<span class="name"><?php echo $comment['Comment']['name']; ?></span><br />
					<span class="email"><?php echo $comment['Comment']['email']; ?></span><br />
					<span class="website"><?php echo $comment['Comment']['website']; ?></span><br />
				</td>
				<td>
					<span>
						<small>
							Sent at: <em><?php echo $this->Time->niceShort($comment['Comment']['created']); ?></em>
						</small>
					</span>
					<p>
						<?php echo $comment['Comment']['body']; ?>
					</p>
				</td>
				<td><?php echo $this->Html->link($comment['Post']['title'],array('controller' => 'pages','action' => 'view',$comment['Post']['id'],'#' => 'comment'.$comment['Comment']['id'])); ?></td>
				<td><?php
						echo $this->Form->create('Comment',array('action' => 'changeStatus','inputDefaults' => array('label' => false),'class' => 'noMargin'));
						echo $this->Form->input('id',array('type' => 'hidden','value' => $comment['Comment']['id']));
						echo $this->Form->input('status',array('type' => 'hidden','value' => $comment['Comment']['status']=="publish"?"pending":"publish"));
						echo $this->Form->button(
							'Publish',
							array(
								'class' => "statusSelect btn btn-inverse " . (($comment['Comment']['status']=="publish")?"active":""),
								'data-toggle' => "button"
							)
						);
						echo $this->Form->end();
					?>
				</td>
				<?php if (AuthComponent::user('username')!=null) : ?>
					<td><?php echo $this->Html->link('Edit',array('controller' => 'comments','action' => 'edit', $comment['Comment']['id']), array('class' => 'btn btn-primary')); ?></td>
					<td>
						<?php
							echo $this->Form->postLink(
								'Delete',
								array('controller' => 'comments', 'action' => 'delete', $comment['Comment']['id']),
								array('confirm' => 'Are you sure?', 'class' => 'btn btn-primary')
							);
						?>
					</td>
				<?php endif; ?>
			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->element('pageNav'); ?>
</div>