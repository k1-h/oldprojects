<div class="settings index">
	<?php
		echo $this->Form->create('Setting');
		echo $this->Form->input('blog_name',array('value' => $settings['blog_name']));
		echo $this->Form->input(
			'pages_default_sort_by',
			array(
				'options' => array(
					'title' => 'Title',
					'created' => 'Created',
					'modified' => 'Modified',
					'username' => 'Username',
					'modified' => 'Modified',
					'status' => 'Status',
					'Comment_count' => 'Comment Count'
				),
				'value' => $settings['pages_default_sort_by']
			)
		);
		echo $this->Form->input(
			'pages_default_sort_order',
			array(
				'options' => array(
					'desc' => 'Descending',
					'asc' => 'Ascending'
				),
				'value' => $settings['pages_default_sort_order']
			)
		);
		echo $this->Form->input('pages_post_limit',array('value' => $settings['pages_post_limit']));
		echo $this->Form->input('body_char_for_pages_index',array('value' => $settings['body_char_for_pages_index']));
		
		echo $this->Form->input(
			'posts_default_sort_by',
			array(
				'options' => array(
					'title' => 'Title',
					'created' => 'Created',
					'modified' => 'Modified',
					'username' => 'Username',
					'modified' => 'Modified',
					'status' => 'Status',
					'Comment_count' => 'Comment Count'
				),
				'value' => $settings['posts_default_sort_by']
			)
		);
		echo $this->Form->input(
			'posts_default_sort_order',
			array(
				'options' => array(
					'desc' => 'Descending',
					'asc' => 'Ascending'
				),
				'value' => $settings['posts_default_sort_order']
			)
		);
		echo $this->Form->input('posts_post_limit',array('value' => $settings['posts_post_limit']));
		
		echo $this->Form->input(
			'comments_default_sort_by',
			array(
				'options' => array(
					'title' => 'Title',
					'created' => 'Created',
					'modified' => 'Modified',
					'username' => 'Username',
					'modified' => 'Modified',
					'status' => 'Status',
					'Comment_count' => 'Comment Count'
				),
				'value' => $settings['comments_default_sort_by']
			)
		);
		echo $this->Form->input(
			'comments_default_sort_order',
			array(
				'options' => array(
					'desc' => 'Descending',
					'asc' => 'Ascending'
				),
				'value' => $settings['comments_default_sort_order']
			)
		);
		echo $this->Form->input('comments_post_limit',array('value' => $settings['comments_post_limit']));
		echo $this->Form->input('body_char_for_comments_index',array('value' => $settings['body_char_for_comments_index']));
		
		echo $this->Form->input(
			'users_default_sort_by',
			array(
				'options' => array(
					'title' => 'Title',
					'created' => 'Created',
					'modified' => 'Modified',
					'username' => 'Username',
					'modified' => 'Modified',
					'status' => 'Status',
					'Comment_count' => 'Comment Count'
				),
				'value' => $settings['users_default_sort_by']
			)
		);
		echo $this->Form->input(
			'users_default_sort_order',
			array(
				'options' => array(
					'desc' => 'Descending',
					'asc' => 'Ascending'
				),
				'value' => $settings['users_default_sort_order']
			)
		);
		echo $this->Form->input('users_post_limit',array('value' => $settings['users_post_limit']));
		
		echo $this->Form->end('Save Settings');
	?>
</div>