<div class="groups index">
	<h2>Groups</h2>
	<div class="add">
		<?php echo $this->Html->link('Add Group',array('controller' => 'groups','action' => 'add'), array('class' => 'btn btn-primary')); ?>
	</div>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('user_count'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th colspan="2">Actions</th>
		</tr>
		<?php foreach ($groups as $group): ?>
			<tr>
				<td><strong><?php echo $this->Html->link($group['Group']['name'], array('action' => 'view', $group['Group']['id'])); ?></strong></td>
				<td><?php echo empty($group['Group']['user_count'])?"0":$group['Group']['user_count']; ?>&nbsp;</td>
				<td><?php echo $this->Time->niceShort($group['Group']['created']); ?>&nbsp;</td>
				<td><?php echo $this->Time->niceShort($group['Group']['modified']); ?>&nbsp;</td>
				<td><?php echo $this->Html->link('Edit', array('action' => 'edit', $group['Group']['id']), array('class' => 'btn btn-primary')); ?></td>
				<td>
					<?php 
						echo $this->Form->postLink(
							'Delete',
							array('action' => 'delete', $group['Group']['id']),
							array('confirm' => 'Are you sure?','class' => 'btn btn-primary')
						); 
					?>
				 </td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>