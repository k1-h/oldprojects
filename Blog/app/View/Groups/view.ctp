<style>
	table {
		width: 550px
	}
	.col
	{
		-moz-column-count: 2; /* Firefox */
		-webkit-column-count: 2; /* Safari and Chrome */
		column-count: 2;
	}
</style>
<div class="groups view">
	<h2><?php echo $group['Group']['name']; ?></h2>
	<table>
		<tr>
			<td>User Count</td>
			<td><?php echo $group['Group']['user_count']; ?></td>
		</tr>
		<tr>
			<td>Created</td>
			<td><?php echo $this->Time->niceShort($group['Group']['created']); ?></td>
		</tr>
		<tr>
			<td>Modified</td>
			<td><?php echo $this->Time->niceShort($group['Group']['modified']); ?></td>
		</tr>
		<tr>
			<td>Permissions</td>
			<td class="col"><?php array_walk_recursive($permissions, create_function('$val, $key', 'echo substr($val,12,strlen($val))."<br />";')); ?></td>
		</tr>
	</table>
	<table>
		<tr>
			<th>Username</th>
			<th>Post Count</th>
			<th>Created</th>
			<th>Modified</th>
			<th colspan="2">Actions</th>
		</tr>
		
		<?php foreach($group['User'] as $user): ?>
			<tr>
				<td><?php echo $this->Html->link($user['username'],array('controller' => 'users','action' => 'view',$user['id'])); ?></td>
				<td><?php echo $user['post_count']; ?></td>
				<td><?php echo $this->Time->niceShort($user['created']); ?></td>
				<td><?php echo $this->Time->niceShort($user['modified']); ?></td>
				<td><?php echo $this->Html->link('Edit',array('controller' => 'users','action' => 'edit', $user['id']), array('class' => 'btn btn-primary')); ?></td>
				<td>
					<?php
						echo $this->Form->postLink(
							'Delete',
							array('controller' => 'users','action' => 'delete', $user['id']),
							array('confirm' => 'Are you sure?','class' => 'btn btn-primary')
						);
					?>
				</td>
		<?php endforeach;?>
	</table>
</div>