<style>
	#checks
	{
		-moz-column-count: 3; /* Firefox */
		-webkit-column-count: 3; /* Safari and Chrome */
		column-count: 3;
	}
	.formFieldset { 
		display: inline-block;
		-webkit-margin-start: 2px;
		-webkit-margin-end: 2px;
		-webkit-padding-before: 0.35em;
		-webkit-padding-start: 0.75em;
		-webkit-padding-end: 0.75em;
		-webkit-padding-after: 0.625em;
		border: 2px groove threedface;
		border-image: initial;
		width: 180px;
	}
	
	.formFieldset legend {
		display: block;
		-webkit-padding-start: 2px;
		-webkit-padding-end: 2px;
		border: none;
		border-image: initial;
		width: auto;
		margin: 0px;
		cursor: pointer;
	}
</style>
<script>
	$(document).ready(function(){
		$(".formFieldset legend").click(function(){
			$(this).data('checked' , !$(this).data('checked'));
			var checked=$(this).data('checked');
			$(this).parent().find('input[type="checkbox"]').each(function(){
				$(this).attr('checked', checked);
			});
		});
		$('#inverse').click(function(event) {
			event.preventDefault();
			var checked=!$(this).data('checked');
			$(this).data('checked',checked);
			var list=$(this).closest('form').find("legend");
			
			list.each(function(){
				$(this).trigger('click');
			});
		});
	});
</script>
<div class="groups add">
	<fieldset>
		<legend>Add Group</legend>
		<?php
			$actions=array(
				'Categories' => array(
					'controllers/Categories/index' => 'Index',
					'controllers/Categories/add' => 'Add',
					'controllers/Categories/edit' => 'Edit',
					'controllers/Categories/delete' => 'Delete',
				),
				'Comments' => array(
					'controllers/Comments/index' => 'Index',
					'controllers/Comments/add' => ' Add',
					'controllers/Comments/edit' => ' Edit',
					'controllers/Comments/delete' => 'Delete',
					'controllers/Comments/changeStatus' => 'Change Status',
				),
				'Groups' => array(
					'controllers/Groups/index' => 'Index',
					'controllers/Groups/add' => 'Add',
					'controllers/Groups/edit' => 'Edit',
					'controllers/Groups/delete' => 'Delete',
					'controllers/Groups/view' => 'View',
				),
				'Pages' => array(
					'controllers/Pages/index' => 'Index',
					'controllers/Pages/view' => 'View',
				),
				'Posts' => array(
					'controllers/Posts/index' => 'Index',
					'controllers/Posts/add' => 'Add',
					'controllers/Posts/edit' => 'Edit',
					'controllers/Posts/delete' => ' Delete',
					'controllers/Posts/changeStatus' => 'Change Status',
				),
				'Uettings' => array(
					'controllers/Settings/index' => 'Index',
				),
				'Users' => array(
					'controllers/Users/index' => 'Index',
					'controllers/Users/add' => 'Add',
					'controllers/Users/edit' => 'Edit',
					'controllers/Users/delete' => 'Delete',
					'controllers/Users/view' => 'View',
				)
			);
			echo $this->Form->create('Group');
			echo $this->Form->input('id');
			echo $this->Form->input('name');
			
			echo '<div id="checks">';
			foreach($actions as $key=>$action) {
				echo '<fieldset class="formFieldset"><legend data-checked="false">'.$key.'</legend>';
				echo $this->Form->select('Group.Acl.'.$key, $action, array('multiple' => 'checkbox'));
				echo '</fieldset>';
			}
			echo '</div>';
			
			echo $this->Form->button('Select Inverse' , array('class' => 'btn btn-primary', 'id' => 'inverse' ,'data-checked' => 'false', 'div' => false));
			echo $this->Form->submit('Submit' , array('class' => 'btn btn-primary', 'div' => false));
			echo $this->Html->link('Cancel',array('controller' => 'groups','action' => 'index'), array('class' => 'btn btn-primary'));
			echo $this->Form->end();
		?>
	</fieldset>
</div>