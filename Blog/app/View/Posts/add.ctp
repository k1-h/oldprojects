<div class="posts add">
	<fieldset>
		<legend>Add Post</legend>
		<?php
			echo $this->Form->create('Post',array('class' => 'form'));
			echo $this->Form->input('title');
			echo $this->Form->input('body',array('rows' => '10'));
			echo '<div class="statusRadio">';
			$options = array('pending' => 'Pending', 'publish' => 'Publish');
			$attributes = array('legend' => false, 'value' => 'pending');
			echo $this->Form->radio('status',$options,$attributes);
			echo '</div>';
			echo $this->Form->input('category_id',array(
					'options' => $select
			));
			echo $this->Form->input('tags');
			echo $this->Form->submit('Save Post' , array('class' => 'btn btn-primary', 'div' => false));
		    echo $this->Html->link('Cancel',array('controller' => 'posts','action' => 'index'), array('class' => 'btn btn-primary')); 
			echo $this->Form->end();
		?>
	</fieldset>
</div>