<div class="posts index">
	<h2>Posts</h2>
	<div class="add">
		<?php echo $this->Html->link('Add Post',array('controller' => 'posts','action' => 'add'), array('class' => 'btn btn-primary')); ?>
	</div>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('comment_count','Comments'); ?></th>
			<th colspan="2">Actions</th>
		</tr>
		
		<?php foreach($posts as $post): ?>
			<tr class="<?php echo $post['Post']['status']=='pending'?'pending':'publish'; ?>">
				<td>
					<p class="postTitle">
						<?php echo $this->Html->link($post['Post']['title'],array('controller' => 'pages','action' => 'view',$post['Post']['id'])); ?>
					</p>
					<small>Posted By: <?php echo empty($post['Post']['username'])?"Unknown":$this->Html->link($post['Post']['username'],array('controller' => 'users','action' => 'view', $post['Post']['user_id'])); ?></small>
					<small>Category: <?php echo empty($post['Post']['category_name'])?"Uncategorized":$this->Html->link($post['Post']['category_name'],array('controller' => 'pages','action' => 'index', 'category', $post['Post']['category_name'])); ?></small>
					<small>At : <?php echo $this->Time->niceShort($post['Post']['created']); ?></small>
					<small>Modified At : <?php echo (!empty($post['Post']['modified'])?$this->Time->niceShort($post['Post']['modified']):"Not Modified"); ?></small>
				</td>
				<td>
					<?php
						echo $this->Form->create('Post',array('action' => 'changeStatus','inputDefaults' => array('label' => false)));
						echo $this->Form->input('id',array('type' => 'hidden','value' => $post['Post']['id']));
						echo $this->Form->input('status',array('type' => 'hidden','value' => $post['Post']['status']=="publish"?"pending":"publish"));
						echo $this->Form->button(
								'Publish',
								array(
										'class' => "statusSelect btn btn-inverse " . (($post['Post']['status']=="publish")?"active":""),
										'data-toggle' => "button"
								)
						);
						echo $this->Form->end();
					?>
				</td>
				<td class="comment"><?php echo (!empty($post['Post']['comment_count'])?$post['Post']['comment_count']:"0"); ?></td>
				<td><?php echo $this->Html->link('Edit',array('controller' => 'posts','action' => 'edit', $post['Post']['id']), array('class' => 'btn btn-primary')); ?></td>
				<td>
					<?php
						echo $this->Form->postLink(
							'Delete',
							array('controller' => 'posts', 'action' => 'delete', $post['Post']['id']),
							array('confirm' => 'Are you sure?', 'class' => 'btn btn-primary')
						);
					?>
				</td>
			</tr>
		<?php endforeach;?>
	</table>
	<?php echo $this->element('pageNav'); ?>
</div>