<div id="header">
	<div class="blog_title">
		<h1><a href="<?php echo FULL_BASE_URL; ?>"><?php echo Configure::read('__blog_name'); ?></a></h1>
		<p class="description"><?php echo Configure::read('__blog_desc'); ?></p>
	</div>
	<div id="search">
		<?php 
			echo $this->Form->create('Post',array('url' => array('controller' => 'pages', 'action' => 'search'),'id' => 'searchform','class' => 'form-search','method' => 'get'));
			echo '<div class="input-append">';
			echo $this->Form->input('name',array('label' => false,'id' => 'searchinput','class' => 'searchinput span2 search-query','placeholder' => 'search','div' => false));
			echo $this->Form->button('Search',array('class' => 'btn btn-inverse', 'type' => 'submit'));
			echo '</div>';
			echo $this->Form->end();
		?>
	</div>
	<div class="clear"></div>
</div>