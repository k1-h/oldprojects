<?php
	$categories = $this->requestAction('categories/index');
	function RecursiveCategories($array) {
		//http://bakery.cakephp.org/articles/blackbit/2012/12/20/display_tree_index_with_ol_and_li
		if (count($array)) {
			echo "\n<ul>\n";
			foreach ($array as $vals) {
				echo '<li class="tree">';
				echo '<a href="'.Configure::read('__blog_address').'pages/index/category/'.$vals['Category']['name'].'">'.$vals['Category']['name'].'</a>';
				echo '</li>';
				if (count($vals['children'])) {
					RecursiveCategories($vals['children']);
				}
				echo "</li>\n";
			}
			echo "</ul>\n";
		}
	}
	RecursiveCategories($categories);