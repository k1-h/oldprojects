<div id="main_navi">
	<ul class="left">
		<li class="active"><?php echo $this->Html->link('Home',array('controller' => 'pages','action' => 'index')); ?></li>
		<?php if (AuthComponent::user('username')!=null) : ?>
			<li><?php echo $this->Html->link('Posts',array('controller' => 'posts','action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Categories',array('controller' => 'categories','action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Users',array('controller' => 'users','action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Groups',array('controller' => 'groups','action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Comments',array('controller' => 'comments','action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Settings',array('controller' => 'settings','action' => 'index')); ?></li>
		<?php endif; ?>
		<li><?php echo $this->Html->link('RSS',array('controller' => 'pages','action' => 'index.rss')); ?></li>
	</ul>
	
	<ul class="right">	
		<?php if (AuthComponent::user('username')!=null) : ?>
			<li><span>Welcome</span> <?php echo $this->Html->link(AuthComponent::user('username'),array('controller' => 'users','action' => 'view',AuthComponent::user('id'))); ?></li>
			<li><?php echo $this->Html->link('Logout',array('controller' => 'users','action' => 'logout')); ?></li>
		<?php else: ?>
			<li>
				<?php echo $this->Html->link('Login',array('controller' => 'users','action' => 'login')); ?>
				/
				<?php echo $this->Html->link('Register',array('controller' => 'users','action' => 'add')); ?>
			</li>
			<li><span>Welcome Guest</span></li>
		<?php endif; ?>
	</ul>
</div>
