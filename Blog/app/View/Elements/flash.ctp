<div id="flash" class="alert alert-<?php echo empty($alert)?"info":$alert; ?>">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<?php echo $message; ?>
</div>