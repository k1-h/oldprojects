<?php /* <div class="pagination">
	<ul>
		<li>
	    	<?php echo $this->Paginator->prev('<- Older', array(), null, array('class' => 'left disabled')); ?>	
		</li>
		<?php
			echo $this->Paginator->numbers(array('tag' => 'li','separator' => '','currentTag' => 'a'));
		?>
		<li>
			<?php echo $this->Paginator->next('Newer ->', array(), null, array('class' => 'right disabled')); ?>
		</li>
	</ul>
</div> */ ?>
<nav class="pagination pagination-blue">
	<?php echo $this->Paginator->prev('<', array('tag' => 'a'), null, array('class' => 'disabled')); ?>	
	<?php echo $this->Paginator->numbers(array('tag' => false,'currentClass' => false,'class' => false, 'separator' => false,'currentTag' => 'span')); ?>
	<?php echo $this->Paginator->next('>', array('tag' => false), null, array('class' => 'disabled')); ?>
</nav>