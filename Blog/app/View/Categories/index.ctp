<style>
	#tree {
		float: left;
	}
	#tree, #tree ul {
		list-style-type: none;
		color: #ccc;
	}
	#tree ul {
		list-style-type: none;
		margin: 0 10px 0 25px;
	}
	span.cat {
		cursor: pointer;
	}
	span.cat.selected {
		background: blue;
	}
</style>
<div class="categories index">
	<h2>Categories</h2>
	<script>
	$(document).ready(function(){
		/*var data=<?php // echo $categories; ?>;
		for (var key in data) {
		 	var name=data[key]["Category"]["name"];
		 	var id=data[key]["Category"]["id"];
		 	var parent=data[key]["Category"]["parent_id"];
			$('#tree').append('<li class="tree" name="'+name+'" id="'+id+'" data-parent="'+parent+'"><span class="cat">'+name+'</span><ul></ul></li>');
		}
	 	$("li.tree").each(function() {
	 		var name=$(this).attr('name');
	 		var id=$(this).attr('id');
	 		var parent=$(this).data('parent');
	 		var parentLi=$("li[id='"+id+"']").children("ul");
	 		$("li.tree").each(function() {
	 	 		if($(this).data('parent')==id) {
	 	 			parentLi.append($(this));
	 	 		}
	 		});
		});*/
	 	$("span.cat").click(function() {
	 		$("span.cat").each(function() {
	 			$(this).removeClass('selected');
	 	 	});
	 	 	$(this).toggleClass('selected');
	 	 	$("#CategoryId").val($(this).parent().attr('id'));
	 	 	$("#CategoryName").val($(this).parent().attr('name'));
	 	 	$("#CategoryParentId").val($(this).parent().data('parent'));
	 	});
	 	$('input[value="Add"]').click(function() {
	 		$(this).parents('form').attr('action','categories/add');
	 		$("#CategoryId").val('');
	 	 	//event.preventDefault();
	 	});
	 	$('input[value="Delete"]').click(function() {
	 		$(this).parents('form').attr('action','categories/delete/' + $("#CategoryId").val());
	 		if(!confirm('Are you sure?')) {
	 			event.preventDefault();	
	 		}
	 	});
	 	$('input[value="Edit"]').click(function() {
	 		$(this).parents('form').attr('action','categories/edit/' + $("#CategoryId").val());
	 	});
	});
	</script>
	<div id="tree">
	<?php
		function RecursiveCategories($array) { 
			//http://bakery.cakephp.org/articles/blackbit/2012/12/20/display_tree_index_with_ol_and_li
			if (count($array)) { 
				echo "\n<ul>\n"; 
				foreach ($array as $vals) { 
					echo '<li class="tree" name="'.$vals['Category']['name'].'" id='.$vals['Category']['id'].' data-parent="'.$vals['Category']['parent_id'].'"><span class="cat">'.$vals['Category']['name'].'</span>'; 
					if (count($vals['children'])) { 
						RecursiveCategories($vals['children']); 
					}
					echo "</li>\n"; 
				} 
				echo "</ul>\n"; 
			} 
		}
		RecursiveCategories($categories);
	?>
	</div>
	<div id="form">
		<?php
			echo $this->Form->create('Category',array('class' => 'form'));
			echo $this->Form->input('id');
			echo $this->Form->input('name');
			echo $this->Form->input('parent_id',array(
					'options' => $select
				));
			echo $this->Form->submit('Add' , array('class' => 'btn btn-primary', 'div' => false));
			echo $this->Form->submit('Delete' , array('class' => 'btn btn-primary', 'div' => false));
			echo $this->Form->submit('Edit' , array('class' => 'btn btn-primary', 'div' => false));
			echo $this->Form->end();
		?>
	</div>
</div>