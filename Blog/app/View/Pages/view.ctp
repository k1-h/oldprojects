<div class="pages view">
	<div class="post" id="post-<?php echo $post['Post']['id']; ?>">
		<div class="date">
			<?php echo $this->Time->niceShort($post['Post']['created']); ?>
		</div>
		<div class="title">
			<h2><?php echo $this->Html->link($post['Post']['title'],array('controller' => 'pages','action' => 'view', $post['Post']['id'])); ?></h2>
			
			<div class="postmeta">
				<span class="category">Category: Uncategorized</span>
				<span class="tags">Tags: no tag</span>
				<span class="comments"><?php echo $this->Html->link(((!empty($post['Post']['comment_count'])?$post['Post']['comment_count']:"0").' Comments') , array('controller' => 'pages','action' => 'view',$post['Post']['id'],'#' => 'comments')); ?> </span>
				<span class="user">Posted by: <?php echo $this->Html->link($post['Post']['username'],array('controller' => 'users','action' => 'view', $post['Post']['user_id'])); ?></span>
			</div>
		</div>
		<div class="entry">
			<p><?php echo h($post['Post']['body']); ?></p>
		</div>
	</div>
	<div id="comments">
			<h3><?php echo (!empty($post['Post']['comment_count'])?$post['Post']['comment_count']:"0").' Comments'; ?></h3>
			<span class="add_your_comment"><a href="#respond">Add Your Comment</a></span>
			<div class="clear"></div>
	</div>
	<ol class="commentlist">
		<?php foreach ($post['Comment'] as $comment): ?>
			<?php if (AuthComponent::user('username')!=null || $comment['status']=='publish') : ?>
				<li class="comment odd alt thread-odd thread-alt depth-1 <?php echo $comment['status']=='pending'?'pending':'publish'; ?>">
					<div id="comment<?php echo $comment['id'] ?>">
						<div class="right">
							<?php if ($comment['status']=='pending') : ?>
					        	<em><?php echo 'Waiting for the admin to approve your comment. Please be patient.'; ?></em>
					         	<br />
					        <?php endif; ?>
							
							<div class="comment-meta commentmetadata">
								<?php 
									if(empty($comment['website']))
				 						echo $comment['name']; 
									else
										echo $this->Html->link($comment['name'],$comment['website']); 
								?>
								<?php echo '&nbsp;said:&nbsp;';?>
								<?php echo $this->Time->niceShort($comment['created']); ?>
							</div> 
						
							<p><?php echo $comment['body']; ?></p>
						</div>
						<div class="clear"></div>
						
					</div>
					
				</li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ol>
	<div id="respond">
		<div class="h3_cancel_reply">
			<h3><?php echo 'Your Comment'; ?></h3>
			<div class="clear"></div>
		</div>
		<?php
			echo $this->Form->create('Comment',array('url' => array('controller' => 'comments', 'action' => 'add'),'id' => 'commentform'));
			echo $this->Form->input('body',array('rows' => '4','div' => 'input_area','id' => 'comment','class' => 'message_input','label' => false));
			echo '<div class="user_info">';
			echo $this->Form->input('name',array('div' => 'single_field', 'class' => 'comment_input'));
			echo $this->Form->input('email',array('div' => 'single_field', 'class' => 'comment_input'));
			echo $this->Form->input('website',array('value' => empty($website)?"http://":$website,'div' => 'single_field', 'class' => 'comment_input'));
			echo '</div>';
			echo $this->Form->hidden('post_id',array('value' => $post['Post']['id']));
			echo '<div class="submit_button">';
			echo $this->Form->button('Submit',array('class' => 'button', 'type' => 'submit','id' => 'submit','div'=>false,'label' => false));
			echo '<div class="clear"></div>';
			echo '</div>';
			echo $this->Form->end();
		?>
	</div>
</div>