<div class="pages index">
	<?php foreach ($posts as $post): ?>
		<?php if (AuthComponent::user('username')!=null || $post['Post']['status']=='publish') : ?>
			<div class="post" id="post-<?php echo $post['Post']['id']; ?>">
				<div class="date">
					<?php echo $this->Time->niceShort($post['Post']['created']); ?>
				</div>
				<div class="title">
					<h2><?php echo $this->Html->link($post['Post']['title'],array('controller' => 'pages','action' => 'view', $post['Post']['id'])); ?></h2>
					
					<div class="postmeta">
						<span class="category">
							Category: 
							<?php
								if(!empty($post['Post']['category_name'])) {
									echo $this->Html->link($post['Post']['category_name'],array('controller' => 'pages','action' => 'index', 'category', $post['Post']['category_name']));
								} else {
									echo 'Uncategorized';
								}
							?>
						</span>
						<span class="tags">
							Tags: 
							<?php
								if(!empty($post['Tag'])) {
									foreach($post['Tag'] as $tag)
										echo $this->Html->link($tag['name'],array('controller' => 'pages','action' => 'index', 'tag', $tag['name'])).', ';
								} else {
									echo 'no tag';
								}
							?>
						</span>
						<span class="comments"><?php echo $this->Html->link(((!empty($post['Post']['comment_count'])?$post['Post']['comment_count']:"0").' Comments') , array('controller' => 'pages','action' => 'view',$post['Post']['id'],'#' => 'comments')); ?> </span>
						<span class="user">Posted by: <?php echo empty($post['Post']['username'])?"Unknown":$this->Html->link($post['Post']['username'],array('controller' => 'users','action' => 'view', $post['Post']['user_id'])); ?></span>
					</div>
				</div>
				<div class="entry <?php echo $post['Post']['status']=='pending'?'pending':'publish'; ?>">
					<?php echo $post['Post']['body']; ?>
					<p><?php echo $this->Html->link("Continue Reading...",array('controller' => 'pages','action' => 'view', $post['Post']['id'])); ?></p>
				</div>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
	<?php echo $this->element('pageNav'); ?>
</div>