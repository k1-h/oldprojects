<?php
	class Post extends AppModel {
		public $belongsTo =array(
			'User' => array(
				'className' => 'Users',
				'foreignKey' => 'id',
				'fields' => array(
					'id',
					'username'
				)
			),
			'Category' => array(
				'className' => 'Category',
				'foreignKey' => 'id',
				'fields' => array(
					'id',
					'name',
					'parent_id'
				)
			)
		);
		
		public $hasMany = array(
			'Comment' => array(
				'className' => 'Comment',
				'foreignKey' => 'post_id'
			),
		);
		
		var $hasAndBelongsToMany = array(
			'Tag' => array(
				'className'    => 'Tag', 
                'joinTable'    => 'posts_tags', 
                'foreignKey'   => 'post_id', 
                'associationForeignKey'=> 'tag_id',
				'unique'	=> 'keepExisting'
			)
		);
		
		public $virtualFields = array(
				'username' => 'SELECT u.username FROM users AS u, posts AS p WHERE u.id = p.user_id AND Post.id = p.id',
				'category_name' => 'SELECT c.name FROM categories AS c, posts AS p WHERE c.id = p.category_id AND Post.id = p.id',
				'comment_count' => 'SELECT count(*) FROM comments AS c, posts AS p WHERE p.id = c.post_id AND Post.id = c.post_id GROUP BY c.post_id'
		);
		
		public $validate = array(
			'title' => array(
				'rule' => 'notEmpty'
			),
			'body' => array (
				'rule' => 'notEmpty'
			)
		);
		
		public function afterFind($results, $primary = false) {
			foreach ($results as $key => $val) {
				if(!empty($val['Tag'])) {
					$tags=null;
					foreach ($val['Tag'] as $tag) {
						$tags.=$tag['name'].', ';
					}
					$results[$key]['Post']['tags']=$tags;
				}
			}
			return $results;
		}
		
		public function isOwnedBy($post,$user) { 
			return $this->field('id', array('id'=> $post,'username' => $user)) === $post;
		}
		
		public function getPostsAndOwners($by = 'created',$order = 'DESC') {
			$this->recursive = -1;
			$posts=$this->find(
				'all',
					array(
						'order' => array(
							'Post.'.$by => $order
					)
				)
			);
			foreach ($posts as $key => $post) {
				$posts[$key]['Post']['body']=substr($post['Post']['body'], 0,140);
			}
			return $posts;
		}
	}
?>