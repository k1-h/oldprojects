<?php
	class Comment extends AppModel {
		var $belongsTo = array(
			'Post' => array(
				'className'=>'Post',
				'foreignKey' => 'post_id',
				'fields' => array(
					'id',
					'title'
				)
			)
		);
		
		public $validate = array(
			'name' => array (
				'rule' => 'notEmpty',
				'message'  => 'Can\'t be empty!'
			),
			'email' => array (
				'rule' => array('email', true),
				'message'  => 'Must be a valid email address!'
			),
			'website' => array (
				'rule' => array('url', true),
				'allowEmpty' => true,
				'message'  => 'Must be a valid URL address!'
			),
			'body' => array (
				'rule' => 'notEmpty',
				'message'  => 'Can\'t be empty!'
			)
		);
		
	}
?>