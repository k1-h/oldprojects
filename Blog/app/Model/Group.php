<?php
class Group extends AppModel {
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			),
		),
	);

	public $hasMany = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'group_id',
		)
	);
	
	public $virtualFields = array(
		'user_count' => 'SELECT count(*) FROM users AS u, groups AS g WHERE u.group_id = g.id AND Group.id = g.id GROUP BY g.id'
	);

	public function afterSave($created) {
		$this->Aro->save(array('alias'=>'group'.$this->data[$this->alias]['id']));
	}
	
	public $actsAs = array('Acl' => array('type' => 'both'));

    public function parentNode() {
        return null;
    }
}
