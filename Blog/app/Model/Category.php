<?php
	class Category extends AppModel {
		public $actsAs = array('Tree');
		
		public $hasMany = array(
			'Post' => array(
				'className' => 'Post',
				'foreignKey' => 'category_id'
			)
		);
	}