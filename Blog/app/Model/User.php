<?php
	class User extends AppModel {
		public $hasMany= array(
			'Post' => array(
				'className' => 'Post',
				'foreignKey' => 'user_id',
				'fields' => array(
					'id',
					'title',
					'created',
					'modified',
					'user_id'
				)
			)
		);
		
		public $actsAs = array('Acl' => array('type' => 'both'));
		
		public $belongsTo = array(
			'Group' => array(
				'className' => 'Group',
				'foreignKey' => 'group_id',
			)
		);
		
		public $virtualFields = array(
			'post_count' => 'SELECT count(*) FROM users AS u, posts AS p WHERE u.id = p.user_id AND User.id = u.id GROUP BY u.id'
		);
		
		public $validate = array(
			'username' => array(
				'required' => array (
					'rule' => array('notEmpty'),
					'message' => 'A username is required.'
				)
			),
			'password' => array(
				'required' => array(
					'rule' => array('notEmpty'),
					'message' => 'A password is required'
				)
			),
			'group_id' => array(
				'numeric' => array(
					'rule' => array('numeric')
				)
			)
		);
		
		public function afterSave($created) {
			$this->Aro->save(array('alias'=>'user'.$this->data[$this->alias]['id']));
		}
		
		public function parentNode() {
			if (!$this->id && empty($this->data)) {
				return null;
			}
			if (isset($this->data['User']['group_id'])) {
				$groupId = $this->data['User']['group_id'];
			} else {
				$groupId = $this->field('group_id');
			}
			if (!$groupId) {
				return null;
			} else {
				return array('Group' => array('id' => $groupId));
			}
		}
		
		public function bindNode($user) {
			return array(
				'model' => 'Group',
				'foreign_key' => $user['User']['group_id'],
				//'alias' => $user['User']['id']
			);
		}
		
		public function isOwner($userId,$user) {
			return $this->field('id', array('id'=> $userId,'username' => $user)) === $userId;
		}
		
		public function beforeSave($options=array()) {
			if(isset($this->data[$this->alias]['password'])) {
				$this->data[$this->alias]['password']=AuthComponent::password($this->data[$this->alias]['password']);
			}
			return true;
		}
		
		
	}
?>