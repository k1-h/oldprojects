<?php
	class Setting extends AppModel {
		public $findMethods = array('setting' =>  true);
		
		protected function _findSetting($state, $query, $results = array()) {
			if ($state == 'before') {
				return $query;
			}
			$newResults=array();
			foreach ($results as $result){
				$newResults[$result['Setting']['name']]=$result['Setting']['value'];
			}
			return $newResults;
		}
	}
?>