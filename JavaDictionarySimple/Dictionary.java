package dictionary;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Dictionary {

    JFrame loginFrame = new JFrame();
    JFrame registerFrame = new JFrame();
    JFrame mainFrame = new JFrame();

    public static void main(String[] args) {
        new Dictionary();

    }
    Dictionary() {
        prepareLogin();
        prepareRegister();
        prepareMain();

        loginFrame.setVisible(true);
    }
    
    void prepareLogin() {
        loginFrame.setTitle("Login");
        loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel loginPanel = new JPanel();
        loginFrame.add(loginPanel);
        loginPanel.setLayout(new GridLayout(3,2));

        JLabel usernameLabel = new JLabel("Username: ");
        JLabel passwordLabel = new JLabel("Password: ");
        final JTextField username = new JTextField(10);
        final JPasswordField password = new JPasswordField(10);
        JButton login =new JButton("Login");
        JButton register =new JButton("Register");

        loginPanel.add(usernameLabel);

        loginPanel.add(username);
        loginPanel.add(passwordLabel);
        loginPanel.add(password);
        loginPanel.add(login);
        loginPanel.add(register);
        login.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                login(username.getText(),password.getText());
            }
        });
        register.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                loginFrame.setVisible(false);
                registerFrame.setVisible(true);
            }
        });
        loginFrame.pack();
    }

    void prepareRegister() {
        registerFrame.setTitle("Register");
        registerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel registerPanel = new JPanel();
        registerFrame.add(registerPanel);
        registerPanel.setLayout(new GridLayout(4,2));

        JLabel usernameLabel = new JLabel("Username: ");
        final JTextField username = new JTextField(10);
        JLabel emailLabel = new JLabel("Email: ");
        final JTextField email = new JTextField(10);
        JLabel passwordLabel = new JLabel("Password: ");
        final JPasswordField password = new JPasswordField(10);
        JButton register =new JButton("Register");

        registerPanel.add(usernameLabel);
        registerPanel.add(username);
        registerPanel.add(emailLabel);
        registerPanel.add(email);
        registerPanel.add(passwordLabel);
        registerPanel.add(password);
        registerPanel.add(new JLabel(""));
        registerPanel.add(register);

        register.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                register(username.getText(),email.getText(),password.getText());
            }
        });
        registerFrame.pack();
    }

    void prepareMain() {
        mainFrame.setTitle("Dictionary");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel mainPanel = new JPanel();
        mainFrame.add(mainPanel);
        mainPanel.setLayout(new GridLayout(3,2));

        JLabel englishLabel = new JLabel("English: ");
        final JTextField english = new JTextField(10);
        JLabel persianLabel = new JLabel("Persian: ");
        final JLabel persian = new JLabel("");
        JButton search =new JButton("Search");

        mainPanel.add(englishLabel);
        mainPanel.add(english);
        mainPanel.add(persianLabel);
        mainPanel.add(persian);
        mainPanel.add(new JLabel(""));
        mainPanel.add(search);

        search.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                String word;
                word=search(english.getText());
                persian.setText(word);
            }
        });
        mainFrame.pack();
    }
    void login(String username,String password) {
        try {
            Connection connection=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/javaDictionary", "root", "");
            Statement statement=connection.createStatement();
            ResultSet resultSet=statement.executeQuery("select * from user where username='"+username+"' and password='"+password+"';");
            if (!resultSet.next()){
                JOptionPane.showMessageDialog(null, "Username and/or password is incorrect.");
            }
            else {
                loginFrame.setVisible(false);
                mainFrame.setVisible(true);
            }
            resultSet.close();
            statement.close();
            connection.close();
        }
        catch (SQLException sqlExp){
            sqlExp.printStackTrace();
        }
    }
    void register(String username,String email,String password) {
        try {
            Connection connection=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/javaDictionary", "root", "");
            Statement statement=connection.createStatement();
            Boolean resultSet=statement.execute("insert into user values('"+username+"', '"+email+"' ,'"+password+"');");
            if (!(email.contains("@") && email.contains(".")) {
                JOptionPane.showMessageDialog(null,"Email should be Valid.");
                return;
            }
            if (resultSet){
                JOptionPane.showMessageDialog(null, "Registeration Failed.");
            }
            else {
                JOptionPane.showMessageDialog(null, "Registeration Completed.");
                registerFrame.setVisible(false);
                loginFrame.setVisible(true);
            }
            statement.close();
            connection.close();
        }
        catch (SQLException sqlExp){
            sqlExp.printStackTrace();
        }
    }
    String search(String word){
        try {
            Connection connection=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/javaDictionary", "root", "");
            Statement statement=connection.createStatement();
            ResultSet resultSet=statement.executeQuery("select persian from words where english ='"+word+"';");
            if(resultSet.next()){
                return resultSet.getString(1);
            }
            resultSet.close();
            statement.close();
            connection.close();
        }
        catch (SQLException sqlExp){
            sqlExp.printStackTrace();
        }
        return "Not Found.";
    }
}
