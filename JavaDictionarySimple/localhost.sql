-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 05, 2013 at 01:43 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `javadictionary`
--
CREATE DATABASE `javadictionary` DEFAULT CHARACTER SET utf8 COLLATE utf8_persian_ci;
USE `javadictionary`;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(40) COLLATE utf8_persian_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_persian_ci NOT NULL,
  `password` varchar(40) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `email`, `password`) VALUES
('user', 'user', '123'),
('user2', 'user@', '123'),
('user3', 'user3', '123'),
('user4', 'user3@gmail.com', '123');

-- --------------------------------------------------------

--
-- Table structure for table `words`
--

CREATE TABLE IF NOT EXISTS `words` (
  `english` varchar(40) COLLATE utf8_persian_ci NOT NULL,
  `persian` varchar(40) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`english`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `words`
--

INSERT INTO `words` (`english`, `persian`) VALUES
('browse', 'جستجو کردن'),
('create', 'ایجاد کردن'),
('error', 'خطا'),
('export', 'صادر کردن'),
('import', 'وارد کردن'),
('insert', 'وارد کردن'),
('structure', 'ساختار'),
('table', 'جدول'),
('user', 'کاربر'),
('word', 'کلمه');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
