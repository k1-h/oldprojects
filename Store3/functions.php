<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/simple-store
 */
function readDataFile() {		// Khondan file vorodi
    $data=file_get_contents('data.txt');		// Tamam mohtaviyat file data.txt ro bekhon va to reshte $data briz
    $data=json_decode($data,true);		// Tabe json_decode ye reshte ro ke format khasi dare ro migire va on ro be arraye tabdil mikone

    return $data;		// arraye $data ro bargar don
}

function writeDataFile($data) {		// Neveshtan dar file vorodi
    $data=json_encode($data);		// Tabe json_encode ye arraye ro be sorat be reshte khas code mikone ke badan be vasile tabe json_decode mishe reshte ro be arraye tabdil kard
    file_put_contents('data.txt',$data);		// Reshte tabdil shode to file data.txt minevise
}

function search($search,$data) {		// Jostojo dar dade ha
    $result=array();		// Arraye shamel kild haye peyda shode
    foreach ($data as $item) {			// Baraye har satr(ghate) az arraye(dade ha)
        foreach ($item as $field) {		// Baraye har onsor(nam, gheymat va ...) az satr(ghate)
            if (strtolower($search)==strtolower($field)) {		// Agar onsor ba ebarat vared shode barabar bod(hardo reshte aval be horof kochik tabdil mishan)
                $result[]=$item;		// on satr ro dakhel arraye natije briz
            }
        }
    }

    return $result;		// arraye natije ro bargardon
}

function add($name,$type,$price,$serial,$date,$data) {		// Ezafe kardan
    $item=array();		// Ghate e ke bayad ezafe beshe
    $item['name']=$name;		// Nam Ghate
    $item['type']=$type;		// Noee Ghate
    $item['price']=$price;		// Gheymat Ghate
    $item['serial']=$serial;		// Serial Ghate
    $item['date']=$date;		// Date Ghate
    $data[]=$item;		// Ghate jadid ro be akhar array dade ha ezafe kon

    return $data;		// Arraye shamel ghate jadid ro bargar don
}

function remove($id,$data) {		// Hazf ghate
    $newdata=array();		// Arraye jadid ke onsori ke karvar vared karde az on hazf shode
    foreach ($data as $key => $item) {		// Baraye har satr az arraye ke klid on $key va khod satr $item ast
        if($key!=$id)		// agr kild ba id dade shode barabar "NA BOD"
            $newdata[]=$item;		// Item ro be arraye jadid ezafe kon
    }

    return $newdata;		// Arraye jadid ke onsor mored nazar az on hazf shode ro bargar don
}

function sortData($by,$data) {		// Moratab sazi
    $count=count($data);		// Tedad anasor arraye ro be motaghayer $count bede
    for ($i=0;$i<$count;$i++) {		// Bubble Sort
        for ($j=0;$j<$count;$j++) {
            if (strnatcmp($data[$i][$by],$data[$j][$by])<0) {		// strnatcmp() Mesl tabe compare() c++ hast
                $temp=$data[$i];
                $data[$i]=$data[$j];
                $data[$j]=$temp;
            }
        }
    }

    return $data;		// Arraye moratab shode ro bar gar don
}
