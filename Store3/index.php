<?php
/**
 * @author  Keyvan Hedayati <k1.hedayati93@gmail.com>
 * @license GNU General Public License, version 3
 * @link    https://github.com/k1-hedayati/simple-store
 */
// Dade ha dar arraye 2 bodi $data zakire shode and ke
// har satr arraye shamel moshakhasat yek ghete ast
require 'functions.php';		// Include file tavabe
$data=readDataFile();			// Khondan file va rikhtan mohtaviyyat on dar araye $data
if (isset($_POST['search'])) {	// Agar karbar form jostojo ro ersal karde bashe
    $data=search($_POST['search'],$data); 		// Tabe search ro seda bazan va ebarat mored nazar va dade ha ro be tabe bede va item haye peyda shode ro dobare dakhel $data beriz
}
if (isset($_POST['name'])) {		// Agar karbar form ezafe kardan ro ersal karde bashe
    $data=add($_POST['name'],$_POST['type'],$_POST['price'],$_POST['serial'],$_POST['date'],$data);		// Moshakhsat ghate va dade ha ro be tabe add() bede ta ghate ro be dade ha ezafe kone
    writeDataFile($data);		// Dade haye jadid ro dar file zakhire kon
}
if (isset($_POST['id'])) {		// Agar karbar form hazf kardan ro ersal karde bashe
    $data=remove($_POST['id']-1,$data);		// Ghate ba ID dade shode va dade ha ro be tabe remove() befrest va dade haye jadid ro ke ID dade shode az onha hazf shode ro to $data zakhire kon
    writeDataFile($data);		// Dade haye jadid ro to file zakhire kon
}
if (isset($_GET['sort'])) {		// Agar karbar ghasd moratab sazi dashte
    $data=sortData($_GET['sort'],$data);	// Arraye ro ba klid dade shode('name', 'type', etc) moratab kon va natije ro dakhel $data beriz
}
?>
<html>
<head>
    <style>
        #container {
            width:50%;
            margin:auto;
        }
        #actions {
            float:left;
        }
        #data {

        }
        #head {
            font-weight:bold;
            text-align:center;
        }
        a {
            text-decoration:none;
            color:blue;
        }
        a:visited {
            color:blue;
        }
    </style>
</head>
<body>
    <div id="container">
        <div id="actions">
            <div id="search">
                <form method="post" name="searchForm" action="index.php">
                    <table cellspacing="10">
                        <tr>
                            <td><label>Search </label></td>
                            <td><input type="text" name="search"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" value="Search" /> <a href="index.php">Rest Search</a></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="add">
                <form method="post" name="addForm" action="index.php">
                    <table cellspacing="10">
                        <tr>
                            <td><label>Name </label></td>
                            <td><input type="text" name="name"/></td>
                        </tr>
                        <tr>
                            <td><label>Type </label></td>
                            <td><input type="text" name="type"/></td>
                        </tr>
                        <tr>
                            <td><label>Price </label></td>
                            <td><input type="text" name="price"/></td>
                        </tr>
                        <tr>
                            <td><label>Serial </label></td>
                            <td><input type="text" name="serial"/></td>
                        </tr>
                        <tr>
                            <td><label>Date </label></td>
                            <td><input type="date" name="date"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" value="Add" /></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="remove">
                <form method="post" name="removeForm" action="index.php">
                    <table cellspacing="10">
                        <tr>
                            <td><label>Delete</label></td>
                            <td><input type="text" name="id" placeholder="Enter item ID"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" value="Remove" /></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="sort">
            </div>
        </div>
        <div id="data">
            <table border="1" cellspacing="0" cellpadding="5">
                <tr id="head">
                    <td><a href="index.php">ID</a></td>
                    <td><a href="index.php?sort=name">Name</a></td>
                    <td><a href="index.php?sort=type">Type</a></td>
                    <td><a href="index.php?sort=price">Price</a></td>
                    <td><a href="index.php?sort=serial">Serial</a></td>
                    <td><a href="index.php?sort=date">Date</a></td>
                </tr>
                <?php
                    foreach ($data as $key=>$item) {		// Baraye har satr az arraye ke klidesh $key(0,1,2 va ...) va satresh $item ast, en karha ro anjam bede
                        echo '<tr>';
                            echo '<td>'.($key+1).'</td>';		// Be klid arraye yeki ezafe kon ta to jadval dorost neshon dade beshe va ono to jadval chap kon
                            echo '<td>'.$item['name'].'</td>';		// Name ghate ro chap kon
                            echo '<td>'.$item['type'].'</td>';		// Noe ghate ro chap kon
                            echo '<td>'.$item['price'].'</td>';		// Gheymat ghate ro chap kon
                            echo '<td>'.$item['serial'].'</td>';		// Serial ghate ro chap kon
                            echo '<td>'.$item['date'].'</td>';		// Tarikh ghate ro chap kon
                        echo '</tr>';
                    }
                ?>
            </table>
        </div>
    </div>
</body>
</html>
